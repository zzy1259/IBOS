/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : oa

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-01-30 20:25:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `yckj_announcement`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_announcement`;
CREATE TABLE `yckj_announcement` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `author` varchar(15) NOT NULL DEFAULT '' COMMENT '作者',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型，0为内容，1为链接',
  `sort` tinyint(3) NOT NULL DEFAULT '0' COMMENT '排序号',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `message` text NOT NULL COMMENT '公告内容',
  PRIMARY KEY (`id`),
  KEY `timespan` (`starttime`,`endtime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_announcement
-- ----------------------------
INSERT INTO `yckj_announcement` VALUES ('1', '管理员', '<span style=\'color: rgb(226, 111, 80);\'>请使用支持HTML5的浏览器登录！</span>', '0', '0', '1401552000', '1433088000', '');

-- ----------------------------
-- Table structure for `yckj_approval`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_approval`;
CREATE TABLE `yckj_approval` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '审批流程名称',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核等级,1-5级',
  `free` text NOT NULL COMMENT '免审核人uid，逗号隔开',
  `desc` text NOT NULL COMMENT '描述',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_approval
-- ----------------------------
INSERT INTO `yckj_approval` VALUES ('1', '一级审核', '1', '', '', '1402631014');
INSERT INTO `yckj_approval` VALUES ('2', '二级审核', '2', '', '', '1402631014');

-- ----------------------------
-- Table structure for `yckj_approval_record`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_approval_record`;
CREATE TABLE `yckj_approval_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增 ID',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT '关联模型',
  `relateid` int(11) NOT NULL COMMENT '关联ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `step` int(11) NOT NULL COMMENT '步骤',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '审核时间',
  `status` int(11) NOT NULL COMMENT '审核状态,0表示退回，1表示通过，2表示流程结束，3表示发起',
  `reason` text NOT NULL COMMENT '原因，通过可以没有原因，退回一定有原因',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_approval_record
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_approval_step`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_approval_step`;
CREATE TABLE `yckj_approval_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增 ID',
  `aid` int(11) NOT NULL COMMENT '审批流程 ID',
  `step` tinyint(4) NOT NULL COMMENT '处于流程中第几级步骤',
  `uids` text NOT NULL COMMENT '当前步骤审核人员 ID 串 1,2,3....',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_approval_step
-- ----------------------------
INSERT INTO `yckj_approval_step` VALUES ('1', '1', '1', '1');
INSERT INTO `yckj_approval_step` VALUES ('2', '2', '1', '1');
INSERT INTO `yckj_approval_step` VALUES ('3', '2', '2', '1');

-- ----------------------------
-- Table structure for `yckj_article`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article`;
CREATE TABLE `yckj_article` (
  `articleid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `subject` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '内容类型 0为文章 1为图片 2为链接',
  `author` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '作者',
  `approver` text NOT NULL COMMENT '审批人',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `clickcount` int(10) unsigned NOT NULL DEFAULT '0',
  `attachmentid` text NOT NULL COMMENT '附件ID',
  `commentstatus` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '评论状态，1为开启0为关闭',
  `votestatus` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '投票状态 1为开启0为关闭',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '超链接地址',
  `catid` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '文章状态，1为公开2为审核3为草稿0为退回',
  `deptid` text NOT NULL COMMENT '阅读范围部门',
  `positionid` text NOT NULL COMMENT '阅读范围职位',
  `roleid` text NOT NULL COMMENT '阅读范围角色',
  `uid` text NOT NULL COMMENT '阅读范围人员',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶，1代表置顶，0为不置顶',
  `toptime` int(10) NOT NULL DEFAULT '0' COMMENT '置顶时间',
  `topendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '置顶过期时间',
  `ishighlight` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否高亮',
  `highlightstyle` char(50) NOT NULL DEFAULT '' COMMENT '高亮样式',
  `highlightendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '高亮过期时间',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  PRIMARY KEY (`articleid`),
  KEY `SUBJECT` (`subject`) USING BTREE,
  KEY `PROVIDER` (`author`) USING BTREE,
  KEY `NEWS_TIME` (`addtime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_article
-- ----------------------------
INSERT INTO `yckj_article` VALUES ('1', 'IBOS·酷办公荣获2017中国企业服务洞察力年会三项大奖', '', '2', '1', '1', '1517282443', '1517282443', '0', '', '1', '0', 'http://www.ciotimes.com/IT/123310.html', '1', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('2', 'IBOS受邀OSC源创会年终盛典，荣获“最受欢迎开源项目”大奖', '', '2', '1', '1', '1517282443', '1517282443', '0', '', '1', '0', 'http://mp.weixin.qq.com/s/rxdiJr4_6dtk5GCjrlzYVg', '1', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('3', '揭秘企业微信大战略 微信开发者大会回顾', '<p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">导读：11月15日，深圳微信开发者大会隆重召开。本次大会演讲嘉宾邀请到IBOS微办公创始人杨芳贤，杨芳贤担任数十家知名<a class=\"keyword_link\" id=\"k530831776\" href=\"http://com.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">企业</a><a class=\"keyword_link\" id=\"k530768899\" href=\"http://info.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">信息化</a>顾问，<a class=\"keyword_link\" id=\"k531798779\" href=\"http://www.chinabyte.com/keyword/%E8%87%B4%E5%8A%9B/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">致力</a>于以员工体验为中心的协同管理平台设计。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　议题：《企业微信大战略之企业号篇：如何构建以员工体验为中心的管理平台》</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　议题简介：企业微信大战略之企业号篇涵盖企业生产、供应链、营销及内部协作系统的全面升级改造，以及微信平台如何与企业原有的<a class=\"keyword_link\" id=\"k14071285\" href=\"http://www.chinabyte.com/keyword/CRM/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">CRM</a>、<a class=\"keyword_link\" id=\"k530768919\" href=\"http://oa.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">OA</a>、<a class=\"keyword_link\" id=\"k530831783\" href=\"http://www.chinabyte.com/keyword/ERP/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">ERP</a>等系统对接。通过微信开放平台实现移动端跨系统、跨平台、跨终端良好的设备适应性，微信支持文本、语音、图片、视频，扩展消息打破业务信息<a class=\"keyword_link\" id=\"k531804962\" href=\"http://www.chinabyte.com/keyword/%E6%B2%9F%E9%80%9A%E9%9A%9C%E7%A2%8D/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">沟通障碍</a>。此外，永久在线机制突破地域和时间限制，有效利用碎片化时间，使得个人任务、时间管理更高效。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　演讲吸引了大量来自全国各地的微信开发商/者、传统IT开发商。每场演讲后都安排了充分的交流时间，参会者积极踊跃的提问，及讲师耐心诚恳的解答，使大会现场气氛十分火热。很多参会者在活动结束后，都表示这次大会没有白来。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　以下是演讲全文：</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　“连接企业的另一种可能”是微信对企业号给出的定义，也诠释了微信“人与企业的连接”战略企图。人们在狂热追捧订阅号和服务号的同时，却忽视了微信可以成为企业应用的入口。微信企业号的诞生并不亚于<a class=\"keyword_link\" id=\"k530831684\" href=\"http://www.chinabyte.com/keyword/%E4%B9%94%E5%B8%83%E6%96%AF/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">乔布斯</a>的“重新发明<a class=\"keyword_link\" id=\"k530831540\" href=\"http://www.chinabyte.com/keyword/%E6%89%8B%E6%9C%BA/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">手机</a>”，当<a class=\"keyword_link\" id=\"k530831781\" href=\"http://com.chinabyte.com/baidu/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">百度</a>CEO<a class=\"keyword_link\" id=\"k530834388\" href=\"http://www.chinabyte.com/keyword/%E6%9D%8E%E5%BD%A6%E5%AE%8F/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">李彦宏</a>还在预言“企业级软件将是一个大市场”，微信已经优雅的绕过了<a class=\"keyword_link\" id=\"k531804383\" href=\"http://www.chinabyte.com/keyword/%E4%BC%81%E4%B8%9A%E7%A7%BB%E5%8A%A8%E5%BA%94%E7%94%A8/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">企业移动应用</a>入口战场，留下如梦江南，烟雨女子般的眼神，从此众人念念不忘....</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxLic5OSh7icvEQkd3Xkic4FHTScasOPdpmVK4BEXtk6CV8pQdUHvbsQhZX5ULZUlcCfRB695ZYOMCHK2A/0\" width=\"500\" height=\"375\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业号的无限价值</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、企业内部、上游供应商、下游分销商的全连接</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　一两万名员工，几千家经销商及门店，上下游供应商一起十万人，内部十几个业务系统整合、移动化。而在过去建立、运维这样的系统所需的能力已超越大多企业IT部门的能力。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、企业全信息的统一出入口</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业号可以是内部所有信息系统统一的消息出口及入口，基于微信企业号，员工无需安装任何手机APP，即可打通企业内全部信息。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、内部IT系统孤岛的桥梁</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业内部IT系统长期以来分而治之，数据不互通，给企业带来了信息交换上的困难。然而，基于企业号则可以打通企业内部现有IT系统，把信息孤岛相互连接起来。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　4、集成各类<a class=\"keyword_link\" id=\"k530769329\" href=\"http://www.chinabyte.com/keyword/SaaS/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">SaaS</a>应用成提供平台</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　随着越来越多企业引入了轻量级的SaaS应用，到SaaS应用与企业内系统数据、不同SaaS应用间数据互通是个难题。基于微信企业号，除了能够打通内部系统外，还能将内部应用以及外部SaaS应用在企业号内互通。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　5、企业IM功能、开发多方视频通话</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　未来，当微信平台开放了点对点的沟<a class=\"keyword_link\" id=\"k530831723\" href=\"http://telecom.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">通信</a>息，企业号将会成为企业内部的IM。另外，微信还开放视频通话，将可以在内部业务系统上一键发起对指定人员的微信视频通话，完成实时视频会议。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETSbUU83tkxfR27rVRxibytbvics2mViabsy5ibuSJF6JuYR7JKeazl8NMkQ/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　让构想成为现实</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　大家会发现，如果上述的价值需要通过自建系统成为现实，企业将会面临搭建成本高、实施周期长、实施难度大以及运维成本庞大等问题。但基于微信企业号，企业将能够轻松地避免上述问题。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、企业号是免费的。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、员工只需扫码关注企业号即可使用，实施周期短。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、企业号一扫关注即可完成实施。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　4、借助微信强大的运维能力，企业运维成本为零。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETbaOklEMeV84ILozDxHZnFZpLpa8pnXH8ibYOw0qNQz5dlLYI13xhiafw/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　轻前端+大后台的设计原则</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业内现有的IT系统往往功能完整、系统庞大，已经能够满足企业管理需求，这些系统都将得以保留并且被称为“大后台”，而作为移动端的微信企业号中的应用主要呈现方式则被称为“轻前端”。在设计当中企业需要考虑四个要点：</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、用户体验</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　现在的管理软件用户实际上就是<a class=\"keyword_link\" id=\"k530770682\" href=\"http://net.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">互联网</a>用户，这些户已经互联网产品的极致用户体验习以为常，便开始对传统管理软件产生抱怨。我们建议在微信企业号应用功能设计时参考互联网产品的用户体验进行设计，并且始终坚持“用户体验放在第一位”的原则。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、最小化模块</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　根据设计原则我们在微信轻前端上应该实现操作最简单，建议企业将功能<a class=\"keyword_link\" id=\"k530847329\" href=\"http://www.chinabyte.com/keyword/%E6%A8%A1%E5%9D%97/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">模块</a>进行最小化、颗粒化设计。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、阅读、基础功能为主</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　移动端下受屏幕大小的限制，建议在设计时应考虑以阅读、实现基础功能为主，而对于较为复杂的功能则建议在PC端进行实现。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　4、轻前端大后台</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　对于小型系统，可以在微信企业号应用与企业现有IT系统直接建立连接;对于大型复杂的系统，建议在中间建立一个消息<a class=\"keyword_link\" id=\"k530769397\" href=\"http://soft.chinabyte.com/zjj/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">中间件</a>，通过消息中间件可以判断信息的流向，消息队列、异步推送缓解<a class=\"keyword_link\" id=\"k530768927\" href=\"http://server.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">服务器</a>压力。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBEThEBjw98T1kdAG3g43tPVg7bm5ANrC8ylSYHgFeYdhdnmNIvRGZztQQ/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　五个典型企业应用<a class=\"keyword_link\" id=\"k530831497\" href=\"http://solution.chinabyte.com/new/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">方案</a>分享</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、USB KEY、电子签章的替代</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　很多企业安全级别较高的账号或系统往往会配以USB KEY，而现在手机上的微信账号也能成为系统验证用户身份的唯一标识。手机就成了用户随身的USB KEY了，用户通过手机中微信扫一扫，实现替代USB KEY，实现安全登录系统。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETwXtlMYhw0icicp2ASWxgM5iau6WZpNnZOMAZazibjHx1UC6yLWIvAelXRA/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　市场上的电子签章系统主要能解决两个问题：1. 用户身份验证2. 信息完整性验证，防止篡改。对于只需要用电子签章系统解决身份认证的用户，可以通过而微信扫一扫功能，很好地解决了身份验证问题。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETL8MO4AZWOn4TjyPvhm8zuPIF8YZVTfGAbYJS0qsSYjxNTsOooCt2xg/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、内部文件共享</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　我们假设一个外出员工需要查找公司网盘上的文件，这时他只需打开微信企业号发送文件名或者直接发送语音，系统通过中间件将语音转换为文字并通过后端查找到相应的文件进行推送，员工立即获得文件。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETxzVMKvmicPuia4ibJuqEq7XMKicD4rD9kMILTdRicvNp5daoM31nRaoWrPA/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、财务支付</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\"><a class=\"keyword_link\" id=\"k530831849\" href=\"http://www.chinabyte.com/keyword/%E9%A9%AC%E4%BA%91/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">马云</a>希望让中国企业能够用<a class=\"keyword_link\" id=\"k530832395\" href=\"http://www.chinabyte.com/keyword/%E6%94%AF%E4%BB%98%E5%AE%9D/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">支付宝</a>发放工资的构想，也许在未来可以在微信支付上得以实现。对于大型企业，每月向员工发放工资是一项相对庞大而繁琐的工作。基于微信企业号，员工可以在财务助手中收到工资条，并直接完成工资单的核对，避免了与财务人员面对面的签收。倘若微信支付能够在企业号进行开放，将可以实现员工在微信确认工资单后立即通过微信支付发放工资。马云多年的夙愿或许将被微信支付来完成。工资被打入微信账号后，微店消费、微信支付便成了水到渠成的事，</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBET0w9tdqTiak7NOVqXMOF2FUjCe8x9YLrqBywjjTw9BLSQHXxk3ndE8oQ/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　4、服务号+企业号，服务面对面</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　在微信大战略中，如果狭隘地认为企业号仅仅是解决企业内部沟通问题则将会大大地减低企业号的价值。当企业号与服务号、订阅号连接起来，就会延伸出许多新的玩法。譬如某以定制类产品为主导的家居企业，以往用户只能通过实体店进行需求提交，而现在通过微信服务号能够直接提交设计申请，并且系统将根据地理位置以及需求属性推荐到合适的设计师当中，设计师通过企业号领取设计任务，并可以直接添加客户微信进行一对一的沟通。设计完成后用户对设计师评分，公司根据用户对设计师评分进行绩效考核。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETLoKHadEliaUk3o94Ra6Dkrl5GkoPb4u9coteLSxq2gJdCRgDAzRkZicA/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　5、企业号+朋友圈，成为营销主阵地</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　基于微信企业号，企业可以通过营销大使模块鼓励员工参与分享传播企业产品、服务、活动信息，让每个员工都成为公司“水军”的一员。企业可以根据员工的职位以及好友数量来<a class=\"keyword_link\" id=\"k531801710\" href=\"http://www.chinabyte.com/keyword/%E7%95%8C%E5%AE%9A/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">界定</a>“水军”级别，不同的级别将决定了员工分享后所能获得的回报。公司员工、上下游有10万人，而每个人平均有两百个好友，那么当这10万人分别转发公司的产品、服务、活动信息，所产生的营销价值将会比一个具有2000万粉丝的公众号推送营销信息带来的还要大。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETKrQUd6cibzJpoVw3IhAbjYdwku9zHkibibS4oHpguoCLeyKW0iaN1XI7rg/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　链接一切</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　微信个人账号实现了人与人的链接，订阅号实现人与信息的链接，通过服务号实现了人与商品间的链接，通过企业号实现了人与企业的链接。基于微信的开放战略，微信可以连接一切!</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBET0qtpPI9Ve9tqoSl4YD2s0LzCic5MujS6WicUGQMibiatIS0Vibd0VVrfgeQ/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业个性化开发需注意</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、消息免打扰</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　我们不能假设每个人都和在座的<a class=\"keyword_link\" id=\"k530839316\" href=\"http://www.chinabyte.com/keyword/CTO/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">CTO</a>一样都是工作狂，热爱工作。事实上，频繁的提醒会让用户产生厌恶感。因此，可以考虑给员工自行设置在线状态，我们在设计系统的时候也应该增加允许设置消息免打扰时间段。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBETxnw59PZ3G3ic3ib9soJsuBTQwQ6IwHvne5zeVM0ia7jiaHGIiaCyaAGsbOg/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、数据安全加固</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　目前，无论是微信账号的登录机制、企业号后台登录机制还是微信与系统对接的机制都是十分安全的。然而，这一说法需要建立在两个假设下：1、对微信充分信任;2.用户手机已设置密码且不丢失。对涉及核心数据的模块，我们在通过微信链接进入系统的过程中增加一个密码验证层。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://mmbiz.qpic.cn/mmbiz/Hpd3zFZHxL9e9dfm1icibgziag1YRg2OBEToAbJgnqMnWQF8wn8ZXt6Eh6Od9ul2PibBV8mlQ1MKELtao5u8EQiciclg/0\" width=\"500\" height=\"323\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　未来的企业号</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、降低认证门槛</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、加大开放力度</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、做好平台，宽而浅的应用、应用市场</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　4、开放IM能力，开放<a class=\"keyword_link\" id=\"k530840180\" href=\"http://www.chinabyte.com/keyword/VoIP/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">VOIP</a>、视频等通信能力</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　如果未来微信能够做到上述几点，我们相信…</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1、大多数企业将开通不止一个企业号</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　2、微信将聚集比<a class=\"keyword_link\" id=\"k530831281\" href=\"http://com.chinabyte.com/apple/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">苹果</a>应用市场更大的开发群</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　3、移动设备消失了，微信还在继续...</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　一起用微信连接的力量完成智慧的构想...</p><p><br /></p>', '0', '1', '1', '1517282443', '1517282443', '1', '', '1', '0', '', '1', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('4', 'IBOS受邀首批入驻腾讯海南创业基地', '<p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">2014年10月30日，优网科技旗下IBOS创始人杨芳贤受邀参加了在海南博鳌亚洲论坛举行的首届<a class=\"keyword_link\" id=\"k530831746\" href=\"http://com.chinabyte.com/qq/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">腾讯</a>全球合作伙伴大会，大会分会场《连接·创业》正式宣布海南生态软件园成为腾讯第五个的创业基地，并邀请腾讯优秀合作伙伴免费入驻，经过几轮路演角逐，IBOS在数百家合作伙伴中脱颖而出，最终被腾讯认可成为首批获邀入驻<a class=\"keyword_link\" id=\"k530831776\" href=\"http://com.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">企业</a>，11月1日IBOS创始人杨芳贤代表IBOS公司正式签约入驻腾讯海南创业基地。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015902146.jpg\" width=\"500\" height=\"278\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\">　　腾讯海南生态软件园创业基地</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015904982.jpg\" width=\"500\" height=\"434\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\">　　签字仪式一</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015905929.jpg\" width=\"500\" height=\"373\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\">　　签字仪式二</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015906678.jpg\" width=\"500\" height=\"674\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\">　　园区实景</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015911242.jpg\" width=\"500\" height=\"674\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;text-align:center;margin-top:0px;margin-bottom:26px;\">　　办公区域</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　关于IBOS<a class=\"keyword_link\" id=\"k530833359\" href=\"http://www.chinabyte.com/keyword/%E5%8D%8F%E5%90%8C%E5%8A%9E%E5%85%AC/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">协同办公</a>平台</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　开源 + 微信企业号 + 云服务中心，IBOS V3 真正可用的微信办公!</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　与订阅号、服务号不同的是，企业号提供的是开放的通讯能力，后端的管理功能的实现需要依赖管理软件厂商的各类产品来完成。传统管理软件厂商在移动端一直是短板，依托企业号，将如虎添翼，对传统管理软件厂商而言，这是拥抱<a class=\"keyword_link\" id=\"k530832303\" href=\"http://www.chinabyte.com/keyword/%E7%A7%BB%E5%8A%A8%E4%BA%92%E8%81%94%E7%BD%91/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">移动互联网</a>的绝好机会。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015912220.jpg\" width=\"500\" height=\"339\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　企业号最大的价值在于其开放性，开放的微信原生能力，如拍照、语音、视频、扫码、支付、位置等，能将<a class=\"keyword_link\" id=\"k530831540\" href=\"http://www.chinabyte.com/keyword/%E6%89%8B%E6%9C%BA/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">手机</a>设备的硬件特性直接引入到企业管理软件中，使用门槛还无限低。</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015913536.jpg\" width=\"500\" height=\"339\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　开放源代码的IBOS V3让你即便是虚拟主机也可运行无阻，后台三步完成企业号与IBOS的绑定，无缝连接企业号。能力无限的云服务带来短信、邮件、语音、<a class=\"keyword_link\" id=\"k530840180\" href=\"http://www.chinabyte.com/keyword/VoIP/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">VOIP</a>等通讯能力，瞬间企业号瞬间百媚千抹，美艳绝伦!</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img alt=\"\\\" src=\"http://www.zwtxnews.com/uploadfile/2014/1127/20141127015914609.jpg\" width=\"500\" height=\"339\" data-bd-imgshare-binded=\"1\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　关于海南腾讯创业基地</p><p style=\"padding:0px;border:0px;font-size:16px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　腾讯创业基地正式落户海南生态软件园，这是腾讯的第五个创业基地。基地规划规模1.6万平方米，未来五年将实现100家产值大于千万、10家产值过亿的创新企业入驻，实现腾讯创业基地(海南)年总产值超过20亿元，每年选择培养3-5家行业细分领域标杆企业，争取有一批企业上市。海南生态软件园投资发展有限公司总经理杨淳至介绍，腾讯主要提供线上的资源，生态软件园主要提供线下的资源，包括办公、住房、以及<a class=\"keyword_link\" id=\"k530833378\" href=\"http://www.chinabyte.com/keyword/%E8%9E%8D%E8%B5%84/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">融资</a>的服务。</p><p><br /></p>', '0', '1', '1', '1517282443', '1517282443', '1', '', '1', '0', '', '1', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('5', '企业微信大战略之企业号篇', '', '1', '1', '1', '1517282443', '1517282443', '0', '', '1', '0', '', '1', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('6', '博思协创推微信办公新产品“微办公”', '<p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　1月25日消息(记者王亚萍)1月25日，博思协创旗下IBOS团队推出了一款基于微信公众平台开发接口开发的移动办公产品“微办公”，博思协创“微办公”将IBOS<a class=\"keyword_link\" id=\"k530833359\" href=\"http://www.chinabyte.com/keyword/%E5%8D%8F%E5%90%8C%E5%8A%9E%E5%85%AC/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">协同办公</a>平台嵌入微信公众平台，用户无需安装<a class=\"keyword_link\" id=\"k530831540\" href=\"http://www.chinabyte.com/keyword/%E6%89%8B%E6%9C%BA/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">手机</a>APP端即可实现移动办公的所有功能，以满足移动时代用户对于移动办公产品的需求。</p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img src=\"http://www.zwtxnews.com/u_file/images/image/2014_01/26/201401261135094767.jpg\" width=\"450\" height=\"313\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;text-align:center;margin-top:0px;margin-bottom:26px;\">　　微信公众账号 微办公主界面</p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;margin-top:0px;margin-bottom:26px;\"><img src=\"http://www.zwtxnews.com/u_file/images/image/2014_01/26/201401261136354750.jpg\" width=\"450\" height=\"311\" style=\"margin:0px;padding:0px;border:0px;vertical-align:middle;\" /></p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;text-align:center;text-align:center;margin-top:0px;margin-bottom:26px;\">　　信息中心界面 工作日志界面</p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　据了解，“微办公”现面向微信公众账号所有用户使用，订阅号及服务号用户均可在微信公众平台中绑定“微办公”快速实现<a class=\"keyword_link\" id=\"k530831776\" href=\"http://com.chinabyte.com/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">企业</a>移动办公。成功绑定后，企业内每个用户通过微信便可查看工作日程、工作任务、收发邮件、查阅公司新闻、工作日志、签发公文、审批工作流程。此外，基于“微办公”还能实现消息通知、企业<a class=\"keyword_link\" id=\"k530835885\" href=\"http://www.chinabyte.com/keyword/%E5%BE%AE%E5%8D%9A/\" target=\"_blank\" style=\"text-decoration:none;color:#333333;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d30010;\">微博</a>、客户管理、合同查询、订单查询等功能。</p><p style=\"padding:0px;border:0px;line-height:28px;font-family:verdana, arial, helvetica, sans-serif, &#39;microsoft yahei&#39;;color:#555555;margin-top:0px;margin-bottom:26px;\">　　有业内人士评论表示 “微办公”的出现让企业可以通过微信公众账号打通企业内部信息协作通道，给用户带来前所未有的移动办公新体验。IBOS团队推出的“微办公”如果能整合微信开放平台认证号高级接口中的语音识别、获取用户地理位置、上传下载多媒体文件、OAuth2.0网页授权、生成带参数二维码等高级接口，“微办公”将会有更广的想象空间，能有效解决传统管理软件无法满足用户对于移动办公的新需求。</p><p><br /></p><p>原文地址：http://soft.chinabyte.com/os/438/12848438.shtml</p>', '0', '4', '4', '1517282443', '0', '3', '', '1', '0', '', '2', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('7', 'IBOS类社区积分体系，打造最具朝气团队', '', '2', '1', '1', '1517282443', '1517282443', '0', '', '1', '0', 'http://www.zwtxnews.com/info/2014_02/08/38630.html', '2', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('8', 'IBOS2.0 最新统计效果图预览', '', '1', '1', '1', '1517282443', '1517282443', '2', '', '1', '0', '', '4', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('9', '公司管理制度', '<p>为加强公司的规范化管理，完善各项工作制度，促进公司发展壮大，提高经济效益，根据国家有关法律、法规及公司章程的规定，特制订本公司管理制度大纲。</p><p><br /></p><p>一、公司全体员工必须遵守公司章程，遵守公司的各项规章制度和决定。</p><p><br /></p><p>二、公司倡导树立“一盘棋”思想，禁止任何部门、个人做有损公司利益、形象、声誉或破坏公司发展的事情。</p><p><br /></p><p>三、公司通过发挥全体员工的积极性、创造性和提高全体员工的技术、管理、经营水平，不断完善公司的经营、管理体系，实行多种形式的责任制，不断壮大公司实力和提高经济效益。</p><p><br /></p><p>四、公司提倡全体员工刻苦学习科学技术和文化知识，为员工提供学习、深造的条件和机会，努力提高员工的整体素质和水平，造就一支思想新、作风硬、业务强、技术精的员工队伍。</p><p><br /></p><p>五、公司鼓励员工积极参与公司的决策和管理，鼓励员工发挥才智，提出合理化建议。</p><p><br /></p><p>六、公司实行“岗薪制”的分配制度，为员工提供收入和福利保证，并随着经济效益的提高逐步提高员工各方面待遇；公司为员工提供平等的竞争环境和晋升机会；公司推行岗位责任制，实行考勤、考核制度，评先树优，对做出贡献者予以表彰、奖励。</p><p><br /></p><p>七、公司提倡求真务实的工作作风，提高工作效率；提倡厉行节约，反对铺张浪费；倡导员工团结互助，同舟共济，发扬集体合作和集体创造精神，增强团体的凝聚力和向心力。</p><p><br /></p><p>八、员工必须维护公司纪律，对任何违反公司章程和各项规章制度的行为，都要予以追究。</p><p><br /></p>', '0', '1', '1', '1517282443', '1517282443', '6', '', '1', '0', '', '3', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('10', '公司考勤制度', '<p>公司员工上班期间严格执行考勤制度，本制度适用于我公司一般员工至部门经理。</p><p><br /></p><p>一、作息时间</p><p style=\"text-indent:2em;\">1、公司实行每周5天工作制</p><p style=\"text-indent:2em;\"> &nbsp; &nbsp; 上午 &nbsp; 9:00 ――12:00</p><p style=\"text-indent:2em;\"> &nbsp; &nbsp; 下午 &nbsp; 14:00――18:00 &nbsp;</p><p style=\"text-indent:2em;\">2、部门负责人办公时间：8：45～12：00　　13：55～18：10 </p><p style=\"text-indent:2em;\">3、行政部经理、行政 &nbsp; &nbsp;管理员、考勤员办公时间：8：30～12：05　13：55～18：10。</p><p style=\"text-indent:2em;\">4、保洁员：7：30</p><p style=\"text-indent:2em;\">5、在公司办公室以外的工作场所：工作人员必须在约定时间的前五分钟到达指定地点，招集人必须提前15分钟到达指定地点。</p><p><br /></p><p>三、违纪界定</p><p style=\"text-indent:2em;\">员工违纪分为：迟到、早退、旷工、脱岗和睡岗等五种，管理程序如下：</p><p style=\"text-indent:2em;\">1、迟到：指未按规定达到工作岗位（或作业地点）；迟到30分钟以内的，每次扣10元；迟到30分钟以上的扣半天基本工资；迟到一小时的扣全天工资 ；</p><p style=\"text-indent:2em;\">2、早退：指提前离开工作岗位下班；早退3分钟以内，每次扣罚10元；30分钟以上按旷工半天处理。</p><p style=\"text-indent:2em;\">3、旷工：指未经同意或按规定程序办理请假手续而未正常上班的；旷工半天扣1天工资，旷工一天扣罚2天工资；一月内连续旷工3天或累计旷工5天的，作自动解除合同处理；全年累计旷工7天的作开除处理；</p><p style=\"text-indent:2em;\">4、脱岗：指员工在上班期间未履行任何手续擅自离开工作岗位的，脱岗一次罚款20元。</p><p style=\"text-indent:2em;\">5、睡岗：指员工在上班期间打瞌睡的，睡岗一次罚款20元；造成重大损失的，由责任人自行承担。</p><p style=\"text-indent:2em;\"><br /></p><p>四、请假制度</p><p style=\"text-indent:2em;\">1、假别分为：病假、事假、婚假、产假、年假、工伤假、丧假等七种。凡发生以上假者取消当月全勤奖。</p><p style=\"text-indent:2em;\">2、病假：指员工生病必须进行治疗而请的假别；病假必须持县级以上医院证明，无有效证明按旷工处理；出据虚假证明加倍处罚；病假每月2日内扣除50%的基本日工资；超过2天按事假扣薪。</p><p style=\"text-indent:2em;\">3、事假：指员工因事必须亲自办理而请的假别；但全年事假累计不得超过30天，超过天数按旷工处理；事假按 实际天数扣罚日薪。</p><p style=\"text-indent:2em;\">4、婚假：指员工达到法定结婚年龄并办理结婚证明而请的假别；</p><p style=\"text-indent:2em;\">5、年假：指员工在公司工作满一年后可享受3天带薪休假，可逐年递增，但最多不得超过7天，特殊情况根据工作能力决定；年假必须提前申报当年使用。</p><p style=\"text-indent:2em;\">6、工伤假：按国家相关法律法规执行。</p><p style=\"text-indent:2em;\">7、丧假：指员工父母、配偶父母、配偶、子女等因病伤亡而请的假别；丧假期间工资照发，准假天数如下：</p><p style=\"text-indent:2em;\"> &nbsp; &nbsp; &nbsp;父母或配偶父母伤亡 &nbsp; &nbsp; &nbsp; &nbsp;给假7天</p><p style=\"text-indent:2em;\"> &nbsp; &nbsp; &nbsp;配偶或子女伤亡 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 给假10天</p><p style=\"text-indent:2em;\"><br /></p><p>五、批假权限</p><p style=\"text-indent:2em;\">1、病事假：1天以内由部门负责人批准；3天以内由分管付总经理批准；三天以上总经理批准。请假手续送行政部行政管理员处案。</p><p style=\"text-indent:2em;\">2、其它假别由部门负责人签署意见后报分管付总经理审批，并送行政部行政管理员处备案。</p><p style=\"text-indent:2em;\">3、所有假别都必须由本人书面填写请假单，并按规定程序履行签字手续后方为有效假别；特殊情况必须来电、函请示，并于事后一日内补办手续方为有效假别；未按规定执行一律视为旷工。</p><p style=\"text-indent:2em;\"><br /></p><p>六、考勤登记</p><p style=\"text-indent:2em;\">公司实行每日签到制度，员工每天上班、下班需签字(共计每日2次)。</p><p style=\"text-indent:2em;\"><br /></p><p>七、外出</p><p style=\"text-indent:2em;\">1、员工上班直接在外公干的，返回公司时必须进行登记，并交由部门经理签字确认；上班后外出公干的，外出前先由部门经理签字同意后到前台处登记方可外出。如没有得到部门经理确认私自外出的，视为旷工。</p><p style=\"text-indent:2em;\">2、员工未请假即不到岗或虽已事先知会公司但事后不按规定补办请假手续的视为旷工。</p><p style=\"text-indent:2em;\"><br /></p><p>八、加班</p><p style=\"text-indent:2em;\">1、公司要求员工在正常工作时间内努力工作，提高工作效率，按时完成规定的任务，不提倡加班。特殊情况非加班不可的，必须填写《加班审批表》，部门经理签字后报公司分管领导批准。未经批准，公司一律不予承认加班。</p><p style=\"text-indent:2em;\">2、经过批准的加班，公司办公室按月进行统计结算。所有加班首先必须抵冲病、事假，有一天抵冲一天，多余部分由公司发给加班工资，不作调休处理。</p><p style=\"text-indent:2em;\">3、行政部对每月的考勤进行统计，统计表由经理签字后交财务部计发工资。</p><p style=\"text-indent:2em;\"><br /></p><p>九、出差</p><p style=\"text-indent:2em;\">1、员工出差，应事先填写《出差申请表》，由部门经理签署意见后报公司分管领导批准，部门经理以上人员由分管经理批准；总经理出差时应知会办公室，以便联络。《出差申请表》交行政部备查。</p><p style=\"text-indent:2em;\"><br /></p><p>十、员工因违纪的扣款，统一由公司办公室管理，作为员工集体活动的补充费用。</p><p><br /></p><p>十一、本制度自公司公布之日起执行。</p><p><br /></p><p>十二、本制度解释权归行政部。</p>', '0', '1', '1', '1517282443', '1517282443', '8', '', '1', '0', '', '3', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `yckj_article` VALUES ('11', '优秀员工评选', '<p>星级员工评选管理办法</p><p>目的为加强企业文化建设，培养和塑造广大员工的集体荣誉感和使命感，不断增强企业向心力和凝聚力，特制订此方案。</p><p>一，评选原则</p><p style=\"text-indent:2em;\">1,公平，公正，公开的原则</p><p style=\"text-indent:2em;\">2,择优录取</p><p style=\"text-indent:2em;\">3,以生产一线员工为主的原则（不含管理人员和临时工）</p><p style=\"text-indent:2em;\"><br /></p><p>二，星级员工的评定标准</p><p style=\"text-indent:2em;\">1,能够严格执行作业流程，做到规范，完整。GMP执行标准率达到100%</p><p style=\"text-indent:2em;\">2,能够严格执行生产操作流程，工作差率为零。</p><p style=\"text-indent:2em;\">3,严格遵守公司的各项规章制度</p><p style=\"text-indent:2em;\">4,能在各项工作中起到带头和表率作用</p><p style=\"text-indent:2em;\">5,能够团结同事，同事内部好评度要超过70%</p><p style=\"text-indent:2em;\">6,个人行为符合公司的员工标准规范。</p><p style=\"text-indent:2em;\"><br /></p><p>三，否决项</p><p style=\"text-indent:2em;\">1,当月非全勤的员工</p><p style=\"text-indent:2em;\">2<span style=\"text-indent:28px;\">,</span>三个月内受到记过处份的员工</p><p style=\"text-indent:2em;\">3,不能积极参加公司组织的各项活动的</p><p style=\"text-indent:2em;\"><br /></p><p>四，星级员工的评选办法</p><p style=\"text-indent:2em;\">1,由各部门负责人根据考核情况推荐本部门符合条件的候选人，于次月5日前组织所有员工对候选人进行民主测评。</p><p style=\"text-indent:2em;\">2<span style=\"text-indent:28px;\">,</span>由车间主任，生产厂长和经理，对候选人进行最后评定。</p><p style=\"text-indent:2em;\">3<span style=\"text-indent:28px;\">,</span>原则上每月会产生一名星级员工。</p><p style=\"text-indent:2em;\"><br /></p><p>五，星级员工的奖励办法</p><p style=\"text-indent:2em;\">1<span style=\"text-indent:28px;\">,</span>凡被评为星级员工可在当月享受1天带薪假期。</p><p style=\"text-indent:2em;\">2<span style=\"text-indent:28px;\">,</span>公司会在内部大力表彰星级员工的事迹，每月将星级员工的照片，名字和先进事迹张贴在本公司的阳光板上。</p><p style=\"text-indent:2em;\">3,星级员工每增加一星将获得20元的现金奖励</p><p style=\"text-indent:2em;\">4,凡被评选为星级员工者，将作为公司后备管理干部的必备条件之一。</p><p><br /></p>', '0', '1', '1', '1517282443', '1517282443', '3', '', '1', '1', '', '4', '1', 'alldept', '', '', '', '0', '0', '0', '0', '', '0', '0');

-- ----------------------------
-- Table structure for `yckj_article_approval`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article_approval`;
CREATE TABLE `yckj_article_approval` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `articleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '新闻id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '签收人id',
  `step` varchar(10) NOT NULL DEFAULT '' COMMENT '签收步骤(1,2,3,4,5对应approval表level1,level2,level3,level4,level5)',
  PRIMARY KEY (`id`),
  KEY `ARTICLEID` (`articleid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_article_approval
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_article_back`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article_back`;
CREATE TABLE `yckj_article_back` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `articleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '操作者UID',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '退回时间',
  `reason` text NOT NULL COMMENT '退回理由',
  PRIMARY KEY (`id`),
  KEY `ARTICLEID` (`articleid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_article_back
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_article_category`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article_category`;
CREATE TABLE `yckj_article_category` (
  `catid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '父分类id',
  `name` char(20) NOT NULL COMMENT '文章分类名称',
  `sort` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `aid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '审批流程id',
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_article_category
-- ----------------------------
INSERT INTO `yckj_article_category` VALUES ('1', '0', '默认分类', '0', '0');
INSERT INTO `yckj_article_category` VALUES ('2', '1', 'IBOS', '1', '0');
INSERT INTO `yckj_article_category` VALUES ('3', '1', '公司制度', '2', '0');
INSERT INTO `yckj_article_category` VALUES ('4', '1', '公司活动', '3', '0');

-- ----------------------------
-- Table structure for `yckj_article_picture`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article_picture`;
CREATE TABLE `yckj_article_picture` (
  `picid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '图片ID ',
  `articleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片所属相册ID ',
  `aid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '附件所属ID ',
  `sort` mediumint(3) NOT NULL DEFAULT '0',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片上传时间戳',
  `postip` varchar(255) NOT NULL DEFAULT '' COMMENT '图片上传ip',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '图片文件名',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '图片标题',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '图片类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片大小',
  `filepath` varchar(255) NOT NULL DEFAULT '' COMMENT '图片路径',
  `thumb` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有缩略图',
  `remote` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片状态 1-审核',
  PRIMARY KEY (`picid`),
  KEY `articleid` (`articleid`,`sort`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='文章图片表';

-- ----------------------------
-- Records of yckj_article_picture
-- ----------------------------
INSERT INTO `yckj_article_picture` VALUES ('1', '9', '4', '0', '0', '::1', '人事档案---统计.png', '', 'png', '323', 'data/attachment/article/201406/12/152749izmygggpnigpzseq.png', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('2', '9', '5', '1', '0', '::1', '日志统计---个人.png', '', 'png', '238', 'data/attachment/article/201406/12/152750ulvdvu51hv1p1bu9.png', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('3', '9', '6', '2', '0', '::1', '日志统计---评阅.png', '', 'png', '333', 'data/attachment/article/201406/12/152750dks4sx49kkecicse.png', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('4', '3', '1', '0', '0', '127.0.*.*', '幻灯片1.JPG', '', 'jpg', '119734', 'data/attachment/article/201406/12/1730558oyoffy3bnelhwfo.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('5', '3', '2', '1', '0', '127.0.*.*', '幻灯片2.JPG', '', 'jpg', '157826', 'data/attachment/article/201406/12/173055rdahnrnh78xmzmer.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('6', '3', '3', '2', '0', '127.0.*.*', '幻灯片3.JPG', '', 'jpg', '81900', 'data/attachment/article/201406/12/173055ddwyz7hhys06dgln.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('7', '3', '4', '3', '0', '127.0.*.*', '幻灯片4.JPG', '', 'jpg', '78143', 'data/attachment/article/201406/12/173056wwllv46lfa47fzks.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('8', '3', '5', '4', '0', '127.0.*.*', '幻灯片5.JPG', '', 'jpg', '83008', 'data/attachment/article/201406/12/173056az6i5f3xpna0p0mf.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('9', '3', '6', '5', '0', '127.0.*.*', '幻灯片6.JPG', '', 'jpg', '96640', 'data/attachment/article/201406/12/173056h1ceffw1hh6nqiq3.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('10', '3', '7', '6', '0', '127.0.*.*', '幻灯片7.JPG', '', 'jpg', '80438', 'data/attachment/article/201406/12/173057p05dpb3xcdp1ypxb.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('11', '3', '8', '7', '0', '127.0.*.*', '幻灯片8.JPG', '', 'jpg', '81389', 'data/attachment/article/201406/12/173057rnncgndghmd3gxzx.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('12', '3', '9', '8', '0', '127.0.*.*', '幻灯片9.JPG', '', 'jpg', '21836', 'data/attachment/article/201406/12/173057n5k665996k3kzl93.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('13', '3', '10', '9', '0', '127.0.*.*', '幻灯片10.JPG', '', 'jpg', '146677', 'data/attachment/article/201406/12/173058m00ao0qy60mpjs0u.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('14', '3', '11', '10', '0', '127.0.*.*', '幻灯片11.JPG', '', 'jpg', '71305', 'data/attachment/article/201406/12/173058lzm8v3sqyq3h2yfs.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('15', '3', '12', '11', '0', '127.0.*.*', '幻灯片12.JPG', '', 'jpg', '114083', 'data/attachment/article/201406/12/1730598anzd54fdzp54dds.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('16', '3', '13', '12', '0', '127.0.*.*', '幻灯片13.JPG', '', 'jpg', '81967', 'data/attachment/article/201406/12/173059e3o373w7o6cjwdjl.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('17', '3', '14', '13', '0', '127.0.*.*', '幻灯片14.JPG', '', 'jpg', '81679', 'data/attachment/article/201406/12/173100q3395y1s4m81txp3.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('18', '3', '15', '14', '0', '127.0.*.*', '幻灯片15.JPG', '', 'jpg', '89303', 'data/attachment/article/201406/12/173100f5zn24mity54ee43.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('19', '3', '16', '15', '0', '127.0.*.*', '幻灯片16.JPG', '', 'jpg', '96159', 'data/attachment/article/201406/12/173101ynbxajltxtm5ncci.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('20', '3', '17', '16', '0', '127.0.*.*', '幻灯片17.JPG', '', 'jpg', '74412', 'data/attachment/article/201406/12/173101adrr9mzh4vav59rz.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('21', '3', '18', '17', '0', '127.0.*.*', '幻灯片18.JPG', '', 'jpg', '86797', 'data/attachment/article/201406/12/1731026ksusfkqytrqgr11.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('22', '3', '19', '18', '0', '127.0.*.*', '幻灯片19.JPG', '', 'jpg', '107482', 'data/attachment/article/201406/12/173102x4kll9klr9w5e54i.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('23', '3', '20', '19', '0', '127.0.*.*', '幻灯片20.JPG', '', 'jpg', '98651', 'data/attachment/article/201406/12/1731032bb2wyhgjengnghb.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('24', '3', '21', '20', '0', '127.0.*.*', '幻灯片21.JPG', '', 'jpg', '89338', 'data/attachment/article/201406/12/1731037rb0gp67f48fcs88.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('25', '3', '22', '21', '0', '127.0.*.*', '幻灯片22.JPG', '', 'jpg', '29482', 'data/attachment/article/201406/12/173104nrdvsrdcg2ir7vsm.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('26', '3', '23', '22', '0', '127.0.*.*', '幻灯片23.JPG', '', 'jpg', '89849', 'data/attachment/article/201406/12/173104lb2o4oyy2tbc0zi4.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('27', '3', '24', '23', '0', '127.0.*.*', '幻灯片24.JPG', '', 'jpg', '105251', 'data/attachment/article/201406/12/173105nvwzn1ordsv2rda2.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('28', '3', '25', '24', '0', '127.0.*.*', '幻灯片25.JPG', '', 'jpg', '75769', 'data/attachment/article/201406/12/173105qcq9h9i3kohfhq29.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('29', '3', '26', '25', '0', '127.0.*.*', '幻灯片26.JPG', '', 'jpg', '88522', 'data/attachment/article/201406/12/1731065ddjaa8mvs25bdwb.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('30', '3', '27', '26', '0', '127.0.*.*', '幻灯片27.JPG', '', 'jpg', '105628', 'data/attachment/article/201406/12/173106ly5yma744ml5zqmg.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('31', '3', '28', '27', '0', '127.0.*.*', '幻灯片28.JPG', '', 'jpg', '100220', 'data/attachment/article/201406/12/173107ab9xwwtmtbt1jpxv.jpg', '1', '0', '0');
INSERT INTO `yckj_article_picture` VALUES ('32', '3', '29', '28', '0', '127.0.*.*', '幻灯片29.JPG', '', 'jpg', '41886', 'data/attachment/article/201406/12/173107wwnqt3wa8w01891n.jpg', '1', '0', '0');

-- ----------------------------
-- Table structure for `yckj_article_reader`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_article_reader`;
CREATE TABLE `yckj_article_reader` (
  `readerid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '读者表id',
  `articleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '阅读者UID',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `readername` varchar(30) NOT NULL,
  PRIMARY KEY (`readerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_article_reader
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_assignment`;
CREATE TABLE `yckj_assignment` (
  `assignmentid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '指派任务id',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '任务主题',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '任务描述',
  `designeeuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '指派人uid',
  `chargeuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '负责人uid',
  `participantuid` text NOT NULL COMMENT '参与者uid,逗号隔开字符串',
  `attachmentid` text NOT NULL COMMENT '附件id',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发起时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '任务状态(0:未读,1:进行中,2:已完成,3:已评价,4:取消)',
  `finishtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '完成时间',
  `stamp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '图章',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  PRIMARY KEY (`assignmentid`),
  KEY `SUBJECT` (`subject`) USING BTREE,
  KEY `DESIGNEEUID` (`designeeuid`) USING BTREE,
  KEY `CHARGEUID` (`chargeuid`) USING BTREE,
  KEY `FINISHTIME` (`finishtime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_assignment
-- ----------------------------
INSERT INTO `yckj_assignment` VALUES ('1', '会议', '对新产品的看法', '1', '2', '3,4', '', '1517282443', '0', '1517278843', '1517286043', '0', '0', '0', '1');

-- ----------------------------
-- Table structure for `yckj_assignment_apply`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_assignment_apply`;
CREATE TABLE `yckj_assignment_apply` (
  `applyid` mediumint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `assignmentid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '指派任务id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '申请人uid',
  `isdelay` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否任务延时申请',
  `delaystarttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '申请延时开始时间',
  `delayendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '申请延时结束时间',
  `delayreason` varchar(255) NOT NULL DEFAULT '' COMMENT '申请延时理由',
  `iscancel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否任务取消申请',
  `cancelreason` varchar(255) NOT NULL DEFAULT '' COMMENT '申请取消理由',
  PRIMARY KEY (`applyid`),
  KEY `ASSIGNMENTID` (`assignmentid`) USING BTREE,
  KEY `ISDELAY` (`isdelay`) USING BTREE,
  KEY `ISCANCEL` (`iscancel`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_assignment_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_assignment_log`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_assignment_log`;
CREATE TABLE `yckj_assignment_log` (
  `logid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `assignmentid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '指派任务id',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '操作人ID',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '记录日志时间',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT '操作人IP地址',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '日志类型：(add-新建,del-删除,edit-修改,view-查看,push-推办任务,finish-完成任务,stamp-评价任务,restart-重启任务,delay-延期,applydelay-申请延期,agreedelay-同意延期,refusedelay-拒绝延期,cancel-取消,applycancel-申请取消,agreecancel-同意取消,refusecancel-拒绝取消)',
  `content` text NOT NULL COMMENT '日志信息',
  PRIMARY KEY (`logid`),
  KEY `ASSIGNMENTID` (`assignmentid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_assignment_log
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_assignment_remind`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_assignment_remind`;
CREATE TABLE `yckj_assignment_remind` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `assignmentid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '指派任务id',
  `calendarid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日程的id',
  `remindtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '提醒时间，时间戳',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '提醒人uid',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '提醒内容',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态。0 未提醒，1 已提醒',
  PRIMARY KEY (`id`),
  KEY `A_ID` (`assignmentid`) USING BTREE,
  KEY `C_ID` (`calendarid`) USING BTREE,
  KEY `U_ID` (`uid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_assignment_remind
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_atme`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_atme`;
CREATE TABLE `yckj_atme` (
  `atid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，@我的编号',
  `module` char(30) NOT NULL COMMENT '所属模块',
  `table` char(15) NOT NULL COMMENT '存储内容的表名',
  `rowid` int(11) NOT NULL DEFAULT '0' COMMENT '模块内含有@的内容的编号',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '被@的用户编号',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '连接地址',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细来源信息描述',
  PRIMARY KEY (`atid`),
  KEY `module2` (`uid`,`table`),
  KEY `module3` (`table`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of yckj_atme
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment`;
CREATE TABLE `yckj_attachment` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `tableid` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '所属表id',
  `downloads` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_0`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_0`;
CREATE TABLE `yckj_attachment_0` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_0
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_1`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_1`;
CREATE TABLE `yckj_attachment_1` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_1
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_2`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_2`;
CREATE TABLE `yckj_attachment_2` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_2
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_3`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_3`;
CREATE TABLE `yckj_attachment_3` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_3
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_4`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_4`;
CREATE TABLE `yckj_attachment_4` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_4
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_5`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_5`;
CREATE TABLE `yckj_attachment_5` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_5
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_6`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_6`;
CREATE TABLE `yckj_attachment_6` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_6
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_7`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_7`;
CREATE TABLE `yckj_attachment_7` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_7
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_8`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_8`;
CREATE TABLE `yckj_attachment_8` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_8
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_9`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_9`;
CREATE TABLE `yckj_attachment_9` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  `attachment` varchar(255) NOT NULL,
  `isimage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`) USING BTREE,
  FULLTEXT KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_9
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_edit`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_edit`;
CREATE TABLE `yckj_attachment_edit` (
  `aid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '当前编辑用户',
  `lastvisit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间戳'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_edit
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_attachment_unused`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_attachment_unused`;
CREATE TABLE `yckj_attachment_unused` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间戳',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment` varchar(255) NOT NULL DEFAULT '' COMMENT '附件真实地址',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '附件描述',
  PRIMARY KEY (`aid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_attachment_unused
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_auth_assignment`;
CREATE TABLE `yckj_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `bizrule` text NOT NULL COMMENT '关联到这个项目的业务逻辑',
  `data` text NOT NULL COMMENT '当执行业务规则的时候所传递的额外的数据',
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_auth_assignment
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_auth_item`;
CREATE TABLE `yckj_auth_item` (
  `name` varchar(64) NOT NULL COMMENT '项目名字',
  `type` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL COMMENT '项目描述',
  `bizrule` text NOT NULL COMMENT '关联到这个项目的业务逻辑',
  `data` text NOT NULL COMMENT '当执行业务规则的时候所传递的额外的数据',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_auth_item
-- ----------------------------
INSERT INTO `yckj_auth_item` VALUES ('article/default/move', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/move', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/cobinding/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/cosync/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/wxbinding/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/im/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/unit/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/credit/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/usergroup/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/optimize/cache', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/date/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/upload/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/sms/manager', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/syscode/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/email/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/security/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/sysstamp/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/approval/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/notify/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/user/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/role/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/position/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/roleadmin/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/nav/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/quicknav/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/login/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/backgroud/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/module/manager', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/permissions/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/update/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/announcement/setup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/database/backup', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/cron/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/upgrade/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('dashboard/service/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/comment/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/comment/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/comment/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/comment/getcommentview', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/preview', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/vote', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/getreader', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/getcount', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/read', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/data/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/data/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/data/preview', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/verify/flowlog', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/verify/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/verify/verify', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/verify/back', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/verify/cancel', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/publish/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/publish/call', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/publish/cancel', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/submit', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/data/edit', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/data/option', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/getcurapproval', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/category/move', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/edit', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/top', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/hightlight', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/save', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/getmove', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('article/default/delete', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/default/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/default/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/default/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/default/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/unfinished/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/unfinished/ajaxentrance', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/finished/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/comment/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/comment/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/comment/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('assignment/unfinished/sublist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/subschedule', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/shareschedule', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/schedule/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/task/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/task/subtask', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/task/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/task/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/task/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/loop/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/loop/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/loop/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('calendar/loop/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/default/ajaxapi', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/default/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/default/printcontact', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/constant/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/deptlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/userlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/groupuserlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/search', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/corp', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/dept', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/user', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('contact/api/hiddenuidarr', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/default/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/default/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/default/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/default/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/share/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/share/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/attention/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/attention/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/attention/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/comment/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/comment/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/comment/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/personal', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/review/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/stats/personal', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('diary/stats/review', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/list/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/list/search', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/folder/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/folder/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/folder/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/folder/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/content/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/content/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/content/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/content/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/content/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/receive', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('email/web/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/default/getdynamic', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/getcate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/personal/ajaxent', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/myshare/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/myshare/getcate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/myshare/share', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/myshare/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/fromshare/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/fromshare/getcate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/fromshare/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/getcate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('file/company/ajaxent', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/getdoclist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/category/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/comment/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/comment/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/comment/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/category/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/category/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/category/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/edit', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('officialdoc/officialdoc/del', '0', '', 'return UserUtil::checkDataPurv($purvId);', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/sendemail', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/resume/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/contact/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/contact/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/contact/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/contact/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/contact/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/interview/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/interview/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/interview/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/interview/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/interview/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/bgchecks/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/bgchecks/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/bgchecks/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/bgchecks/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/bgchecks/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('recruit/stats/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/default/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/default/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/default/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/default/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/type/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/type/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/type/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/comment/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/comment/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/comment/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/addcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/allread', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/delcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/delreport', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/formreport', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getcommentlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getreader', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/savereport', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/showreport', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/usertemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/shoplist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getcount', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getcommentview', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getreviewcomment', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getauthority', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getcharge', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/addtemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/savetemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/formtemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/settemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/deltemplte', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/sorttemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/managertemplate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getpicture', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/personal', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/add', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/review/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/getstamp', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('report/api/setstamp', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/index', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/fetchindexlist', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/showvote', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/showvoteusers', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/vote', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/form/addorupdate', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/form/updateendtime', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/form/del', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/form/show', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/form/edit', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('vote/default/export', '0', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('1', '2', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('2', '2', '', '', 's:0:\"\";');
INSERT INTO `yckj_auth_item` VALUES ('3', '2', '', '', 's:0:\"\";');

-- ----------------------------
-- Table structure for `yckj_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_auth_item_child`;
CREATE TABLE `yckj_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_auth_item_child
-- ----------------------------
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/getcurapproval');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/category/move');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/comment/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/data/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/data/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/data/option');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/data/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/data/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/delete');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/getmove');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/hightlight');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/move');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/read');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/save');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/submit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/top');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/default/vote');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/publish/call');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/publish/cancel');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/publish/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'article/verify/flowlog');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/finished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/unfinished/ajaxentrance');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/unfinished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'assignment/unfinished/sublist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/loop/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/loop/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/loop/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/loop/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/shareschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/schedule/subschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/task/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/task/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/task/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/task/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'calendar/task/subtask');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/corp');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/dept');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/deptlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/groupuserlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/hiddenuidarr');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/search');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/user');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/api/userlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/constant/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/default/ajaxapi');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/default/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'contact/default/printcontact');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/attention/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/attention/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/attention/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/share/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/share/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/stats/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'diary/stats/review');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/content/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/content/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/content/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/content/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/content/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/folder/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/folder/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/folder/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/folder/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/list/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/list/search');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/receive');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'email/web/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/company/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/default/getdynamic');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/fromshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/fromshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/fromshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/myshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/myshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/myshare/share');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/myshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'file/personal/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/category/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/category/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/category/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/getdoclist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'officialdoc/officialdoc/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/bgchecks/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/bgchecks/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/bgchecks/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/bgchecks/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/bgchecks/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/contact/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/contact/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/contact/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/contact/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/contact/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/interview/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/interview/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/interview/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/interview/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/interview/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/sendemail');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/resume/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'recruit/stats/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/addtemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/allread');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/delreport');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/formreport');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getauthority');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getcharge');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getreviewcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/getstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/savereport');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/setstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/shoplist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/showreport');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/api/usertemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/type/add');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/type/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'report/type/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/export');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/fetchindexlist');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/showvote');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/showvoteusers');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/default/vote');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/form/addorupdate');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/form/del');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/form/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/form/show');
INSERT INTO `yckj_auth_item_child` VALUES ('1', 'vote/form/updateendtime');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/category/getcurapproval');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/comment/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/data/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/data/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/data/option');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/data/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/data/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/delete');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/getmove');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/hightlight');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/move');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/read');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/save');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/submit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/top');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/default/vote');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/publish/call');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/publish/cancel');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/publish/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'article/verify/flowlog');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/finished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/unfinished/ajaxentrance');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/unfinished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'assignment/unfinished/sublist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/loop/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/loop/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/loop/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/loop/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/shareschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/schedule/subschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/task/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/task/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/task/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/task/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'calendar/task/subtask');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/corp');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/dept');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/deptlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/groupuserlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/hiddenuidarr');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/search');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/user');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/api/userlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/constant/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/default/ajaxapi');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/default/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'contact/default/printcontact');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/attention/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/attention/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/attention/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/share/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/share/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/stats/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'diary/stats/review');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/content/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/content/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/content/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/content/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/content/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/folder/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/folder/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/folder/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/folder/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/list/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/list/search');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/receive');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'email/web/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/company/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/default/getdynamic');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/fromshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/fromshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/fromshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/myshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/myshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/myshare/share');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/myshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'file/personal/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/getdoclist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'officialdoc/officialdoc/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/bgchecks/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/bgchecks/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/bgchecks/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/bgchecks/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/bgchecks/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/contact/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/contact/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/contact/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/contact/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/contact/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/interview/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/interview/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/interview/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/interview/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/interview/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/sendemail');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'recruit/resume/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/addtemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/allread');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/delreport');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/formreport');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getauthority');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getcharge');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getreviewcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/getstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/savereport');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/setstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/shoplist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/showreport');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/api/usertemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/type/add');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/type/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'report/type/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/export');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/fetchindexlist');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/showvote');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/showvoteusers');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/default/vote');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/form/addorupdate');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/form/del');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/form/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/form/show');
INSERT INTO `yckj_auth_item_child` VALUES ('2', 'vote/form/updateendtime');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/category/getcurapproval');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/comment/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/data/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/data/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/data/option');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/data/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/data/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/delete');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/getmove');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/hightlight');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/move');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/preview');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/read');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/save');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/submit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/top');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/default/vote');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/publish/call');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/publish/cancel');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/publish/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'article/verify/flowlog');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/finished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/unfinished/ajaxentrance');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/unfinished/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'assignment/unfinished/sublist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/loop/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/loop/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/loop/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/loop/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/shareschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/schedule/subschedule');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/task/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/task/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/task/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/task/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'calendar/task/subtask');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/corp');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/dept');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/deptlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/groupuserlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/hiddenuidarr');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/search');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/user');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/api/userlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/constant/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/default/ajaxapi');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/default/export');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'contact/default/printcontact');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/attention/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/attention/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/attention/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/share/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/share/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/stats/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'diary/stats/review');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/content/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/content/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/content/export');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/content/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/content/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/folder/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/folder/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/folder/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/folder/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/list/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/list/search');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/receive');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'email/web/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/company/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/default/getdynamic');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/fromshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/fromshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/fromshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/myshare/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/myshare/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/myshare/share');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/myshare/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/ajaxent');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/getcate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'file/personal/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/category/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/getdoclist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'officialdoc/officialdoc/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/addtemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/allread');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/delreport');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/formreport');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getauthority');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getcharge');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getcommentview');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getcount');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getreader');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getreviewcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/getstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/savereport');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/setstamp');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/shoplist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/showreport');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/api/usertemplate');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/comment/addcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/comment/delcomment');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/comment/getcommentlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/default/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/default/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/default/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/personal');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/review/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/type/add');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/type/del');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'report/type/edit');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/fetchindexlist');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/index');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/show');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/showvote');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/showvoteusers');
INSERT INTO `yckj_auth_item_child` VALUES ('3', 'vote/default/vote');

-- ----------------------------
-- Table structure for `yckj_bg_template`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_bg_template`;
CREATE TABLE `yckj_bg_template` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `desc` varchar(50) NOT NULL DEFAULT '' COMMENT '背景图描述',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否选中',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否系统图片',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '背景图片地址',
  `image_path` varchar(100) NOT NULL DEFAULT '' COMMENT '背景图片地址相对路径',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_bg_template
-- ----------------------------
INSERT INTO `yckj_bg_template` VALUES ('1', '默认背景', '0', '1', 'data/home/template1_bg_big.jpg', '');
INSERT INTO `yckj_bg_template` VALUES ('2', '大气磅礴', '0', '1', 'data/home/template2_bg_big.jpg', '');
INSERT INTO `yckj_bg_template` VALUES ('3', '青葱时光', '0', '1', 'data/home/template3_bg_big.jpg', '');

-- ----------------------------
-- Table structure for `yckj_cache`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_cache`;
CREATE TABLE `yckj_cache` (
  `cachekey` varchar(255) NOT NULL DEFAULT '',
  `cachevalue` mediumblob NOT NULL,
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cachekey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_cache
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_cache_user_detail`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_cache_user_detail`;
CREATE TABLE `yckj_cache_user_detail` (
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'UID',
  `detail` text COMMENT '详细信息',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户状态',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_cache_user_detail
-- ----------------------------
INSERT INTO `yckj_cache_user_detail` VALUES ('1', '{\"uid\":\"1\",\"username\":\"admin\",\"isadministrator\":\"1\",\"deptid\":\"0\",\"positionid\":\"0\",\"roleid\":\"0\",\"upuid\":\"0\",\"groupid\":\"0\",\"jobnumber\":\"\",\"realname\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\",\"password\":\"cf5c03c010684fc8db86062cf1840eff\",\"gender\":\"1\",\"weixin\":\"\",\"mobile\":\"13242930866\",\"email\":\"\",\"status\":\"0\",\"createtime\":\"1517282439\",\"credits\":\"0\",\"newcomer\":\"1\",\"salt\":\"hohgLG\",\"validationemail\":\"0\",\"validationmobile\":\"0\",\"lastchangepass\":\"0\",\"guid\":\"C9586EA1-A4F1-5391-0BDE-3A014CB471C0\",\"birthday\":\"0\",\"telephone\":\"\",\"address\":\"\",\"qq\":\"\",\"bio\":\"\",\"remindsetting\":\"\",\"avatar_big\":\"data\\/avatar\\/noavatar_big.jpg\",\"avatar_middle\":\"data\\/avatar\\/noavatar_middle.jpg\",\"avatar_small\":\"data\\/avatar\\/noavatar_small.jpg\",\"bg_big\":\"data\\/home\\/nobg_big.jpg\",\"bg_middle\":\"\",\"bg_small\":\"data\\/home\\/nobg_small.jpg\",\"group_title\":\"\",\"level\":1,\"upgrade_percent\":0,\"next_group_credit\":0,\"alldeptid\":\"0\",\"deptname\":\"\",\"posname\":\"\",\"allposid\":\"\",\"rolename\":\"\",\"allroleid\":\"\",\"space_url\":\"?r=user\\/home\\/index&uid=1\"}', '0', '0');
INSERT INTO `yckj_cache_user_detail` VALUES ('2', '{\"uid\":\"2\",\"username\":\"\\u949f\\u6c49\\u5510\",\"isadministrator\":\"0\",\"deptid\":\"9\",\"positionid\":\"2\",\"roleid\":\"1\",\"upuid\":\"1\",\"groupid\":\"2\",\"jobnumber\":\"3\",\"realname\":\"\\u949f\\u6c49\\u5510\",\"password\":\"c6d6a14e0a2e39f9eaa8408ea3097a22\",\"gender\":\"1\",\"weixin\":\"\",\"mobile\":\"13658749658\",\"email\":\"test2@ibos.com.cn\",\"status\":\"0\",\"createtime\":\"1392342512\",\"credits\":\"0\",\"newcomer\":\"1\",\"salt\":\"8pg484\",\"validationemail\":\"0\",\"validationmobile\":\"0\",\"lastchangepass\":\"0\",\"guid\":\"DB4EF28D-29AA-90E6-99C2-4BAE34F570E8\",\"birthday\":\"0\",\"telephone\":\"\",\"address\":\"\",\"qq\":\"\",\"bio\":\"\",\"remindsetting\":\"\",\"avatar_big\":\"data\\/avatar\\/noavatar_big.jpg\",\"avatar_middle\":\"data\\/avatar\\/noavatar_middle.jpg\",\"avatar_small\":\"data\\/avatar\\/noavatar_small.jpg\",\"bg_big\":\"data\\/home\\/nobg_big.jpg\",\"bg_middle\":\"\",\"bg_small\":\"data\\/home\\/nobg_small.jpg\",\"group_title\":\"\\u521d\\u5165\\u6c5f\\u6e56\",\"level\":\"2\",\"upgrade_percent\":0,\"next_group_credit\":50,\"alldeptid\":\"9\",\"deptname\":\"\\u5e02\\u573a\\u90e8\",\"allposid\":\"2\",\"posname\":\"\\u90e8\\u95e8\\u7ecf\\u7406\",\"allroleid\":\"1\",\"rolename\":\"\\u7ba1\\u7406\\u5458\",\"space_url\":\"?r=user\\/home\\/index&uid=2\"}', '0', '0');
INSERT INTO `yckj_cache_user_detail` VALUES ('3', '{\"uid\":\"3\",\"username\":\"\\u5f6d\\u541b\\u534e\",\"isadministrator\":\"0\",\"deptid\":\"9\",\"positionid\":\"3\",\"roleid\":\"3\",\"upuid\":\"2\",\"groupid\":\"2\",\"jobnumber\":\"1\",\"realname\":\"\\u5f6d\\u541b\\u534e\",\"password\":\"6c19dbc84e14de92f6a63f32c787233c\",\"gender\":\"1\",\"weixin\":\"\",\"mobile\":\"13586549582\",\"email\":\"test3@ibos.com.cn\",\"status\":\"0\",\"createtime\":\"1392342563\",\"credits\":\"0\",\"newcomer\":\"1\",\"salt\":\"3fegcg\",\"validationemail\":\"0\",\"validationmobile\":\"0\",\"lastchangepass\":\"0\",\"guid\":\"76D8A4B4-8DE0-A41A-6A96-EBACCC2669CD\",\"birthday\":\"0\",\"telephone\":\"\",\"address\":\"\",\"qq\":\"\",\"bio\":\"\",\"remindsetting\":\"\",\"avatar_big\":\"data\\/avatar\\/noavatar_big.jpg\",\"avatar_middle\":\"data\\/avatar\\/noavatar_middle.jpg\",\"avatar_small\":\"data\\/avatar\\/noavatar_small.jpg\",\"bg_big\":\"data\\/home\\/nobg_big.jpg\",\"bg_middle\":\"\",\"bg_small\":\"data\\/home\\/nobg_small.jpg\",\"group_title\":\"\\u521d\\u5165\\u6c5f\\u6e56\",\"level\":\"2\",\"upgrade_percent\":0,\"next_group_credit\":50,\"alldeptid\":\"9\",\"deptname\":\"\\u5e02\\u573a\\u90e8\",\"allposid\":\"3\",\"posname\":\"\\u804c\\u5458\",\"allroleid\":\"3\",\"rolename\":\"\\u666e\\u901a\\u6210\\u5458\",\"space_url\":\"?r=user\\/home\\/index&uid=3\"}', '0', '0');
INSERT INTO `yckj_cache_user_detail` VALUES ('4', '{\"uid\":\"4\",\"username\":\"\\u90d1\\u6d01\",\"isadministrator\":\"0\",\"deptid\":\"9\",\"positionid\":\"3\",\"roleid\":\"3\",\"upuid\":\"2\",\"groupid\":\"2\",\"jobnumber\":\"0\",\"realname\":\"\\u90d1\\u6d01\",\"password\":\"a2f6669d9ec7504c5f1fc99f8c854b61\",\"gender\":\"0\",\"weixin\":\"\",\"mobile\":\"13685423685\",\"email\":\"test4@ibos.com.cn\",\"status\":\"0\",\"createtime\":\"1392342608\",\"credits\":\"0\",\"newcomer\":\"1\",\"salt\":\"wIWk2a\",\"validationemail\":\"0\",\"validationmobile\":\"0\",\"lastchangepass\":\"0\",\"guid\":\"913F0D78-DA6C-EC2A-880B-BF4EFCB3611A\",\"birthday\":\"681321600\",\"telephone\":\"13658246958\",\"address\":\"\\u5e7f\\u5dde\",\"qq\":\"228571845\",\"bio\":\"KIM JAEJOONG\",\"remindsetting\":\"\",\"avatar_big\":\"data\\/avatar\\/noavatar_big.jpg\",\"avatar_middle\":\"data\\/avatar\\/noavatar_middle.jpg\",\"avatar_small\":\"data\\/avatar\\/noavatar_small.jpg\",\"bg_big\":\"data\\/home\\/nobg_big.jpg\",\"bg_middle\":\"\",\"bg_small\":\"data\\/home\\/nobg_small.jpg\",\"group_title\":\"\\u521d\\u5165\\u6c5f\\u6e56\",\"level\":\"2\",\"upgrade_percent\":0,\"next_group_credit\":50,\"alldeptid\":\"9\",\"deptname\":\"\\u5e02\\u573a\\u90e8\",\"allposid\":\"3\",\"posname\":\"\\u804c\\u5458\",\"allroleid\":\"3\",\"rolename\":\"\\u666e\\u901a\\u6210\\u5458\",\"space_url\":\"?r=user\\/home\\/index&uid=4\"}', '0', '0');

-- ----------------------------
-- Table structure for `yckj_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_calendars`;
CREATE TABLE `yckj_calendars` (
  `calendarid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '日程ID',
  `taskid` varchar(50) NOT NULL DEFAULT '' COMMENT '任务ID',
  `isfromdiary` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否来自日志的计划提醒',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '主题',
  `location` varchar(200) NOT NULL DEFAULT '' COMMENT '地点(暂无用)',
  `mastertime` char(10) NOT NULL DEFAULT '' COMMENT '未被实例之前的时间，格式（年月日）：2012-10-01',
  `masterid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '被实例周期性事务的ID',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '详细(未用)',
  `calendartype` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日程类型( 个人 , 部门 )',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `isalldayevent` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否整天日程',
  `hasattachment` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '包含附件',
  `category` varchar(30) NOT NULL DEFAULT '-1' COMMENT '颜色分类',
  `instancetype` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实例类型( 1循环主日程 , 2循环实例 , 异常 , 邀请 )',
  `recurringtype` varchar(255) NOT NULL DEFAULT '' COMMENT '循环类型(年、月、周)',
  `recurringtime` varchar(255) NOT NULL DEFAULT '' COMMENT '循环的具体时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '日程状态（未进行，完成，删除）',
  `recurringbegin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '周期开始时间',
  `recurringend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '周期结束时间',
  `attendees` varchar(255) NOT NULL DEFAULT '' COMMENT '参与人地址',
  `attendeenames` varchar(255) NOT NULL DEFAULT '' COMMENT '参与人姓名',
  `otherattendee` varchar(255) NOT NULL DEFAULT '' COMMENT '其它参与人',
  `upuid` varchar(50) NOT NULL DEFAULT '' COMMENT '添加人帐号(上司或自己)',
  `upname` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `recurringrule` varchar(255) NOT NULL DEFAULT '' COMMENT '循环规则',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `lock` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否被锁定，锁定不能操作，只能看(0为未锁定，1为锁定)',
  PRIMARY KEY (`calendarid`),
  KEY `uid` (`uid`,`starttime`,`endtime`,`masterid`,`taskid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_calendars
-- ----------------------------
INSERT INTO `yckj_calendars` VALUES ('1', '', '0', '跟进客户使用情况', '', '', '0', '', '0', '1517278843', '1517282443', '0', '0', '2', '0', '', '', '1', '0', '0', '', '', '', '3', '', '1517282443', '', '3', '0');
INSERT INTO `yckj_calendars` VALUES ('4', '', '0', '每周会议', '', '', '0', '', '0', '1517282443', '1517286043', '0', '0', '-1', '0', '', '', '0', '0', '0', '', '', '', '3', '', '1517282443', '', '3', '0');
INSERT INTO `yckj_calendars` VALUES ('5', '', '0', '周会', '', '', '0', '', '0', '1517282443', '1517286043', '0', '0', '-1', '0', '', '', '0', '0', '0', '', '', '', '2', '', '1517282443', '', '2', '0');
INSERT INTO `yckj_calendars` VALUES ('9', '', '0', '与李总签订合同', '', '', '0', '', '0', '1517278843', '1517282443', '0', '0', '-1', '0', '', '', '0', '0', '0', '', '', '', '2', '', '1517282443', '', '2', '0');
INSERT INTO `yckj_calendars` VALUES ('7', '', '0', '下午2点准备开会 (钟汉唐)', '', '', '0', '', '0', '1517278843', '1517282443', '0', '0', '-1', '0', '', '', '0', '0', '0', '', '', '', '2', '', '1517282443', '', '4', '0');
INSERT INTO `yckj_calendars` VALUES ('8', '', '0', '把所有客户信息整理 (钟汉唐)', '', '', '0', '', '0', '1517282443', '1517286043', '0', '0', '5', '0', '', '', '0', '0', '0', '', '', '', '2', '', '1517282443', '', '4', '0');

-- ----------------------------
-- Table structure for `yckj_calendar_record`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_calendar_record`;
CREATE TABLE `yckj_calendar_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日程的id',
  `rid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '来自日志的计划id',
  `did` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日志的id',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`) USING BTREE,
  KEY `rid` (`rid`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_calendar_record
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_calendar_rep_record`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_calendar_rep_record`;
CREATE TABLE `yckj_calendar_rep_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日程的id',
  `rid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '来自日志的计划id',
  `repid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '总结的id',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`) USING BTREE,
  KEY `rid` (`rid`) USING BTREE,
  KEY `repid` (`repid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_calendar_rep_record
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_calendar_setup`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_calendar_setup`;
CREATE TABLE `yckj_calendar_setup` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户id',
  `mintime` char(10) NOT NULL DEFAULT '' COMMENT '日程开始时间（小时）',
  `maxtime` char(10) NOT NULL DEFAULT '' COMMENT '日程结束时间（小时）',
  `hiddendays` char(100) NOT NULL DEFAULT '' COMMENT '隐藏日期（星期几）',
  `viewsharing` varchar(50) NOT NULL DEFAULT '' COMMENT '阅读权限分享人员，（1,2,3...）',
  `editsharing` varchar(50) NOT NULL DEFAULT '' COMMENT '编辑权限分享人员，（1,2,3...）',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_calendar_setup
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_comment`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_comment`;
CREATE TABLE `yckj_comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，评论编号',
  `module` char(30) NOT NULL DEFAULT '' COMMENT '所属模块',
  `table` varchar(50) NOT NULL DEFAULT '' COMMENT '被评论的内容所存储的表',
  `rowid` int(11) NOT NULL DEFAULT '0' COMMENT '应用进行评论的内容的编号',
  `moduleuid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '模块内进行评论的内容的作者的UID',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '评论者UID',
  `content` text NOT NULL COMMENT '评论内容',
  `tocid` int(11) NOT NULL DEFAULT '0' COMMENT '被回复的评论的编号',
  `touid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '被回复的评论的作者的UID',
  `data` text NOT NULL COMMENT '所评论的内容的相关参数（序列化存储）',
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记删除（0：没删除，1：已删除）',
  `from` tinyint(2) NOT NULL DEFAULT '0' COMMENT '客户端类型，0：网站；1：手机网页版；2：android；3：iphone',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '该评论回复数',
  `attachmentid` text NOT NULL COMMENT '附件id',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '连接地址',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细来源信息描述',
  PRIMARY KEY (`cid`),
  KEY `module` (`table`,`isdel`,`rowid`) USING BTREE,
  KEY `module2` (`uid`,`isdel`,`table`) USING BTREE,
  KEY `module3` (`uid`,`touid`,`isdel`,`table`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_contact`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_contact`;
CREATE TABLE `yckj_contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `cuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '常联系人id',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `cuid` (`cuid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='常联系人表';

-- ----------------------------
-- Records of yckj_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_contact_hide`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_contact_hide`;
CREATE TABLE `yckj_contact_hide` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `deptid` text NOT NULL COMMENT '发布范围部门',
  `positionid` text NOT NULL COMMENT '发布范围职位',
  `roleid` text NOT NULL COMMENT '发布范围角色',
  `uid` text NOT NULL COMMENT '发布范围人员',
  `column` char(35) NOT NULL DEFAULT '' COMMENT '需要隐藏的字段名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='用户字段隐藏记录表';

-- ----------------------------
-- Records of yckj_contact_hide
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_credit`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_credit`;
CREATE TABLE `yckj_credit` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '积分id',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统自带：1为是；0为否',
  `name` varchar(50) NOT NULL COMMENT '积分名字',
  `initial` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '初始积分',
  `lower` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分下限',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启动：1为启动，0为不启用',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_credit
-- ----------------------------
INSERT INTO `yckj_credit` VALUES ('1', '1', '经验', '0', '0', '1');
INSERT INTO `yckj_credit` VALUES ('2', '1', '金钱', '0', '0', '1');
INSERT INTO `yckj_credit` VALUES ('3', '1', '贡献', '0', '0', '1');

-- ----------------------------
-- Table structure for `yckj_credit_log`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_credit_log`;
CREATE TABLE `yckj_credit_log` (
  `logid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `operation` char(3) NOT NULL DEFAULT '',
  `relatedid` int(10) unsigned NOT NULL DEFAULT '0',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `extcredits1` int(10) NOT NULL DEFAULT '0',
  `extcredits2` int(10) NOT NULL DEFAULT '0',
  `extcredits3` int(10) NOT NULL DEFAULT '0',
  `extcredits4` int(10) NOT NULL DEFAULT '0',
  `extcredits5` int(10) NOT NULL DEFAULT '0',
  `curcredits` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`logid`),
  KEY `uid` (`uid`),
  KEY `operation` (`operation`),
  KEY `relatedid` (`relatedid`),
  KEY `dateline` (`dateline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_credit_log
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_credit_rule`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_credit_rule`;
CREATE TABLE `yckj_credit_rule` (
  `rid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `rulename` varchar(20) NOT NULL DEFAULT '',
  `action` varchar(20) NOT NULL DEFAULT '',
  `cycletype` tinyint(1) NOT NULL DEFAULT '0',
  `cycletime` int(10) NOT NULL DEFAULT '0',
  `rewardnum` smallint(5) NOT NULL DEFAULT '1',
  `norepeat` tinyint(1) NOT NULL DEFAULT '0',
  `extcredits1` int(10) NOT NULL DEFAULT '0',
  `extcredits2` int(10) NOT NULL DEFAULT '0',
  `extcredits3` int(10) NOT NULL DEFAULT '0',
  `extcredits4` int(10) NOT NULL DEFAULT '0',
  `extcredits5` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `action` (`action`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_credit_rule
-- ----------------------------
INSERT INTO `yckj_credit_rule` VALUES ('1', '评论', 'addcomment', '3', '0', '40', '0', '3', '1', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('2', '被评论', 'getcomment', '3', '0', '20', '0', '2', '1', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('3', '删除评论', 'delcomment', '3', '0', '20', '0', '-3', '1', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('4', '每天登录', 'daylogin', '3', '0', '1', '0', '0', '2', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('5', '验证邮箱', 'verifyemail', '1', '0', '1', '0', '10', '10', '2', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('6', '验证手机', 'verifymobile', '1', '0', '1', '0', '10', '10', '2', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('7', '发布微博', 'addweibo', '3', '0', '10', '0', '2', '2', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('8', '删除微博', 'deleteweibo', '3', '0', '10', '0', '-1', '1', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('9', '转发微博', 'forwardweibo', '3', '0', '10', '0', '1', '2', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('10', '微博被转发', 'forwardedweibo', '3', '0', '10', '0', '3', '2', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('11', '顶微博', 'diggweibo', '3', '0', '5', '0', '0', '1', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('12', '微博被顶', 'diggedweibo', '3', '0', '200', '0', '0', '5', '0', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('13', '发表信息公告', 'addarticle', '3', '0', '2', '0', '0', '2', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('14', '完成任务指派', 'finishassignment', '1', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('15', '发表工作日志', 'adddiary', '3', '0', '2', '0', '0', '2', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('16', '发表通知', 'addofficialdoc', '3', '0', '2', '0', '0', '2', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('17', '添加简历', 'addresume', '3', '0', '1', '0', '0', '1', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('18', '发表工作汇报', 'addreport', '3', '0', '2', '0', '0', '2', '1', '0', '0');
INSERT INTO `yckj_credit_rule` VALUES ('19', '写邮件', 'postmail', '3', '0', '4', '0', '0', '2', '1', '0', '0');

-- ----------------------------
-- Table structure for `yckj_credit_rule_log`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_credit_rule_log`;
CREATE TABLE `yckj_credit_rule_log` (
  `clid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则记录id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '操作用户ID',
  `rid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '规则ID',
  `total` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '总积分',
  `cyclenum` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '周期次数',
  `extcredits1` int(10) NOT NULL DEFAULT '0' COMMENT '积分1',
  `extcredits2` int(10) NOT NULL DEFAULT '0' COMMENT '积分2',
  `extcredits3` int(10) NOT NULL DEFAULT '0' COMMENT '积分3',
  `extcredits4` int(10) NOT NULL DEFAULT '0' COMMENT '积分4',
  `extcredits5` int(10) NOT NULL DEFAULT '0' COMMENT '积分5',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '记录时间',
  PRIMARY KEY (`clid`),
  KEY `dateline` (`dateline`),
  KEY `uid` (`uid`,`rid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_credit_rule_log
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_credit_rule_log_field`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_credit_rule_log_field`;
CREATE TABLE `yckj_credit_rule_log_field` (
  `clid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `user` text NOT NULL,
  `app` text NOT NULL,
  PRIMARY KEY (`clid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_credit_rule_log_field
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_cron`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_cron`;
CREATE TABLE `yckj_cron` (
  `cronid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('user','system') NOT NULL DEFAULT 'user',
  `module` varchar(30) NOT NULL DEFAULT '' COMMENT '所属模块',
  `name` char(50) NOT NULL DEFAULT '',
  `filename` char(50) NOT NULL DEFAULT '',
  `lastrun` int(10) unsigned NOT NULL DEFAULT '0',
  `nextrun` int(10) unsigned NOT NULL DEFAULT '0',
  `weekday` tinyint(1) NOT NULL DEFAULT '0',
  `day` tinyint(2) NOT NULL DEFAULT '0',
  `hour` tinyint(2) NOT NULL DEFAULT '0',
  `minute` char(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`cronid`),
  KEY `nextrun` (`available`,`nextrun`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_cron
-- ----------------------------
INSERT INTO `yckj_cron` VALUES ('1', '1', 'system', 'dashboard', '自动同步 IBOS 绑定酷办公用户列表', 'CronAutoSync.php', '1517282453', '1517364000', '-1', '-1', '10', '0');
INSERT INTO `yckj_cron` VALUES ('2', '1', 'system', 'dashboard', '自动补丁', 'CronAutoPatch.php', '1517282453', '1517364000', '-1', '-1', '10', '0');
INSERT INTO `yckj_cron` VALUES ('3', '1', 'system', 'message', '更新企业QQ授权有效期', 'CronUpdateBQQToken.php', '1517282444', '1517760000', '1', '-1', '0', '0');
INSERT INTO `yckj_cron` VALUES ('4', '1', 'system', 'message', '发送通用提醒', 'CronSentNoifyAlarm.php', '1517287742', '1517287800', '-1', '-1', '-1', '*/1');
INSERT INTO `yckj_cron` VALUES ('5', '1', 'system', 'user', '清空本月在线时间', 'CronOnlinetimeMonthly.php', '1517282444', '1517414400', '-1', '1', '0', '0');
INSERT INTO `yckj_cron` VALUES ('6', '1', 'system', 'assignment', '任务指派定时提醒', 'CronAssignmentTimer.php', '1517287742', '1517287800', '-1', '-1', '-1', '*/1');
INSERT INTO `yckj_cron` VALUES ('7', '1', 'system', 'calendar', '日程提醒', 'CronCalendarRemind.php', '1517284804', '1517288400', '-1', '-1', '-1', '0');
INSERT INTO `yckj_cron` VALUES ('8', '1', 'system', 'file', '清空文件柜回收站15天前的文件', 'CronFileTrash.php', '1517282450', '1517328000', '-1', '-1', '0', '0');
INSERT INTO `yckj_cron` VALUES ('9', '1', 'system', 'recruit', '每日招聘统计', 'CronRecruitStatistics.php', '1517282451', '1517328000', '-1', '-1', '0', '0');

-- ----------------------------
-- Table structure for `yckj_department`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_department`;
CREATE TABLE `yckj_department` (
  `deptid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `deptname` char(20) NOT NULL DEFAULT '' COMMENT '部门名称',
  `pid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门ID',
  `manager` mediumint(8) NOT NULL DEFAULT '0' COMMENT '部门主管',
  `leader` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `subleader` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tel` char(15) NOT NULL DEFAULT '' COMMENT '部门电话',
  `fax` char(15) NOT NULL DEFAULT '' COMMENT '部门传真',
  `addr` char(100) NOT NULL DEFAULT '' COMMENT '部门地址',
  `func` char(255) NOT NULL DEFAULT '' COMMENT '部门职能',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序ID',
  `isbranch` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否作为分支机构',
  PRIMARY KEY (`deptid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_department
-- ----------------------------
INSERT INTO `yckj_department` VALUES ('1', '广州', '0', '0', '0', '0', '', '', '', '', '1', '0');
INSERT INTO `yckj_department` VALUES ('2', '运营中心', '1', '0', '0', '0', '', '', '', '', '2', '0');
INSERT INTO `yckj_department` VALUES ('3', '客服部', '1', '0', '0', '0', '', '', '', '', '3', '0');
INSERT INTO `yckj_department` VALUES ('4', '行政人事部', '1', '0', '0', '0', '', '', '', '', '4', '0');
INSERT INTO `yckj_department` VALUES ('5', '财务部', '1', '0', '0', '0', '', '', '', '', '5', '0');
INSERT INTO `yckj_department` VALUES ('6', '深圳', '0', '0', '0', '0', '', '', '', '', '6', '0');
INSERT INTO `yckj_department` VALUES ('7', '项目中心', '6', '0', '0', '0', '', '', '', '', '7', '0');
INSERT INTO `yckj_department` VALUES ('8', '售前支持部', '6', '0', '0', '0', '', '', '', '', '8', '0');
INSERT INTO `yckj_department` VALUES ('9', '市场部', '1', '2', '1', '0', '18265487593', '020-51346852', '广州', '市场销售', '9', '0');

-- ----------------------------
-- Table structure for `yckj_department_binding`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_department_binding`;
CREATE TABLE `yckj_department_binding` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `deptid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '部门ID',
  `bindvalue` text NOT NULL COMMENT '绑定的值',
  `app` char(30) NOT NULL COMMENT '绑定的类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deptidandapp` (`deptid`,`app`),
  KEY `deptid` (`deptid`),
  KEY `app` (`app`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_department_binding
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_department_related`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_department_related`;
CREATE TABLE `yckj_department_related` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `deptid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '部门id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_department_related
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_diary`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary`;
CREATE TABLE `yckj_diary` (
  `diaryid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `diarytime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '日志添加的当日时间',
  `nextdiarytime` int(10) NOT NULL DEFAULT '0' COMMENT '下一个日志添加时间',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `content` text NOT NULL COMMENT '日志内容',
  `attachmentid` text NOT NULL COMMENT '附件ID',
  `shareuid` text NOT NULL COMMENT '分享id',
  `readeruid` text NOT NULL COMMENT '阅读人员',
  `remark` text NOT NULL COMMENT '备注',
  `stamp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '图章',
  `isreview` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已评阅',
  `attention` text NOT NULL COMMENT '谁关注了这篇日志',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  PRIMARY KEY (`diaryid`),
  UNIQUE KEY `ID` (`diaryid`) USING BTREE,
  KEY `USER_ID` (`uid`) USING BTREE,
  KEY `DIA_DATE` (`diarytime`) USING BTREE,
  KEY `DIA_TIME` (`addtime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary
-- ----------------------------
INSERT INTO `yckj_diary` VALUES ('1', '2', '1517155200', '1517241600', '1517282443', '<p><span style=\"color:#58585c;line-height:21px;font-family:宋体;font-size:14px;background-color:#ffffff;\">“被业界称为用户体验最好的协同管理平台-IBOS”独具匠心的将类社区中的积分体系及微博、微信朋友圈@功能导入管理软件中，有效地引导员工积极参与内部协同办公平台的使用和促进员工互动。下面，我们就具体分析一下，如何基于IBOS的将类社区中的积分体系及微博、微信朋友圈@功能让企业管理平台活跃起来</span></p>', '', '', '', '', '0', '0', '', '0');
INSERT INTO `yckj_diary` VALUES ('2', '3', '1517241600', '1517155200', '1517282443', '<p>1、区域市场日常沟通及市场项目支持</p><p>2、区域市场活动策划与推广配合展会、会议营销、合作会议等</p><p>&nbsp;</p>', '', '', '2', '', '1', '1', '', '0');

-- ----------------------------
-- Table structure for `yckj_diary_attention`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary_attention`;
CREATE TABLE `yckj_diary_attention` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '登陆用户UID',
  `auid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '关注哪个用户的UID',
  PRIMARY KEY (`id`),
  KEY `USER_ID` (`uid`) USING BTREE,
  KEY `AT_USER_ID` (`auid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary_attention
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_diary_direct`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary_direct`;
CREATE TABLE `yckj_diary_direct` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `direct` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否设置只看直属下属,1为是,0为否',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary_direct
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_diary_record`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary_record`;
CREATE TABLE `yckj_diary_record` (
  `recordid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `diaryid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日志ID',
  `content` char(255) NOT NULL DEFAULT '' COMMENT '记录内容',
  `uid` int(8) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '完成标记(0为未完成1为已完成',
  `planflag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '计划标记,1为原计划、0为计划外)',
  `schedule` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '进度',
  `plantime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '计划执行的时间',
  `timeremind` char(10) NOT NULL DEFAULT '' COMMENT '设置时间提醒',
  PRIMARY KEY (`recordid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary_record
-- ----------------------------
INSERT INTO `yckj_diary_record` VALUES ('1', '2', '市场费用的管控与发布', '3', '0', '1', '0', '1517155200', '');
INSERT INTO `yckj_diary_record` VALUES ('2', '1', '招聘面试', '2', '1', '0', '10', '1517241600', '');
INSERT INTO `yckj_diary_record` VALUES ('3', '1', '广告投放业务', '2', '1', '0', '10', '1517241600', '');
INSERT INTO `yckj_diary_record` VALUES ('4', '1', '外出考察学习', '2', '1', '0', '10', '1517241600', '');
INSERT INTO `yckj_diary_record` VALUES ('5', '1', '签约及交付事宜', '2', '0', '1', '0', '1517155200', '');
INSERT INTO `yckj_diary_record` VALUES ('6', '1', '咨询回复及跟进', '2', '0', '1', '0', '1517155200', '');

-- ----------------------------
-- Table structure for `yckj_diary_share`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary_share`;
CREATE TABLE `yckj_diary_share` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `deftoid` text NOT NULL COMMENT '分享给谁',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary_share
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_diary_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_diary_statistics`;
CREATE TABLE `yckj_diary_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `diaryid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '日志ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '登陆用户UID',
  `stamp` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '图章id',
  `integration` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `scoretime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评分时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `DIARY_ID` (`diaryid`) USING BTREE,
  KEY `USER_ID` (`uid`) USING BTREE,
  KEY `SCORE_TIME` (`scoretime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_diary_statistics
-- ----------------------------
INSERT INTO `yckj_diary_statistics` VALUES ('1', '1', '2', '1', '2', '1517278843');

-- ----------------------------
-- Table structure for `yckj_doc`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc`;
CREATE TABLE `yckj_doc` (
  `docid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `subject` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `author` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '作者',
  `approver` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '审批人',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `clickcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数',
  `attachmentid` text NOT NULL COMMENT '附件ID',
  `docno` text NOT NULL COMMENT '通知号',
  `commentstatus` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '评论状态，1为开启0为关闭',
  `catid` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '文章状态，1为公开2为审核3为草稿',
  `deptid` text NOT NULL COMMENT '阅读范围部门',
  `positionid` text NOT NULL COMMENT '阅读范围职位',
  `roleid` text NOT NULL COMMENT '阅读范围角色',
  `uid` text NOT NULL COMMENT '阅读范围人员',
  `readers` text NOT NULL COMMENT '阅读人uid',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶，1代表置顶，0为不置顶',
  `toptime` int(10) NOT NULL DEFAULT '0' COMMENT '置顶时间',
  `topendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '置顶过期时间',
  `ishighlight` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否高亮',
  `highlightstyle` char(50) NOT NULL DEFAULT '' COMMENT '高亮样式',
  `highlightendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '高亮过期时间',
  `rcid` smallint(5) NOT NULL DEFAULT '0' COMMENT '套红id',
  `ccdeptid` text NOT NULL COMMENT '抄送部门id',
  `ccpositionid` text NOT NULL COMMENT '抄送职位id',
  `ccroleid` text NOT NULL COMMENT '抄送角色id',
  `ccuid` text NOT NULL COMMENT '抄送uid',
  `version` smallint(5) NOT NULL DEFAULT '1' COMMENT '版本号',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  PRIMARY KEY (`docid`),
  KEY `SUBJECT` (`subject`) USING BTREE,
  KEY `PROVIDER` (`author`) USING BTREE,
  KEY `NEWS_TIME` (`addtime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_doc_approval`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc_approval`;
CREATE TABLE `yckj_doc_approval` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `docid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '通知id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '签收人id',
  `step` varchar(10) NOT NULL DEFAULT '' COMMENT '签收步骤(1,2,3,4,5对应approval表level1,level2,level3,level4,level5)',
  PRIMARY KEY (`id`),
  KEY `DOCID` (`docid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc_approval
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_doc_back`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc_back`;
CREATE TABLE `yckj_doc_back` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `docid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '操作者UID',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '退回时间',
  `reason` text NOT NULL COMMENT '退回理由',
  PRIMARY KEY (`id`),
  KEY `ARTICLEID` (`docid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc_back
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_doc_category`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc_category`;
CREATE TABLE `yckj_doc_category` (
  `catid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '父分类id',
  `name` char(20) NOT NULL COMMENT '文章分类名称',
  `sort` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `aid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '审批流程id',
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc_category
-- ----------------------------
INSERT INTO `yckj_doc_category` VALUES ('1', '0', '默认分类', '0', '0');
INSERT INTO `yckj_doc_category` VALUES ('2', '1', 'IBOS', '1', '0');

-- ----------------------------
-- Table structure for `yckj_doc_reader`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc_reader`;
CREATE TABLE `yckj_doc_reader` (
  `readerid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '读者表id',
  `docid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '阅读者UID',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `readername` varchar(30) NOT NULL,
  `issign` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已签收:1为已签收，0为未签收',
  `signtime` int(10) NOT NULL DEFAULT '0' COMMENT '签收时间',
  `frommobile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否签收与手机端',
  PRIMARY KEY (`readerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc_reader
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_doc_version`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_doc_version`;
CREATE TABLE `yckj_doc_version` (
  `versionid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '//版本号id',
  `docid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `content` text NOT NULL COMMENT '内容',
  `author` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '作者uid',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `clickcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数',
  `commentstatus` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '评论状态，1为开启0为关闭',
  `catid` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '文章状态，1为公开2为审核3为草稿',
  `deptid` text NOT NULL COMMENT '阅读范围部门',
  `positionid` text NOT NULL COMMENT '阅读范围职位',
  `roleid` text NOT NULL COMMENT '阅读范围角色',
  `uid` text NOT NULL COMMENT '阅读范围人员',
  `readers` text NOT NULL COMMENT '阅读人uid',
  `rcid` smallint(5) NOT NULL DEFAULT '0' COMMENT '套红id',
  `ccdeptid` text NOT NULL COMMENT '抄送部门id',
  `ccpositionid` text NOT NULL COMMENT '抄送职位id',
  `ccroleid` text NOT NULL COMMENT '抄送角色id',
  `ccuid` text NOT NULL COMMENT '抄送uid',
  `version` smallint(5) NOT NULL DEFAULT '1' COMMENT '版本号',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  `editor` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '修改人uid',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '修改理由',
  PRIMARY KEY (`versionid`),
  KEY `NEWS_TIME` (`addtime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_doc_version
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_email`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email`;
CREATE TABLE `yckj_email` (
  `emailid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `toid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '发送给谁',
  `isread` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已阅读 (0为未阅读，1为已阅读)',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记(0为未删除，1为已删除)',
  `fid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '文件夹id',
  `bodyid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '邮件主体id',
  `isreceipt` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '回执标识 (0未回执，1已回执，2不回执)',
  `ismark` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否标为待办 (0为不待办，1为标记待办)',
  `isweb` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否外部邮件 (0不是，1是)',
  PRIMARY KEY (`emailid`),
  UNIQUE KEY `emailid` (`emailid`) USING BTREE,
  KEY `bodyid` (`bodyid`) USING BTREE,
  KEY `toid` (`toid`) USING BTREE,
  KEY `fid` (`fid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_email
-- ----------------------------
INSERT INTO `yckj_email` VALUES ('2', '1', '0', '0', '1', '2', '0', '0', '0');
INSERT INTO `yckj_email` VALUES ('3', '2', '0', '0', '1', '2', '0', '0', '0');
INSERT INTO `yckj_email` VALUES ('4', '3', '0', '0', '1', '2', '0', '0', '0');

-- ----------------------------
-- Table structure for `yckj_email_1`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email_1`;
CREATE TABLE `yckj_email_1` (
  `emailid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `toid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '发送给谁',
  `isread` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已阅读 (0为未阅读，1为已阅读)',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记(0为未删除，1为已删除)',
  `fid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '文件夹id',
  `bodyid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '邮件主体id',
  `isreceipt` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '回执标识 (0未回执，1已回执，2不回执)',
  `ismark` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否标为待办 (0为不待办，1为标记待办)',
  `isweb` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否外部邮件 (0不是，1是)',
  PRIMARY KEY (`emailid`),
  UNIQUE KEY `emailid` (`emailid`) USING BTREE,
  KEY `bodyid` (`bodyid`) USING BTREE,
  KEY `toid` (`toid`) USING BTREE,
  KEY `fid` (`fid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_email_1
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_email_body`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email_body`;
CREATE TABLE `yckj_email_body` (
  `bodyid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '邮件内容ID',
  `fromid` text NOT NULL COMMENT '发件人ID',
  `toids` text NOT NULL COMMENT '收件人',
  `copytoids` text NOT NULL COMMENT '抄送人',
  `secrettoids` text NOT NULL COMMENT '密送人',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `content` mediumtext NOT NULL COMMENT '邮件内容',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `attachmentid` text NOT NULL COMMENT '附件id',
  `issend` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已发送(0为未发送，1为已发送)',
  `important` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '重要程度 (0:一般1:重要2:紧急)',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '邮件大小，单位为字节',
  `fromwebmail` varchar(255) NOT NULL COMMENT '外部邮件的来源',
  `towebmail` text NOT NULL COMMENT '发送的外部邮箱，以分号;为分隔',
  `issenderdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发送者删除(0为未删除1为已删除)',
  `isneedreceipt` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要回执(0为不需要1为需要)',
  PRIMARY KEY (`bodyid`),
  UNIQUE KEY `bodyid` (`bodyid`),
  KEY `sendtime` (`sendtime`) USING BTREE,
  KEY `subject` (`subject`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_email_body
-- ----------------------------
INSERT INTO `yckj_email_body` VALUES ('2', '4', '1,2,3', '', '', 'OA协同管理平台', '<p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">互联网浪潮席卷全球与企业信息化日益普及，企业步入互联网、信息化时代。几乎所有企业对于管理软件的需求都在加深，不少企业开始将企业2.0的理念融入到自己的企业文化中。我们一起来看看年度中国优秀CIO们在互联网时代都是怎样选择协同OA的?</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">好的OA协同管理平台不仅仅是拥有几个特色功能，需要站在企业战略高度透视即将迎来的企业大数据时代赋予CIO们“高效管理”、“提升运营”、“推进转型”、“开拓创新”这样的新使命，还需要迎合一线使用者的使用感受。因此简洁易用，功能强大、安全稳定、可拓展、零风险是年度中国优秀CIO们考虑的主要因素：</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">1、简洁易用。系统不能界面布置让你眼花缭乱，让使用者要大量时间花费在熟悉系统、查询操作上面，时间长了发现大部分模块不好用，让系统最终成为发发通知、公告的摆设，而核心管理流程很难在OA协同管理平台中跑得通，最后成了一个食之无味、弃之可惜的‘鸡肋’!</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">2、功能强大。中小企业关注的自由协作、信息发布和文档管理;中型企业关注的职能分工、流程规范;大型企业关注的效率提升、业务督办;集团企业更关注的集中管控、风险控制。好的OA协同管理平台不仅要满足现阶段，还要考虑企业未来3-5年的发展。</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">3、安全稳定。OA协同管理平台使用之后，大量企业知识，管理数据都会存在系统里。平台的安全性、稳定性对于企业来说，是非常重要的。</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">4、可拓展。企业在持续不断的发展过程中，战略、组织架构、业务流程、角色权限都在不断的变化，尤其是基础管理需求在不断的变化，其中一些变化可以通过流程优化和表单调整完成，但还有相当多的变化无法通过产品化模块实现，因此能够开放源码的产品为首选。</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">5、零风险。众多网站的调查报告指出，国内企业使用OA协同管理平台的失败率高达60%，如何让企业在OA协同管理平台选型时避免错误，从而提升项目成功率，成为关键。</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">伴随着互联网技术的演进，以及IT与业务融合进程的深入，新时代的CIO们不断被赋予新的使命和职责。核心团队来自腾讯、谷歌埃森哲等一流企业的IBOS产品团队，在协同软件中融入强互联网元素，用了五年时间将IBOS协同管理平台变得如QQ空间、FACEBOOK、淘宝一样简单易用，你会发现只要IT人员会WORD表格，EXCEL函数，三分钟设计工作流程。同时博思协创提供的30天内无条件全额退款，终身质保服务将实施风险降到最低。</p><p style=\"color:#58585c;padding:0px;list-style:none;text-indent:2em;line-height:25px;font-family:simsun;background-color:#ffffff;margin-top:10px;margin-bottom:0px;\">今天中国的企业正处在一个充满波动性和复杂性的特殊阶段，CIO们不仅需要在基础的技术方面出色地完成任务，还需要从数据中挖掘有价值的洞察力，并最终将其转化为创新的催化剂。借助IBOS协同管理平台，中国优秀CIO们轻松肩负“高效管理”、“提升运营”、“推进转型”、“开拓创新”这样时代赋予的新使命。</p><p><br /></p>', '1517282443', '', '0', '0', '4706', '', '', '0', '0');

-- ----------------------------
-- Table structure for `yckj_email_body_1`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email_body_1`;
CREATE TABLE `yckj_email_body_1` (
  `bodyid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '邮件内容ID',
  `fromid` text NOT NULL COMMENT '发件人ID',
  `toids` text NOT NULL COMMENT '收件人',
  `copytoids` text NOT NULL COMMENT '抄送人',
  `secrettoids` text NOT NULL COMMENT '密送人',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `content` mediumtext NOT NULL COMMENT '邮件内容',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `attachmentid` text NOT NULL COMMENT '附件id',
  `issend` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已发送(0为未发送，1为已发送)',
  `important` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '重要程度 (0:一般1:重要2:紧急)',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '邮件大小，单位为字节',
  `fromwebmail` varchar(255) NOT NULL COMMENT '外部邮件的来源',
  `towebmail` text NOT NULL COMMENT '发送的外部邮箱，以分号;为分隔',
  `issenderdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发送者删除(0为未删除1为已删除)',
  `isneedreceipt` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要回执(0为不需要1为需要)',
  PRIMARY KEY (`bodyid`),
  UNIQUE KEY `bodyid` (`bodyid`),
  KEY `sendtime` (`sendtime`) USING BTREE,
  KEY `subject` (`subject`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_email_body_1
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_email_folder`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email_folder`;
CREATE TABLE `yckj_email_folder` (
  `fid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统自带；1为是；0为否',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `name` char(100) NOT NULL DEFAULT '' COMMENT '邮箱名',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `webid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '外部邮箱文件夹id',
  PRIMARY KEY (`fid`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `sort` (`sort`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of yckj_email_folder
-- ----------------------------
INSERT INTO `yckj_email_folder` VALUES ('1', '1', '0', 'inbox', '0', '0');
INSERT INTO `yckj_email_folder` VALUES ('2', '1', '0', 'draft', '0', '0');
INSERT INTO `yckj_email_folder` VALUES ('3', '1', '0', 'send', '0', '0');
INSERT INTO `yckj_email_folder` VALUES ('4', '1', '0', 'del', '0', '0');

-- ----------------------------
-- Table structure for `yckj_email_web`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_email_web`;
CREATE TABLE `yckj_email_web` (
  `webid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '外部邮箱id',
  `address` varchar(255) NOT NULL COMMENT 'emai地址',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '邮箱密码',
  `smtpserver` varchar(255) NOT NULL COMMENT '发信服务器地址',
  `smtpport` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'smtp服务器端口',
  `smtpssl` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'smtp服务器是否使用ssl',
  `server` varchar(255) NOT NULL DEFAULT '0' COMMENT '服务器地址',
  `port` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '服务器端口',
  `ssl` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '服务器是否使用ssl链接',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户id',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '发信昵称',
  `lastrectime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后接收时间',
  `fid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文件夹id',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认发信箱(0不是，1是)',
  PRIMARY KEY (`webid`),
  UNIQUE KEY `webid` (`webid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_email_web
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_failedip`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_failedip`;
CREATE TABLE `yckj_failedip` (
  `ip` char(7) NOT NULL DEFAULT '',
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `count` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ip`,`lastupdate`),
  KEY `lastupdate` (`lastupdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_failedip
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_failedlogin`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_failedlogin`;
CREATE TABLE `yckj_failedlogin` (
  `ip` char(15) NOT NULL DEFAULT '',
  `username` char(32) NOT NULL DEFAULT '',
  `count` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ip`,`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_failedlogin
-- ----------------------------
INSERT INTO `yckj_failedlogin` VALUES ('172.16.1.50', 'admin', '0', '1517282444');

-- ----------------------------
-- Table structure for `yckj_feed`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_feed`;
CREATE TABLE `yckj_feed` (
  `feedid` int(11) NOT NULL AUTO_INCREMENT COMMENT '动态ID',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '产生动态的用户UID',
  `type` char(50) DEFAULT NULL COMMENT 'feed类型.由发表feed的程序控制',
  `module` char(30) NOT NULL DEFAULT 'microblog' COMMENT 'feed来源的module',
  `table` varchar(50) NOT NULL DEFAULT 'feed' COMMENT '关联资源所在的表',
  `rowid` int(11) NOT NULL DEFAULT '0' COMMENT '关联的来源ID（如文章的id）',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '产生时间戳',
  `isdel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 默认为0',
  `from` tinyint(2) NOT NULL DEFAULT '0' COMMENT '客户端类型，0：网站；1：手机网页版；2：android；3：iphone',
  `commentcount` int(10) unsigned DEFAULT '0' COMMENT '评论数',
  `repostcount` int(10) DEFAULT '0' COMMENT '分享数',
  `commentallcount` int(10) DEFAULT '0' COMMENT '全部评论数目',
  `diggcount` int(11) unsigned DEFAULT '0' COMMENT '赞数',
  `isrepost` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否转发 0-否  1-是',
  `view` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '微博可见性 (0全公司可见 1仅自己可见 2我所在的部门可见 3自定义范围)',
  `userid` text NOT NULL COMMENT '可见用户ID',
  `deptid` text NOT NULL COMMENT '可见部门ID',
  `positionid` text NOT NULL COMMENT '可见岗位ID',
  `roleid` text NOT NULL COMMENT '可见角色ID',
  PRIMARY KEY (`feedid`),
  KEY `isdel` (`isdel`,`ctime`),
  KEY `uid` (`uid`,`isdel`,`ctime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_feed
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_feed_data`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_feed_data`;
CREATE TABLE `yckj_feed_data` (
  `feedid` int(11) unsigned NOT NULL COMMENT '关联feed表，feedid',
  `feeddata` text COMMENT '关联feed表，动态数据，序列化保存',
  `clientip` char(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '客户端IP',
  `feedcontent` text COMMENT '纯微博内容',
  `fromdata` text COMMENT '微博来源',
  PRIMARY KEY (`feedid`),
  KEY `feedid` (`feedid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_feed_data
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_feed_digg`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_feed_digg`;
CREATE TABLE `yckj_feed_digg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `feedid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产生动态的ID',
  `ctime` int(11) DEFAULT '0' COMMENT '赞的时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of yckj_feed_digg
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_feed_topic`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_feed_topic`;
CREATE TABLE `yckj_feed_topic` (
  `topicid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '话题ID',
  `topicname` varchar(150) NOT NULL COMMENT '话题标题',
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '关联的动态数',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `lock` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `domain` varchar(100) NOT NULL COMMENT '个性化地址',
  `recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `recommend_time` int(11) DEFAULT '0' COMMENT '推荐时间',
  `des` text COMMENT '详细内容',
  `outlink` varchar(100) DEFAULT NULL COMMENT '关联链接',
  `pic` varchar(255) DEFAULT NULL COMMENT '关联图片',
  `essence` tinyint(1) DEFAULT '0' COMMENT '是否精华',
  `note` varchar(255) DEFAULT NULL COMMENT '摘要',
  `topic_user` varchar(255) DEFAULT NULL COMMENT '话题人物推荐',
  PRIMARY KEY (`topicid`),
  KEY `count` (`count`),
  KEY `recommend` (`recommend`,`lock`,`count`),
  KEY `name` (`topicname`,`count`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_feed_topic
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_feed_topic_link`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_feed_topic_link`;
CREATE TABLE `yckj_feed_topic_link` (
  `linkid` int(11) NOT NULL AUTO_INCREMENT,
  `feedid` int(11) NOT NULL DEFAULT '0' COMMENT '动态ID',
  `topicid` int(11) NOT NULL DEFAULT '0' COMMENT '话题ID',
  `type` varchar(255) NOT NULL DEFAULT '0' COMMENT '动态类型ID',
  PRIMARY KEY (`linkid`),
  KEY `topic_type` (`topicid`,`type`),
  KEY `weibo` (`feedid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_feed_topic_link
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file`;
CREATE TABLE `yckj_file` (
  `fid` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `pid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '所属文件夹id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型：0文件，1文件夹',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `idpath` text NOT NULL COMMENT '层级标识',
  `size` int(10) NOT NULL DEFAULT '0' COMMENT '大小(单位kb)',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已删除到回收站',
  `belong` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '所属：0个人文件，1公司文件',
  `cloudid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '对应云盘id（0表示本地）',
  PRIMARY KEY (`fid`),
  KEY `PID` (`pid`) USING BTREE,
  KEY `UID` (`uid`) USING BTREE,
  KEY `ISDEL` (`isdel`) USING BTREE,
  KEY `BELONG` (`belong`) USING BTREE,
  KEY `CLOUDID` (`cloudid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件柜文件/文件夹信息表';

-- ----------------------------
-- Records of yckj_file
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_capacity`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_capacity`;
CREATE TABLE `yckj_file_capacity` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `size` int(10) NOT NULL DEFAULT '0' COMMENT '大小(单位MB)',
  `deptids` text NOT NULL COMMENT '部门ids',
  `posids` text NOT NULL COMMENT '岗位ids',
  `uids` text NOT NULL COMMENT 'uids',
  `roleids` text NOT NULL COMMENT '角色ids',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `ADDTIME` (`addtime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='容量分配表';

-- ----------------------------
-- Records of yckj_file_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_cloud_set`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_cloud_set`;
CREATE TABLE `yckj_file_cloud_set` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `server` varchar(45) NOT NULL DEFAULT '' COMMENT '服务器',
  `keyid` char(20) NOT NULL DEFAULT '' COMMENT '验证keyid',
  `keysecret` char(50) NOT NULL DEFAULT '' COMMENT '验证码',
  `endpoint` varchar(255) NOT NULL DEFAULT '' COMMENT '终端请求连接',
  `bucket` varchar(100) NOT NULL DEFAULT '' COMMENT '云盘唯一标识符',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开通成功',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='云盘设置';

-- ----------------------------
-- Records of yckj_file_cloud_set
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_detail`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_detail`;
CREATE TABLE `yckj_file_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '关联file表fid',
  `attachmentid` int(11) NOT NULL DEFAULT '0' COMMENT '附件id',
  `filetype` char(10) NOT NULL DEFAULT '' COMMENT '文件类型',
  `mark` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图地址',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fid` (`fid`),
  KEY `MARK` (`mark`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件柜文件信息表';

-- ----------------------------
-- Records of yckj_file_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_dir_access`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_dir_access`;
CREATE TABLE `yckj_file_dir_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `fid` int(11) NOT NULL COMMENT '文件夹id',
  `rdeptids` text NOT NULL COMMENT '可读的部门ids',
  `rposids` text NOT NULL COMMENT '可读的岗位ids',
  `ruids` text NOT NULL COMMENT '可读的uids',
  `rroleids` text NOT NULL COMMENT '可读的角色roleids',
  `wdeptids` text NOT NULL COMMENT '可写的部门ids',
  `wposids` text NOT NULL COMMENT '可写的岗位ids',
  `wuids` text NOT NULL COMMENT '可写的uids',
  `wroleids` text NOT NULL COMMENT '可写的roleids',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fid` (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司文件柜读写权限（包括云盘权限）';

-- ----------------------------
-- Records of yckj_file_dir_access
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_dynamic`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_dynamic`;
CREATE TABLE `yckj_file_dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '关联file表fid',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '所属用户id',
  `content` text NOT NULL COMMENT '动态内容',
  `touids` text NOT NULL COMMENT '用户id串',
  `todeptids` text NOT NULL COMMENT '部门id串',
  `toposids` text NOT NULL COMMENT '岗位id串',
  `time` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `FID` (`fid`) USING BTREE,
  KEY `TIME` (`time`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件柜动态表';

-- ----------------------------
-- Records of yckj_file_dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_reader`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_reader`;
CREATE TABLE `yckj_file_reader` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `fromuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '共享人uid',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '读者uid',
  `viewtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '查看时间',
  PRIMARY KEY (`id`),
  KEY `UID` (`uid`) USING BTREE,
  KEY `VIEWTIME` (`viewtime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件柜文件共享已读信息存储';

-- ----------------------------
-- Records of yckj_file_reader
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_share`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_share`;
CREATE TABLE `yckj_file_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '共享id',
  `fid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联file表fid',
  `fromuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '共享人uid',
  `touids` text NOT NULL COMMENT '共享给哪些uid',
  `todeptids` text NOT NULL COMMENT '共享给哪些部门',
  `toposids` text NOT NULL COMMENT '共享给哪些岗位',
  `toroleids` text NOT NULL COMMENT '共享给哪些岗位角色',
  `uptime` int(10) NOT NULL DEFAULT '0' COMMENT '更新共享时间（只针对添加文件而记录的更新时间）',
  PRIMARY KEY (`id`),
  KEY `FID` (`fid`) USING BTREE,
  KEY `FROMUID` (`fromuid`) USING BTREE,
  KEY `UPTIME` (`uptime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件柜共享';

-- ----------------------------
-- Records of yckj_file_share
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_file_trash`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_file_trash`;
CREATE TABLE `yckj_file_trash` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '关联file表fid',
  `deltime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `FID` (`fid`) USING BTREE,
  KEY `DELTIME` (`deltime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回收站';

-- ----------------------------
-- Records of yckj_file_trash
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_ipbanned`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_ipbanned`;
CREATE TABLE `yckj_ipbanned` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `ip1` smallint(3) NOT NULL DEFAULT '0',
  `ip2` smallint(3) NOT NULL DEFAULT '0',
  `ip3` smallint(3) NOT NULL DEFAULT '0',
  `ip4` smallint(3) NOT NULL DEFAULT '0',
  `admin` varchar(15) NOT NULL DEFAULT '',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `expiration` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_ipbanned
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_login_template`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_login_template`;
CREATE TABLE `yckj_login_template` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否启用',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否系统图片',
  `image` varchar(100) NOT NULL COMMENT '背景图片地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_login_template
-- ----------------------------
INSERT INTO `yckj_login_template` VALUES ('1', '1', '1', 'data/login/ibos_login1.jpg');
INSERT INTO `yckj_login_template` VALUES ('2', '0', '1', 'data/login/ibos_login2.jpg');

-- ----------------------------
-- Table structure for `yckj_log_2018`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_log_2018`;
CREATE TABLE `yckj_log_2018` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(128) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `logtime` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_log_2018
-- ----------------------------
INSERT INTO `yckj_log_2018` VALUES ('1', 'admincp', 'module.dashboard.cobinding.index', '1517282445', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/cobinding\\/index; isInstall=1; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('2', 'admincp', 'module.dashboard.default.index', '1517282879', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/default\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('3', 'admincp', 'module.dashboard.index.index', '1517282880', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/index\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('4', 'admincp', 'module.dashboard.index.getsecurity', '1517282880', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"getsecurity\",\"param\":\"GET={r=dashboard\\/index\\/getsecurity; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('5', 'admincp', 'module.dashboard.user.index', '1517282888', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/user\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('6', 'admincp', 'module.dashboard.user.getuserlist', '1517282889', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"getuserlist\",\"param\":\"GET={r=dashboard\\/user\\/getuserlist; }; POST={draw=2; columns={0={searchable=true; orderable=false; search={regex=false; }; }; 1={searchable=true; orderable=false; search={regex=false; }; }; 2={data=realname; searchable=true; orderable=false; search={regex=false; }; }; 3={data=deptname; searchable=true; orderable=false; search={regex=false; }; }; 4={data=rolename; searchable=true; orderable=false; search={regex=false; }; }; 5={data=status; searchable=true; orderable=false; search={regex=false; }; }; 6={data=mobile; searchable=true; orderable=false; search={regex=false; }; }; 7={searchable=true; orderable=false; search={regex=false; }; }; }; length=10; search={regex=false; }; }; \"}');
INSERT INTO `yckj_log_2018` VALUES ('7', 'admincp', 'module.dashboard.position.index', '1517282890', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/position\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('8', 'admincp', 'module.dashboard.position.getpositionlist', '1517282890', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"getpositionlist\",\"param\":\"GET={r=dashboard\\/position\\/getpositionlist; }; POST={draw=2; columns={0={searchable=true; orderable=false; search={regex=false; }; }; 1={data=posname; searchable=true; orderable=false; search={regex=false; }; }; 2={data=catname; searchable=true; orderable=false; search={regex=false; }; }; 3={data=num; searchable=true; orderable=false; search={regex=false; }; }; 4={data=operate; searchable=true; orderable=false; search={regex=false; }; }; }; length=10; search={regex=false; }; }; \"}');
INSERT INTO `yckj_log_2018` VALUES ('9', 'admincp', 'module.dashboard.positioncategory.index', '1517282890', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/positioncategory\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('10', 'admincp', 'module.dashboard.approval.index', '1517282894', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/approval\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('11', 'admincp', 'module.dashboard.cron.index', '1517282898', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/cron\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('12', 'admincp', 'module.dashboard.dashboard.index', '1517282928', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=article\\/dashboard\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('13', 'admincp', 'module.dashboard.dashboard.index', '1517282933', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=report\\/dashboard\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('14', 'admincp', 'module.dashboard.user.index', '1517282934', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"index\",\"param\":\"GET={r=dashboard\\/user\\/index; }; POST={}; \"}');
INSERT INTO `yckj_log_2018` VALUES ('15', 'admincp', 'module.dashboard.user.getuserlist', '1517282935', '{\"user\":\"admin\",\"ip\":\"172.16.1.50\",\"action\":\"getuserlist\",\"param\":\"GET={r=dashboard\\/user\\/getuserlist; }; POST={draw=2; columns={0={searchable=true; orderable=false; search={regex=false; }; }; 1={searchable=true; orderable=false; search={regex=false; }; }; 2={data=realname; searchable=true; orderable=false; search={regex=false; }; }; 3={data=deptname; searchable=true; orderable=false; search={regex=false; }; }; 4={data=rolename; searchable=true; orderable=false; search={regex=false; }; }; 5={data=status; searchable=true; orderable=false; search={regex=false; }; }; 6={data=mobile; searchable=true; orderable=false; search={regex=false; }; }; 7={searchable=true; orderable=false; search={regex=false; }; }; }; length=10; search={regex=false; }; }; \"}');

-- ----------------------------
-- Table structure for `yckj_menu`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_menu`;
CREATE TABLE `yckj_menu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `name` char(20) NOT NULL COMMENT '菜单显示名字',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `m` char(20) NOT NULL DEFAULT '' COMMENT '模块',
  `c` char(20) NOT NULL DEFAULT '' COMMENT '控制器',
  `a` char(20) NOT NULL DEFAULT '' COMMENT '动作',
  `param` char(100) NOT NULL DEFAULT '' COMMENT '要传递的参数',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否禁用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_menu
-- ----------------------------
INSERT INTO `yckj_menu` VALUES ('1', '企业微博', '0', 'weibo', 'dashboard', 'setup', '', '0', '0');
INSERT INTO `yckj_menu` VALUES ('2', '信息中心', '0', 'article', 'dashboard', 'index', '', '10', '0');
INSERT INTO `yckj_menu` VALUES ('3', '日程安排', '0', 'calendar', 'dashboard', 'index', '', '11', '0');
INSERT INTO `yckj_menu` VALUES ('4', '通讯录', '0', 'contact', 'dashboard', 'index', '', '2', '0');
INSERT INTO `yckj_menu` VALUES ('5', '工作日志', '0', 'diary', 'dashboard', 'index', '', '10', '0');
INSERT INTO `yckj_menu` VALUES ('6', '邮件', '0', 'email', 'dashboard', 'index', '', '2', '0');
INSERT INTO `yckj_menu` VALUES ('7', '文件柜', '0', 'file', 'dashboard', 'index', '', '4', '0');
INSERT INTO `yckj_menu` VALUES ('8', '通知公告', '0', 'officialdoc', 'dashboard', 'index', '', '10', '0');
INSERT INTO `yckj_menu` VALUES ('9', '招聘管理', '0', 'recruit', 'dashboard', 'index', '', '11', '0');
INSERT INTO `yckj_menu` VALUES ('10', '汇报', '0', 'report', 'dashboard', 'index', '', '10', '0');
INSERT INTO `yckj_menu` VALUES ('11', '统计', '0', 'statistics', 'dashboard', 'index', '', '0', '0');
INSERT INTO `yckj_menu` VALUES ('12', '调查投票', '0', 'vote', 'dashboard', 'index', '', '15', '0');

-- ----------------------------
-- Table structure for `yckj_menu_common`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_menu_common`;
CREATE TABLE `yckj_menu_common` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `module` varchar(30) NOT NULL DEFAULT '' COMMENT '模块',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '模块名',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '链接地址',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单显示描述',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `iscommon` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已设置为常用菜单',
  `iscustom` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是自定义的快捷导航',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `openway` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '打开连接方式:0为新窗口,1当前页打开',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'icon文件名,在./data/icon/目录下',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='首页通用菜单设置';

-- ----------------------------
-- Records of yckj_menu_common
-- ----------------------------
INSERT INTO `yckj_menu_common` VALUES ('1', 'weibo', '微博', 'weibo/home/index', '企业微博', '8', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('2', 'article', '信息中心', 'article/default/index', '提供企业新闻信息发布', '5', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('3', 'assignment', '任务指派', 'assignment/unfinished/index', '提供企业工作任务指派', '10', '0', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('4', 'calendar', '日程', 'calendar/schedule/index', '提供企业工作日程安排', '4', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('5', 'diary', '日志', 'diary/default/index', '提供企业工作日志发布', '2', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('6', 'email', '邮件', 'email/list/index', '提供企业内外邮件沟通', '1', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('7', 'file', '文件柜', 'file/default/index', '提供企业文件存储', '13', '0', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('8', 'officialdoc', '通知公告', 'officialdoc/officialdoc/index', '提供企业通知信息发布，以及版本记录', '6', '1', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('9', 'recruit', '招聘', 'recruit/resume/index', '提供企业招聘信息', '0', '0', '0', '0', '0', '');
INSERT INTO `yckj_menu_common` VALUES ('10', 'report', '汇报', 'report/default/index', '提供企业工作汇报', '3', '1', '0', '0', '0', '');

-- ----------------------------
-- Table structure for `yckj_menu_personal`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_menu_personal`;
CREATE TABLE `yckj_menu_personal` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '设置菜单的uid',
  `common` text NOT NULL COMMENT '常用菜单，按顺序逗号隔开的menu_common模块id名称',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='首页个人菜单设置';

-- ----------------------------
-- Records of yckj_menu_personal
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_message_content`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_message_content`;
CREATE TABLE `yckj_message_content` (
  `messageid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '私信内对话ID',
  `listid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '私信ID',
  `fromuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '会话发布者UID',
  `content` text COMMENT '会话内容',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，0：否；1：是',
  `mtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会话发布时间',
  PRIMARY KEY (`messageid`),
  KEY `listid` (`listid`,`isdel`,`mtime`),
  KEY `listid2` (`listid`,`mtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_message_content
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_message_list`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_message_list`;
CREATE TABLE `yckj_message_list` (
  `listid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '私信ID',
  `fromuid` mediumint(8) unsigned NOT NULL COMMENT '私信发起者UID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '私信类别，1：一对一；2：多人',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `usernum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '参与者数量',
  `minmax` varchar(255) DEFAULT NULL COMMENT '参与者UID正序排列，以下划线“_”链接',
  `mtime` int(11) unsigned NOT NULL COMMENT '发起时间戳',
  `lastmessage` text NOT NULL COMMENT '最新的一条会话',
  PRIMARY KEY (`listid`),
  KEY `type` (`type`),
  KEY `min_max` (`minmax`),
  KEY `fromuid` (`fromuid`,`mtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_message_list
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_message_user`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_message_user`;
CREATE TABLE `yckj_message_user` (
  `listid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '私信ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `new` smallint(8) NOT NULL DEFAULT '0' COMMENT '未读消息数',
  `messagenum` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '消息总数',
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '该参与者最后会话时间',
  `listctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '私信最后会话时间',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除（假删）',
  PRIMARY KEY (`listid`,`uid`),
  KEY `new` (`new`),
  KEY `ctime` (`ctime`),
  KEY `listctime` (`listctime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of yckj_message_user
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_module`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_module`;
CREATE TABLE `yckj_module` (
  `module` varchar(30) NOT NULL COMMENT '模块',
  `name` varchar(20) NOT NULL COMMENT '模块名',
  `url` varchar(100) NOT NULL COMMENT '链接地址',
  `iscore` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否核心模块',
  `version` varchar(50) NOT NULL DEFAULT '' COMMENT '版本号',
  `icon` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图标文件存在与否',
  `category` varchar(30) NOT NULL COMMENT '模块所属分类',
  `description` varchar(255) NOT NULL COMMENT '模块描述',
  `config` text NOT NULL COMMENT '模块配置，数组形式',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序 ',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已禁用',
  `installdate` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装日期',
  `updatedate` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新日期',
  PRIMARY KEY (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_module
-- ----------------------------
INSERT INTO `yckj_module` VALUES ('main', '核心模块', 'main/default/index', '1', '1.0', '1', '', '系统核心模块。提供IBOS程序核心流程初始化及处理', '{\"param\":{\"name\":\"\\u6838\\u5fc3\\u6a21\\u5757\",\"description\":\"\\u7cfb\\u7edf\\u6838\\u5fc3\\u6a21\\u5757\\u3002\\u63d0\\u4f9bIBOS\\u7a0b\\u5e8f\\u6838\\u5fc3\\u6d41\\u7a0b\\u521d\\u59cb\\u5316\\u53ca\\u5904\\u7406\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"indexShow\":{\"widget\":[\"main\\/voiceConference\"],\"link\":\"main\\/default\\/index\"},\"icon\":1,\"category\":\"\",\"url\":\"main\\/default\\/index\"},\"config\":{\"modules\":{\"main\":{\"class\":\"application\\\\modules\\\\main\\\\MainModule\"}},\"components\":{\"setting\":{\"class\":\"application\\\\modules\\\\main\\\\components\\\\Setting\"},\"session\":{\"class\":\"application\\\\modules\\\\main\\\\components\\\\Session\"},\"cron\":{\"class\":\"application\\\\modules\\\\main\\\\components\\\\Cron\"},\"process\":{\"class\":\"application\\\\modules\\\\main\\\\components\\\\Process\"},\"errorHandler\":{\"errorAction\":\"main\\/default\\/error\"},\"messages\":{\"extensionPaths\":{\"main\":\"application.modules.main.language\"}}}},\"behaviors\":{\"onInitModule\":{\"class\":\"application\\\\modules\\\\main\\\\behaviors\\\\InitMainModule\"}}}', '0', '0', '1517282439', '0');
INSERT INTO `yckj_module` VALUES ('dashboard', '后台管理', '', '1', '1.0', '1', '权限列表', '提供IBOS后台管理所需功能', '{\"param\":{\"name\":\"\\u540e\\u53f0\\u7ba1\\u7406\",\"category\":\"\\u6743\\u9650\\u5217\\u8868\",\"description\":\"\\u63d0\\u4f9bIBOS\\u540e\\u53f0\\u7ba1\\u7406\\u6240\\u9700\\u529f\\u80fd\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"url\":\"\"},\"config\":{\"modules\":{\"dashboard\":{\"class\":\"application\\\\modules\\\\dashboard\\\\DashboardModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"dashboard\":\"application.modules.dashboard.language\"}}}},\"authorization\":{\"cobindings\":{\"type\":\"node\",\"name\":\"\\u9177\\u529e\\u516c\\u7ed1\\u5b9a\",\"group\":\"\\u7ed1\\u5b9a\",\"controllerMap\":{\"cobinding\":[\"index\"],\"cosync\":[\"index\"]}},\"wxbindings\":{\"type\":\"node\",\"name\":\"\\u5fae\\u4fe1\\u4f01\\u4e1a\\u53f7\\u7ed1\\u5b9a\",\"group\":\"\\u7ed1\\u5b9a\",\"controllerMap\":{\"wxbinding\":[\"index\"]}},\"ims\":{\"type\":\"node\",\"name\":\"\\u5373\\u65f6\\u901a\\u8baf\\u7ed1\\u5b9a\",\"group\":\"\\u7ed1\\u5b9a\",\"controllerMap\":{\"im\":[\"index\"]}},\"globals\":{\"type\":\"node\",\"name\":\"\\u5355\\u4f4d\\u7ba1\\u7406\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"unit\":[\"index\"]}},\"credits\":{\"type\":\"node\",\"name\":\"\\u79ef\\u5206\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"credit\":[\"setup\"]}},\"usergroups\":{\"type\":\"node\",\"name\":\"\\u7528\\u6237\\u7ec4\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"usergroup\":[\"index\"]}},\"optimizes\":{\"type\":\"node\",\"name\":\"\\u6027\\u80fd\\u4f18\\u5316\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"optimize\":[\"cache\"]}},\"dates\":{\"type\":\"node\",\"name\":\"\\u65f6\\u95f4\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"date\":[\"index\"]}},\"uploads\":{\"type\":\"node\",\"name\":\"\\u4e0a\\u4f20\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"upload\":[\"index\"]}},\"smss\":{\"type\":\"node\",\"name\":\"\\u624b\\u673a\\u77ed\\u4fe1\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"sms\":[\"manager\"]}},\"syscodes\":{\"type\":\"node\",\"name\":\"\\u7cfb\\u7edf\\u4ee3\\u7801\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"syscode\":[\"index\"]}},\"emails\":{\"type\":\"node\",\"name\":\"\\u90ae\\u4ef6\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"email\":[\"setup\"]}},\"securitys\":{\"type\":\"node\",\"name\":\"\\u5b89\\u5168\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"security\":[\"setup\"]}},\"sysstamps\":{\"type\":\"node\",\"name\":\"\\u7cfb\\u7edf\\u56fe\\u7ae0\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"sysstamp\":[\"index\"]}},\"approvals\":{\"type\":\"node\",\"name\":\"\\u5ba1\\u6279\\u6d41\\u7a0b\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"approval\":[\"index\"]}},\"notifys\":{\"type\":\"node\",\"name\":\"\\u63d0\\u9192\\u7b56\\u7565\\u8bbe\\u7f6e\",\"group\":\"\\u5168\\u5c40\",\"controllerMap\":{\"notify\":[\"setup\"]}},\"users\":{\"type\":\"node\",\"name\":\"\\u90e8\\u95e8\\u7528\\u6237\\u7ba1\\u7406\",\"group\":\"\\u7528\\u6237\",\"controllerMap\":{\"user\":[\"index\"]}},\"roles\":{\"type\":\"node\",\"name\":\"\\u89d2\\u8272\\u6743\\u9650\\u7ba1\\u7406\",\"group\":\"\\u7528\\u6237\",\"controllerMap\":{\"role\":[\"index\"]}},\"positions\":{\"type\":\"node\",\"name\":\"\\u5c97\\u4f4d\\u7ba1\\u7406\",\"group\":\"\\u7528\\u6237\",\"controllerMap\":{\"position\":[\"index\"]}},\"roleadmins\":{\"type\":\"node\",\"name\":\"\\u7ba1\\u7406\\u5458\\u7ba1\\u7406\",\"group\":\"\\u7528\\u6237\",\"controllerMap\":{\"roleadmin\":[\"index\"]}},\"navs\":{\"type\":\"node\",\"name\":\"\\u9876\\u90e8\\u5bfc\\u822a\\u8bbe\\u7f6e\",\"group\":\"\\u754c\\u9762\",\"controllerMap\":{\"nav\":[\"index\"]}},\"quicknavs\":{\"type\":\"node\",\"name\":\"\\u5feb\\u6377\\u5bfc\\u822a\\u8bbe\\u7f6e\",\"group\":\"\\u754c\\u9762\",\"controllerMap\":{\"quicknav\":[\"index\"]}},\"logins\":{\"type\":\"node\",\"name\":\"\\u767b\\u5f55\\u9875\\u80cc\\u666f\\u8bbe\\u7f6e\",\"group\":\"\\u754c\\u9762\",\"controllerMap\":{\"login\":[\"index\"]}},\"backgrounds\":{\"type\":\"node\",\"name\":\"\\u7cfb\\u7edf\\u80cc\\u666f\\u8bbe\\u7f6e\",\"group\":\"\\u754c\\u9762\",\"controllerMap\":{\"backgroud\":[\"index\"]}},\"modules\":{\"type\":\"node\",\"name\":\"\\u6a21\\u5757\\u7ba1\\u7406\",\"group\":\"\\u6a21\\u5757\",\"controllerMap\":{\"module\":[\"manager\"]}},\"permissionss\":{\"type\":\"node\",\"name\":\"\\u6743\\u9650\\u8bbe\\u7f6e\",\"group\":\"\\u6a21\\u5757\",\"controllerMap\":{\"permissions\":[\"setup\"]}},\"updates\":{\"type\":\"node\",\"name\":\"\\u66f4\\u65b0\\u7f13\\u5b58\",\"group\":\"\\u7ba1\\u7406\",\"controllerMap\":{\"update\":[\"index\"]}},\"announcements\":{\"type\":\"node\",\"name\":\"\\u7cfb\\u7edf\\u516c\\u544a\",\"group\":\"\\u7ba1\\u7406\",\"controllerMap\":{\"announcement\":[\"setup\"]}},\"databases\":{\"type\":\"node\",\"name\":\"\\u6570\\u636e\\u5e93\",\"group\":\"\\u7ba1\\u7406\",\"controllerMap\":{\"database\":[\"backup\"]}},\"crons\":{\"type\":\"node\",\"name\":\"\\u8ba1\\u5212\\u4efb\\u52a1\",\"group\":\"\\u7ba1\\u7406\",\"controllerMap\":{\"cron\":[\"index\"]}},\"upgrades\":{\"type\":\"node\",\"name\":\"\\u5728\\u7ebf\\u5347\\u7ea7\",\"group\":\"\\u7ba1\\u7406\",\"controllerMap\":{\"upgrade\":[\"index\"]}},\"services\":{\"type\":\"node\",\"name\":\"\\u4e91\\u670d\\u52a1\",\"group\":\"\\u670d\\u52a1\",\"controllerMap\":{\"service\":[\"index\"]}}}}', '0', '0', '1517282439', '0');
INSERT INTO `yckj_module` VALUES ('message', '消息模块', '', '1', '1.0', '1', '', '系统核心模块。提供IBOS程序消息体系的建立。包括@人，提醒,评论，私信，微博及动态', '{\"param\":{\"name\":\"\\u6d88\\u606f\\u6a21\\u5757\",\"description\":\"\\u7cfb\\u7edf\\u6838\\u5fc3\\u6a21\\u5757\\u3002\\u63d0\\u4f9bIBOS\\u7a0b\\u5e8f\\u6d88\\u606f\\u4f53\\u7cfb\\u7684\\u5efa\\u7acb\\u3002\\u5305\\u62ec@\\u4eba\\uff0c\\u63d0\\u9192,\\u8bc4\\u8bba\\uff0c\\u79c1\\u4fe1\\uff0c\\u5fae\\u535a\\u53ca\\u52a8\\u6001\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"message\":{\"class\":\"application\\\\modules\\\\message\\\\MessageModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"message\":\"application.modules.message.language\"}}}}}', '0', '0', '1517282439', '0');
INSERT INTO `yckj_module` VALUES ('user', '用户模块', '', '1', '1.0', '1', '', '核心模块。提供用户管理，登录验证等功能', '{\"param\":{\"name\":\"\\u7528\\u6237\\u6a21\\u5757\",\"description\":\"\\u6838\\u5fc3\\u6a21\\u5757\\u3002\\u63d0\\u4f9b\\u7528\\u6237\\u7ba1\\u7406\\uff0c\\u767b\\u5f55\\u9a8c\\u8bc1\\u7b49\\u529f\\u80fd\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"user\":{\"class\":\"application\\\\modules\\\\user\\\\UserModule\"}},\"components\":{\"user\":{\"allowAutoLogin\":1,\"class\":\"application\\\\modules\\\\user\\\\components\\\\User\",\"loginUrl\":[\"user\\/default\\/login\"]},\"messages\":{\"extensionPaths\":{\"user\":\"application.modules.user.language\"}}}}}', '0', '0', '1517282439', '0');
INSERT INTO `yckj_module` VALUES ('department', '部门模块', '', '1', '1.0', '1', '', '提供IBOS部门管理所需功能', '{\"param\":{\"name\":\"\\u90e8\\u95e8\\u6a21\\u5757\",\"description\":\"\\u63d0\\u4f9bIBOS\\u90e8\\u95e8\\u7ba1\\u7406\\u6240\\u9700\\u529f\\u80fd\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"department\":{\"class\":\"application\\\\modules\\\\department\\\\DepartmentModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"department\":\"application.modules.department.language\"}}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('position', '岗位模块', '', '1', '1.0', '1', '', '提供IBOS岗位管理所需功能', '{\"param\":{\"name\":\"\\u5c97\\u4f4d\\u6a21\\u5757\",\"description\":\"\\u63d0\\u4f9bIBOS\\u5c97\\u4f4d\\u7ba1\\u7406\\u6240\\u9700\\u529f\\u80fd\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"position\":{\"class\":\"application\\\\modules\\\\position\\\\PositionModule\"}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('weibo', '企业微博', '', '2', '1.0', '1', '', '企业微博', '{\"param\":{\"name\":\"\\u4f01\\u4e1a\\u5fae\\u535a\",\"description\":\"\\u4f01\\u4e1a\\u5fae\\u535a\",\"author\":\"banyan @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"weibo\":{\"class\":\"application\\\\modules\\\\weibo\\\\WeiboModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"weibo\":\"application.modules.weibo.language\"}}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('role', '角色模块', '', '1', '1.0', '1', '', '提供IBOS角色管理所需功能', '{\"param\":{\"name\":\"\\u89d2\\u8272\\u6a21\\u5757\",\"description\":\"\\u63d0\\u4f9bIBOS\\u89d2\\u8272\\u7ba1\\u7406\\u6240\\u9700\\u529f\\u80fd\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"position\":{\"class\":\"application\\\\modules\\\\role\\\\RoleModule\"}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('article', '信息中心', 'article/default/index', '0', '1.0', '1', '信息中心', '提供企业新闻信息发布', '{\"param\":{\"name\":\"\\u4fe1\\u606f\\u4e2d\\u5fc3\",\"category\":\"\\u4fe1\\u606f\\u4e2d\\u5fc3\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u65b0\\u95fb\\u4fe1\\u606f\\u53d1\\u5e03\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":[\"article\\/article\"],\"link\":\"article\\/default\\/index\"},\"icon\":1,\"url\":\"article\\/default\\/index\"},\"config\":{\"modules\":{\"article\":{\"class\":\"application\\\\modules\\\\article\\\\ArticleModule\"}},\"components\":{\"ArticleVote\":{\"class\":\"application\\\\modules\\\\article\\\\components\\\\ArticleVote\"},\"messages\":{\"extensionPaths\":{\"article\":\"application.modules.article.language\"}}}},\"authorization\":{\"view\":{\"type\":\"node\",\"name\":\"\\u4fe1\\u606f\\u67e5\\u770b\",\"group\":\"\\u65b0\\u95fb\",\"controllerMap\":{\"comment\":[\"getcommentlist\",\"addcomment\",\"delcomment\",\"getcommentview\"],\"category\":[\"index\"],\"default\":[\"index\",\"show\",\"preview\",\"vote\",\"getreader\",\"getcount\",\"read\"],\"data\":[\"index\",\"show\",\"preview\"],\"verify\":[\"flowlog\"]}},\"verify\":{\"type\":\"node\",\"name\":\"\\u4fe1\\u606f\\u5ba1\\u6838\",\"group\":\"\\u65b0\\u95fb\",\"controllerMap\":{\"verify\":[\"index\",\"verify\",\"back\",\"index\",\"cancel\"]}},\"publish\":{\"type\":\"node\",\"name\":\"\\u4fe1\\u606f\\u53d1\\u5e03\",\"group\":\"\\u65b0\\u95fb\",\"controllerMap\":{\"publish\":[\"index\",\"call\",\"cancel\"],\"default\":[\"add\",\"vote\",\"submit\"],\"data\":[\"edit\",\"option\"],\"category\":[\"getcurapproval\"]}},\"category\":{\"type\":\"node\",\"name\":\"\\u5206\\u7c7b\\u7ba1\\u7406\",\"group\":\"\\u65b0\\u95fb\",\"controllerMap\":{\"category\":[\"add\",\"edit\",\"del\",\"move\",\"index\"]}},\"manager\":{\"type\":\"data\",\"name\":\"\\u5185\\u5bb9\\u7ba1\\u7406\",\"group\":\"\\u65b0\\u95fb\",\"node\":{\"edit\":{\"name\":\"\\u7f16\\u8f91\",\"controllerMap\":{\"default\":[\"edit\",\"top\",\"move\",\"hightlight\",\"save\",\"getmove\",\"vote\",\"submit\"],\"data\":[\"edit\",\"option\"]}},\"del\":{\"name\":\"\\u5220\\u9664\",\"controllerMap\":{\"default\":[\"delete\"]}}},\"controllerMap\":{\"category\":[\"add\",\"edit\",\"del\"]}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('assignment', '任务指派', 'assignment/unfinished/index', '0', '1.0', '1', '任务指派', '提供企业工作任务指派', '{\"param\":{\"name\":\"\\u4efb\\u52a1\\u6307\\u6d3e\",\"category\":\"\\u4efb\\u52a1\\u6307\\u6d3e\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5de5\\u4f5c\\u4efb\\u52a1\\u6307\\u6d3e\",\"author\":\"gzhzh @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":[\"assignment\\/assignment\"],\"link\":\"assignment\\/unfinished\\/index\"},\"icon\":1,\"url\":\"assignment\\/unfinished\\/index\"},\"config\":{\"modules\":{\"assignment\":{\"class\":\"application\\\\modules\\\\assignment\\\\AssignmentModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"assignment\":\"application.modules.assignment.language\"}}}},\"authorization\":{\"assignment\":{\"type\":\"node\",\"name\":\"\\u4efb\\u52a1\\u7ba1\\u7406\",\"group\":\"\\u4efb\\u52a1\\u6307\\u6d3e\",\"controllerMap\":{\"default\":[\"add\",\"edit\",\"del\",\"show\"],\"unfinished\":[\"index\",\"ajaxentrance\"],\"finished\":[\"index\"],\"comment\":[\"getcommentlist\",\"addcomment\",\"delcomment\"]}},\"review\":{\"type\":\"node\",\"name\":\"\\u67e5\\u770b\\u4e0b\\u5c5e\\u4efb\\u52a1\",\"group\":\"\\u4efb\\u52a1\\u6307\\u6d3e\",\"controllerMap\":{\"unfinished\":[\"sublist\"]}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('calendar', '日程', 'calendar/schedule/index', '0', '1.0', '1', '日程', '提供企业工作日程安排。', '{\"param\":{\"name\":\"\\u65e5\\u7a0b\",\"category\":\"\\u65e5\\u7a0b\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5de5\\u4f5c\\u65e5\\u7a0b\\u5b89\\u6392\\u3002\",\"author\":\"banyan @ IBOS Team Inc\",\"version\":\"1.0\",\"indexShow\":{\"widget\":[\"calendar\\/calendar\",\"calendar\\/task\"],\"link\":\"calendar\\/schedule\\/index\"},\"icon\":1,\"url\":\"calendar\\/schedule\\/index\"},\"config\":{\"modules\":{\"calendar\":{\"class\":\"application\\\\modules\\\\calendar\\\\CalendarModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"calendar\":\"application.modules.calendar.language\"}}}},\"authorization\":{\"schedule\":{\"type\":\"node\",\"name\":\"\\u65e5\\u7a0b\",\"group\":\"\\u65e5\\u7a0b\\u5b89\\u6392\",\"controllerMap\":{\"schedule\":[\"index\",\"subschedule\",\"shareschedule\",\"add\",\"edit\",\"del\"]}},\"task\":{\"type\":\"node\",\"name\":\"\\u5f85\\u529e\",\"group\":\"\\u65e5\\u7a0b\\u5b89\\u6392\",\"controllerMap\":{\"task\":[\"index\",\"subtask\",\"add\",\"edit\",\"del\"]}},\"loop\":{\"type\":\"node\",\"name\":\"\\u5468\\u671f\\u6027\\u4e8b\\u52a1\",\"group\":\"\\u65e5\\u7a0b\\u5b89\\u6392\",\"controllerMap\":{\"loop\":[\"index\",\"add\",\"edit\",\"del\"]}}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('contact', '通讯录', 'contact/default/index', '0', '1.0', '1', '人力资源', '提供企业员工通讯录', '{\"param\":{\"name\":\"\\u901a\\u8baf\\u5f55\",\"category\":\"\\u4eba\\u529b\\u8d44\\u6e90\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5458\\u5de5\\u901a\\u8baf\\u5f55\",\"author\":\"gzhzh @ IBOS Team Inc\",\"version\":\"1.0\",\"indexShow\":{\"link\":\"contact\\/default\\/index\"},\"icon\":1,\"url\":\"contact\\/default\\/index\"},\"config\":{\"modules\":{\"contact\":{\"class\":\"application\\\\modules\\\\contact\\\\ContactModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"contact\":\"application.modules.contact.language\"}}}},\"authorization\":{\"contact\":{\"type\":\"node\",\"name\":\"\\u901a\\u8baf\\u5f55\",\"group\":\"\\u901a\\u8baf\\u5f55\",\"controllerMap\":{\"default\":[\"index\",\"ajaxapi\",\"export\",\"printcontact\"],\"constant\":[\"index\"],\"api\":[\"deptlist\",\"userlist\",\"groupuserlist\",\"search\",\"corp\",\"dept\",\"user\",\"hiddenuidarr\"]}}},\"behaviors\":{\"onUpdateCache\":{\"class\":\"application\\\\modules\\\\contact\\\\behaviors\\\\UpdateContactCache\"}}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('diary', '日志', 'diary/default/index', '0', '1.0', '1', '日志', '提供企业工作日志发布', '{\"param\":{\"name\":\"\\u65e5\\u5fd7\",\"category\":\"\\u65e5\\u5fd7\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5de5\\u4f5c\\u65e5\\u5fd7\\u53d1\\u5e03\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":[\"diary\\/diary\"],\"link\":\"diary\\/default\\/index\"},\"icon\":1,\"url\":\"diary\\/default\\/index\"},\"config\":{\"modules\":{\"diary\":{\"class\":\"application\\\\modules\\\\diary\\\\DiaryModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"diary\":\"application.modules.diary.language\"}}}},\"authorization\":{\"diary\":{\"type\":\"node\",\"name\":\"\\u65e5\\u5fd7\\u7ba1\\u7406\",\"group\":\"\\u5de5\\u4f5c\\u65e5\\u5fd7\",\"controllerMap\":{\"default\":[\"index\",\"add\",\"edit\",\"del\",\"show\"],\"share\":[\"index\",\"show\"],\"attention\":[\"index\",\"edit\",\"show\"],\"comment\":[\"getcommentlist\",\"addcomment\",\"delcomment\"]}},\"review\":{\"type\":\"node\",\"name\":\"\\u8bc4\\u9605\\u4e0b\\u5c5e\",\"group\":\"\\u5de5\\u4f5c\\u65e5\\u5fd7\",\"controllerMap\":{\"review\":[\"index\",\"personal\",\"add\",\"edit\",\"del\",\"show\"]}},\"statistics\":{\"type\":\"node\",\"name\":\"\\u67e5\\u770b\\u7edf\\u8ba1\",\"group\":\"\\u5de5\\u4f5c\\u65e5\\u5fd7\",\"controllerMap\":{\"stats\":[\"personal\",\"review\"]}}},\"statistics\":{\"sidebar\":\"application\\\\modules\\\\diary\\\\widgets\\\\StatDiarySidebar\",\"header\":\"application\\\\modules\\\\diary\\\\widgets\\\\StatDiaryHeader\",\"summary\":\"application\\\\modules\\\\diary\\\\widgets\\\\StatDiarySummary\",\"count\":\"application\\\\modules\\\\diary\\\\widgets\\\\StatDiaryCount\",\"footer\":\"application\\\\modules\\\\diary\\\\widgets\\\\StatDiaryFooter\"}}', '0', '0', '1517282440', '0');
INSERT INTO `yckj_module` VALUES ('email', '邮件', 'email/list/index', '0', '1.0', '1', '邮件', '提供企业内外邮件沟通。', '{\"param\":{\"name\":\"\\u90ae\\u4ef6\",\"category\":\"\\u90ae\\u4ef6\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5185\\u5916\\u90ae\\u4ef6\\u6c9f\\u901a\\u3002\",\"author\":\"banyan @ IBOS Team Inc\",\"version\":\"1.0\",\"indexShow\":{\"widget\":[\"email\\/email\"],\"link\":\"email\\/list\\/index\"},\"icon\":1,\"url\":\"email\\/list\\/index\"},\"config\":{\"modules\":{\"email\":{\"class\":\"application\\\\modules\\\\email\\\\EmailModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"email\":\"application.modules.email.language\"}}}},\"authorization\":{\"inbox\":{\"type\":\"node\",\"name\":\"\\u5185\\u90e8\\u90ae\\u7bb1\",\"group\":\"\\u90ae\\u4ef6\\u7ba1\\u7406\",\"controllerMap\":{\"list\":[\"index\",\"search\"],\"folder\":[\"index\",\"add\",\"edit\",\"del\"],\"content\":[\"index\",\"add\",\"edit\",\"show\",\"export\"]}},\"webinbox\":{\"type\":\"node\",\"name\":\"\\u5916\\u90e8\\u90ae\\u7bb1\",\"group\":\"\\u90ae\\u4ef6\\u7ba1\\u7406\",\"controllerMap\":{\"web\":[\"index\",\"add\",\"edit\",\"del\",\"receive\",\"show\"]}}}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('file', '文件柜', 'file/default/index', '0', '1.0', '1', '文件柜', '提供企业文件存储', '{\"param\":{\"name\":\"\\u6587\\u4ef6\\u67dc\",\"category\":\"\\u6587\\u4ef6\\u67dc\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u6587\\u4ef6\\u5b58\\u50a8\",\"author\":\"gzhzh @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":0,\"indexShow\":{\"widget\":[\"file\\/file\"],\"link\":\"file\\/default\\/index\"},\"icon\":1,\"url\":\"file\\/default\\/index\"},\"config\":{\"modules\":{\"file\":{\"class\":\"application\\\\modules\\\\file\\\\FileModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"file\":\"application.modules.file.language\"}}}},\"authorization\":{\"persoanl\":{\"type\":\"node\",\"name\":\"\\u4e2a\\u4eba\\u7f51\\u76d8\",\"group\":\"\\u6587\\u4ef6\\u67dc\",\"controllerMap\":{\"default\":[\"index\",\"getdynamic\"],\"personal\":[\"index\",\"getcate\",\"add\",\"del\",\"show\",\"ajaxent\"],\"myshare\":[\"index\",\"getcate\",\"share\",\"show\"],\"fromshare\":[\"index\",\"getcate\",\"show\"]}},\"company\":{\"type\":\"node\",\"name\":\"\\u516c\\u53f8\\u7f51\\u76d8\",\"group\":\"\\u6587\\u4ef6\\u67dc\",\"controllerMap\":{\"company\":[\"index\",\"getcate\",\"add\",\"del\",\"show\",\"ajaxent\"]}}}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('mobile', 'IBOS移动平台', '', '1', '1.0', '1', '', '提供IBOS移动平台数据请求和处理相关功能', '{\"param\":{\"name\":\"IBOS\\u79fb\\u52a8\\u5e73\\u53f0\",\"description\":\"\\u63d0\\u4f9bIBOS\\u79fb\\u52a8\\u5e73\\u53f0\\u6570\\u636e\\u8bf7\\u6c42\\u548c\\u5904\\u7406\\u76f8\\u5173\\u529f\\u80fd\",\"author\":\"Aeolus @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"category\":\"\",\"url\":\"\"},\"config\":{\"modules\":{\"mobile\":{\"class\":\"application\\\\modules\\\\mobile\\\\MobileModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"mobile\":\"application.modules.mobile.language\"}}}}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('officialdoc', '通知公告', 'officialdoc/officialdoc/index', '0', '1.0', '1', '通知公告', '提供企业通知信息发布，以及版本记录', '{\"param\":{\"name\":\"\\u901a\\u77e5\\u516c\\u544a\",\"category\":\"\\u901a\\u77e5\\u516c\\u544a\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u901a\\u77e5\\u4fe1\\u606f\\u53d1\\u5e03\\uff0c\\u4ee5\\u53ca\\u7248\\u672c\\u8bb0\\u5f55\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":[\"officialdoc\\/officialdoc\"],\"link\":\"officialdoc\\/officialdoc\\/index\"},\"icon\":1,\"url\":\"officialdoc\\/officialdoc\\/index\"},\"config\":{\"modules\":{\"officialdoc\":{\"class\":\"application\\\\modules\\\\officialdoc\\\\OfficialdocModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"officialdoc\":\"application.modules.officialdoc.language\"}}}},\"authorization\":{\"view\":{\"type\":\"node\",\"name\":\"\\u901a\\u77e5\\u6d4f\\u89c8\",\"group\":\"\\u901a\\u77e5\\u516c\\u544a\",\"controllerMap\":{\"officialdoc\":[\"index\",\"show\",\"getdoclist\"],\"category\":[\"index\"],\"comment\":[\"getcommentlist\",\"addcomment\",\"delcomment\"]}},\"publish\":{\"type\":\"node\",\"name\":\"\\u901a\\u77e5\\u53d1\\u5e03\",\"group\":\"\\u901a\\u77e5\\u516c\\u544a\",\"controllerMap\":{\"officialdoc\":[\"add\"]}},\"category\":{\"type\":\"node\",\"name\":\"\\u901a\\u77e5\\u5206\\u7c7b\\u7ba1\\u7406\",\"group\":\"\\u901a\\u77e5\\u516c\\u544a\",\"controllerMap\":{\"category\":[\"index\",\"add\",\"edit\",\"del\"]}},\"manager\":{\"type\":\"data\",\"name\":\"\\u901a\\u77e5\\u7ba1\\u7406\",\"group\":\"\\u901a\\u77e5\\u516c\\u544a\",\"node\":{\"edit\":{\"name\":\"\\u7f16\\u8f91\",\"controllerMap\":{\"officialdoc\":[\"edit\"]}},\"del\":{\"name\":\"\\u5220\\u9664\",\"controllerMap\":{\"officialdoc\":[\"del\"]}}}}}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('recruit', '招聘', '', '0', '1.0', '1', '人力资源', '提供企业招聘信息', '{\"param\":{\"name\":\"\\u62db\\u8058\",\"category\":\"\\u4eba\\u529b\\u8d44\\u6e90\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u62db\\u8058\\u4fe1\\u606f\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"url\":\"\"},\"config\":{\"modules\":{\"recruit\":{\"class\":\"application\\\\modules\\\\recruit\\\\RecruitModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"recruit\":\"application.modules.recruit.language\"}}}},\"authorization\":{\"resume\":{\"type\":\"node\",\"name\":\"\\u4eba\\u624d\\u7ba1\\u7406\",\"group\":\"\\u62db\\u8058\\u7ba1\\u7406\",\"controllerMap\":{\"resume\":[\"index\",\"add\",\"show\",\"edit\",\"sendemail\",\"del\"]}},\"contact\":{\"type\":\"node\",\"name\":\"\\u8054\\u7cfb\\u8bb0\\u5f55\",\"group\":\"\\u62db\\u8058\\u7ba1\\u7406\",\"controllerMap\":{\"contact\":[\"index\",\"add\",\"edit\",\"del\",\"export\"]}},\"interview\":{\"type\":\"node\",\"name\":\"\\u9762\\u8bd5\\u8bb0\\u5f55\",\"group\":\"\\u62db\\u8058\\u7ba1\\u7406\",\"controllerMap\":{\"interview\":[\"index\",\"add\",\"edit\",\"del\",\"export\"]}},\"bgchecks\":{\"type\":\"node\",\"name\":\"\\u80cc\\u666f\\u8c03\\u67e5\",\"group\":\"\\u62db\\u8058\\u7ba1\\u7406\",\"controllerMap\":{\"bgchecks\":[\"index\",\"add\",\"edit\",\"del\",\"export\"]}},\"statistics\":{\"type\":\"node\",\"name\":\"\\u62db\\u8058\\u7edf\\u8ba1\",\"group\":\"\\u62db\\u8058\\u7ba1\\u7406\",\"controllerMap\":{\"stats\":[\"index\"]}}},\"statistics\":{\"sidebar\":\"application\\\\modules\\\\recruit\\\\widgets\\\\StatRecruitSidebar\",\"header\":\"application\\\\modules\\\\recruit\\\\widgets\\\\StatRecruitHeader\",\"summary\":\"application\\\\modules\\\\recruit\\\\widgets\\\\StatRecruitSummary\",\"count\":\"application\\\\modules\\\\recruit\\\\widgets\\\\StatRecruitCount\"}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('report', '工作汇报', 'report/default/index', '0', '1.0', '1', '工作汇报', '提供企业工作汇报', '{\"param\":{\"name\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"category\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"description\":\"\\u63d0\\u4f9b\\u4f01\\u4e1a\\u5de5\\u4f5c\\u6c47\\u62a5\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":[\"report\\/report\"],\"link\":\"report\\/default\\/index\"},\"icon\":1,\"url\":\"report\\/default\\/index\"},\"config\":{\"modules\":{\"report\":{\"class\":\"application\\\\modules\\\\report\\\\ReportModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"report\":\"application.modules.report.language\"}}}},\"authorization\":{\"report\":{\"type\":\"node\",\"name\":\"\\u4e2a\\u4eba\\u6c47\\u62a5\",\"group\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"controllerMap\":{\"default\":[\"index\",\"add\",\"edit\",\"del\",\"show\"],\"type\":[\"add\",\"edit\",\"del\"],\"comment\":[\"getcommentlist\",\"addcomment\",\"delcomment\"],\"api\":[\"addcomment\",\"allread\",\"delcomment\",\"delreport\",\"formreport\",\"getcommentlist\",\"getlist\",\"getreader\",\"savereport\",\"showreport\",\"usertemplate\",\"shoplist\",\"getcount\",\"getcommentview\",\"getreviewcomment\",\"getauthority\",\"getcharge\",\"addtemplate\"]}},\"managertemplate\":{\"type\":\"node\",\"name\":\"\\u7ba1\\u7406\\u6a21\\u677f\",\"group\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"controllerMap\":{\"api\":[\"savetemplate\",\"formtemplate\",\"settemplate\",\"deltemplte\",\"sorttemplate\",\"usertemplate\",\"shoplist\",\"managertemplate\",\"getpicture\"]}},\"settemplate\":{\"type\":\"node\",\"name\":\"\\u8bbe\\u7f6e\\u6a21\\u677f\",\"group\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"controllerMap\":{\"api\":[\"settemplate\",\"managertemplate\",\"usertemplate\",\"shoplist\"]}},\"review\":{\"type\":\"node\",\"name\":\"\\u8bc4\\u9605\\u4e0b\\u5c5e\\u6c47\\u62a5\",\"group\":\"\\u5de5\\u4f5c\\u6c47\\u62a5\",\"controllerMap\":{\"review\":[\"index\",\"personal\",\"add\",\"edit\",\"del\",\"show\"],\"api\":[\"allread\",\"formreport\",\"getreader\",\"savereport\",\"showreport\",\"usertemplate\",\"getstamp\",\"setstamp\",\"shoplist\",\"getcount\",\"getcommentview\",\"getreviewcomment\",\"getauthority\"]}}},\"statistics\":{\"sidebar\":\"application\\\\modules\\\\report\\\\widgets\\\\StatReportSidebar\",\"header\":\"application\\\\modules\\\\report\\\\widgets\\\\StatReportHeader\",\"summary\":\"application\\\\modules\\\\report\\\\widgets\\\\StatReportSummary\",\"count\":\"application\\\\modules\\\\report\\\\widgets\\\\StatReportCount\"}}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('statistics', '统计模块', '', '0', '1.0', '1', '统计模块', '统计模块，提供各支持模块的数据统计及汇总，采用模块扩展的方式灵活定义统计内容与视图', '{\"param\":{\"name\":\"\\u7edf\\u8ba1\\u6a21\\u5757\",\"category\":\"\\u7edf\\u8ba1\\u6a21\\u5757\",\"description\":\"\\u7edf\\u8ba1\\u6a21\\u5757\\uff0c\\u63d0\\u4f9b\\u5404\\u652f\\u6301\\u6a21\\u5757\\u7684\\u6570\\u636e\\u7edf\\u8ba1\\u53ca\\u6c47\\u603b\\uff0c\\u91c7\\u7528\\u6a21\\u5757\\u6269\\u5c55\\u7684\\u65b9\\u5f0f\\u7075\\u6d3b\\u5b9a\\u4e49\\u7edf\\u8ba1\\u5185\\u5bb9\\u4e0e\\u89c6\\u56fe\",\"author\":\"banyan @ IBOS Team Inc\",\"version\":\"1.0\",\"icon\":1,\"url\":\"\"},\"config\":{\"modules\":{\"statistics\":{\"class\":\"application\\\\modules\\\\statistics\\\\StatisticsModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"statistics\":\"application.modules.statistics.language\"}}}},\"authorization\":[]}', '0', '0', '1517282441', '0');
INSERT INTO `yckj_module` VALUES ('vote', '投票模块', 'vote/default/index', '0', '1.0', '1', '调查投票', '提供信息中心等模块投票调查使用', '{\"param\":{\"name\":\"\\u6295\\u7968\\u6a21\\u5757\",\"category\":\"\\u8c03\\u67e5\\u6295\\u7968\",\"description\":\"\\u63d0\\u4f9b\\u4fe1\\u606f\\u4e2d\\u5fc3\\u7b49\\u6a21\\u5757\\u6295\\u7968\\u8c03\\u67e5\\u4f7f\\u7528\",\"author\":\"banyanCheung @ IBOS Team Inc\",\"version\":\"1.0\",\"pushMovement\":1,\"indexShow\":{\"widget\":true,\"link\":\"vote\\/default\\/index\"},\"icon\":1,\"url\":\"vote\\/default\\/index\"},\"config\":{\"modules\":{\"vote\":{\"class\":\"application\\\\modules\\\\vote\\\\VoteModule\"}},\"components\":{\"messages\":{\"extensionPaths\":{\"vote\":\"application.modules.vote.language\"}}}},\"authorization\":{\"view\":{\"type\":\"node\",\"name\":\"\\u8c03\\u67e5\\u6d4f\\u89c8\",\"group\":\"\\u8c03\\u67e5\\u6295\\u7968\",\"controllerMap\":{\"default\":[\"index\",\"show\",\"fetchindexlist\",\"showvote\",\"showvoteusers\",\"vote\"]}},\"publish\":{\"type\":\"node\",\"name\":\"\\u8c03\\u67e5\\u53d1\\u5e03\",\"group\":\"\\u8c03\\u67e5\\u6295\\u7968\",\"controllerMap\":{\"form\":[\"addorupdate\",\"updateendtime\",\"del\",\"show\",\"edit\"]}},\"manager\":{\"type\":\"node\",\"name\":\"\\u8c03\\u67e5\\u7ba1\\u7406\",\"group\":\"\\u8c03\\u67e5\\u6295\\u7968\",\"controllerMap\":{\"default\":[\"export\"]}}}}', '0', '0', '1517282441', '0');

-- ----------------------------
-- Table structure for `yckj_module_guide`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_module_guide`;
CREATE TABLE `yckj_module_guide` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `route` varchar(32) NOT NULL DEFAULT '' COMMENT '引导的页面id',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '已经引导过的uid',
  PRIMARY KEY (`id`),
  KEY `route` (`route`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_module_guide
-- ----------------------------
INSERT INTO `yckj_module_guide` VALUES ('1', 'doc_add', '1');
INSERT INTO `yckj_module_guide` VALUES ('2', 'cal_sch_index', '1');

-- ----------------------------
-- Table structure for `yckj_module_reader`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_module_reader`;
CREATE TABLE `yckj_module_reader` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `module` varchar(255) NOT NULL COMMENT '关联模型',
  `relateid` int(11) NOT NULL COMMENT '关联id',
  `uid` int(11) NOT NULL COMMENT '读者id',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  `readername` varchar(255) DEFAULT NULL COMMENT '读者真实名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_module_reader
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_nav`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_nav`;
CREATE TABLE `yckj_nav` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `name` varchar(30) NOT NULL COMMENT '导航名字',
  `url` varchar(255) NOT NULL COMMENT '链接URL',
  `targetnew` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0为本窗口打开，1为新窗口打开',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '系统内置',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `module` varchar(15) NOT NULL DEFAULT '' COMMENT '模块名',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0为超链接，1为单页图文',
  `pageid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '单页图文关联id',
  PRIMARY KEY (`id`),
  KEY `module` (`module`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_nav
-- ----------------------------
INSERT INTO `yckj_nav` VALUES ('1', '0', '首页', 'javascript:void(0)', '0', '1', '0', '1', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('3', '0', '个人办公', 'javascript:void(0);', '0', '1', '0', '5', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('5', '0', '综合办公', 'javascript:void(0);', '0', '1', '0', '6', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('9', '0', '人力资源', 'javascript:void(0)', '0', '1', '1', '9', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('10', '1', '个人门户', 'weibo/home/index', '0', '1', '0', '2', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('11', '1', '办公门户', 'main/default/index', '0', '1', '0', '1', '', '0', '0');
INSERT INTO `yckj_nav` VALUES ('12', '5', '信息公告', 'article/default/index', '0', '1', '0', '1', 'article', '0', '0');
INSERT INTO `yckj_nav` VALUES ('13', '3', '任务指派', 'assignment/unfinished/index', '0', '1', '0', '1', 'assignment', '0', '0');
INSERT INTO `yckj_nav` VALUES ('14', '0', '日程', 'calendar/schedule/index', '0', '1', '0', '3', 'calendar', '0', '0');
INSERT INTO `yckj_nav` VALUES ('15', '3', '通讯录', 'contact/default/index', '0', '1', '0', '5', 'contact', '0', '0');
INSERT INTO `yckj_nav` VALUES ('16', '3', '工作日志', 'diary/default/index', '0', '1', '0', '2', 'diary', '0', '0');
INSERT INTO `yckj_nav` VALUES ('2', '0', '邮件', 'email/list/index', '0', '1', '0', '2', 'email', '0', '0');
INSERT INTO `yckj_nav` VALUES ('17', '3', '文件柜', 'file/default/index', '0', '1', '0', '3', 'file', '0', '0');
INSERT INTO `yckj_nav` VALUES ('18', '5', '通知公告', 'officialdoc/officialdoc/index', '0', '1', '0', '2', 'officialdoc', '0', '0');
INSERT INTO `yckj_nav` VALUES ('19', '9', '招聘管理', 'recruit/resume/index', '0', '1', '0', '1', 'recruit', '0', '0');
INSERT INTO `yckj_nav` VALUES ('20', '3', '工作汇报', 'report/default/index', '0', '1', '0', '3', 'report', '0', '0');
INSERT INTO `yckj_nav` VALUES ('21', '5', '调查投票', 'vote/default/index', '0', '1', '0', '7', 'vote', '0', '0');

-- ----------------------------
-- Table structure for `yckj_node`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_node`;
CREATE TABLE `yckj_node` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `module` varchar(30) NOT NULL COMMENT '模块名',
  `key` varchar(20) NOT NULL COMMENT '授权节点key',
  `node` varchar(20) NOT NULL COMMENT '子节点(如果有)',
  `name` varchar(20) NOT NULL COMMENT '节点名称',
  `group` varchar(20) NOT NULL COMMENT '分组',
  `category` varchar(20) NOT NULL COMMENT '分类',
  `type` enum('data','node') NOT NULL DEFAULT 'node' COMMENT '节点类型',
  `routes` text NOT NULL COMMENT '路由',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_node
-- ----------------------------
INSERT INTO `yckj_node` VALUES ('1', 'dashboard', 'cobindings', '', '酷办公绑定', '绑定', '权限列表', 'node', 'dashboard/cobinding/index,dashboard/cosync/index');
INSERT INTO `yckj_node` VALUES ('2', 'dashboard', 'wxbindings', '', '微信企业号绑定', '绑定', '权限列表', 'node', 'dashboard/wxbinding/index');
INSERT INTO `yckj_node` VALUES ('3', 'dashboard', 'ims', '', '即时通讯绑定', '绑定', '权限列表', 'node', 'dashboard/im/index');
INSERT INTO `yckj_node` VALUES ('4', 'dashboard', 'globals', '', '单位管理', '全局', '权限列表', 'node', 'dashboard/unit/index');
INSERT INTO `yckj_node` VALUES ('5', 'dashboard', 'credits', '', '积分设置', '全局', '权限列表', 'node', 'dashboard/credit/setup');
INSERT INTO `yckj_node` VALUES ('6', 'dashboard', 'usergroups', '', '用户组', '全局', '权限列表', 'node', 'dashboard/usergroup/index');
INSERT INTO `yckj_node` VALUES ('7', 'dashboard', 'optimizes', '', '性能优化', '全局', '权限列表', 'node', 'dashboard/optimize/cache');
INSERT INTO `yckj_node` VALUES ('8', 'dashboard', 'dates', '', '时间设置', '全局', '权限列表', 'node', 'dashboard/date/index');
INSERT INTO `yckj_node` VALUES ('9', 'dashboard', 'uploads', '', '上传设置', '全局', '权限列表', 'node', 'dashboard/upload/index');
INSERT INTO `yckj_node` VALUES ('10', 'dashboard', 'smss', '', '手机短信设置', '全局', '权限列表', 'node', 'dashboard/sms/manager');
INSERT INTO `yckj_node` VALUES ('11', 'dashboard', 'syscodes', '', '系统代码设置', '全局', '权限列表', 'node', 'dashboard/syscode/index');
INSERT INTO `yckj_node` VALUES ('12', 'dashboard', 'emails', '', '邮件设置', '全局', '权限列表', 'node', 'dashboard/email/setup');
INSERT INTO `yckj_node` VALUES ('13', 'dashboard', 'securitys', '', '安全设置', '全局', '权限列表', 'node', 'dashboard/security/setup');
INSERT INTO `yckj_node` VALUES ('14', 'dashboard', 'sysstamps', '', '系统图章', '全局', '权限列表', 'node', 'dashboard/sysstamp/index');
INSERT INTO `yckj_node` VALUES ('15', 'dashboard', 'approvals', '', '审批流程', '全局', '权限列表', 'node', 'dashboard/approval/index');
INSERT INTO `yckj_node` VALUES ('16', 'dashboard', 'notifys', '', '提醒策略设置', '全局', '权限列表', 'node', 'dashboard/notify/setup');
INSERT INTO `yckj_node` VALUES ('17', 'dashboard', 'users', '', '部门用户管理', '用户', '权限列表', 'node', 'dashboard/user/index');
INSERT INTO `yckj_node` VALUES ('18', 'dashboard', 'roles', '', '角色权限管理', '用户', '权限列表', 'node', 'dashboard/role/index');
INSERT INTO `yckj_node` VALUES ('19', 'dashboard', 'positions', '', '岗位管理', '用户', '权限列表', 'node', 'dashboard/position/index');
INSERT INTO `yckj_node` VALUES ('20', 'dashboard', 'roleadmins', '', '管理员管理', '用户', '权限列表', 'node', 'dashboard/roleadmin/index');
INSERT INTO `yckj_node` VALUES ('21', 'dashboard', 'navs', '', '顶部导航设置', '界面', '权限列表', 'node', 'dashboard/nav/index');
INSERT INTO `yckj_node` VALUES ('22', 'dashboard', 'quicknavs', '', '快捷导航设置', '界面', '权限列表', 'node', 'dashboard/quicknav/index');
INSERT INTO `yckj_node` VALUES ('23', 'dashboard', 'logins', '', '登录页背景设置', '界面', '权限列表', 'node', 'dashboard/login/index');
INSERT INTO `yckj_node` VALUES ('24', 'dashboard', 'backgrounds', '', '系统背景设置', '界面', '权限列表', 'node', 'dashboard/backgroud/index');
INSERT INTO `yckj_node` VALUES ('25', 'dashboard', 'modules', '', '模块管理', '模块', '权限列表', 'node', 'dashboard/module/manager');
INSERT INTO `yckj_node` VALUES ('26', 'dashboard', 'permissionss', '', '权限设置', '模块', '权限列表', 'node', 'dashboard/permissions/setup');
INSERT INTO `yckj_node` VALUES ('27', 'dashboard', 'updates', '', '更新缓存', '管理', '权限列表', 'node', 'dashboard/update/index');
INSERT INTO `yckj_node` VALUES ('28', 'dashboard', 'announcements', '', '系统公告', '管理', '权限列表', 'node', 'dashboard/announcement/setup');
INSERT INTO `yckj_node` VALUES ('29', 'dashboard', 'databases', '', '数据库', '管理', '权限列表', 'node', 'dashboard/database/backup');
INSERT INTO `yckj_node` VALUES ('30', 'dashboard', 'crons', '', '计划任务', '管理', '权限列表', 'node', 'dashboard/cron/index');
INSERT INTO `yckj_node` VALUES ('31', 'dashboard', 'upgrades', '', '在线升级', '管理', '权限列表', 'node', 'dashboard/upgrade/index');
INSERT INTO `yckj_node` VALUES ('32', 'dashboard', 'services', '', '云服务', '服务', '权限列表', 'node', 'dashboard/service/index');
INSERT INTO `yckj_node` VALUES ('33', 'article', 'view', '', '信息查看', '新闻', '信息中心', 'node', 'article/comment/getcommentlist,article/comment/addcomment,article/comment/delcomment,article/comment/getcommentview,article/category/index,article/default/index,article/default/show,article/default/preview,article/default/vote,article/default/getreader,article/default/getcount,article/default/read,article/data/index,article/data/show,article/data/preview,article/verify/flowlog');
INSERT INTO `yckj_node` VALUES ('34', 'article', 'verify', '', '信息审核', '新闻', '信息中心', 'node', 'article/verify/index,article/verify/verify,article/verify/back,article/verify/index,article/verify/cancel');
INSERT INTO `yckj_node` VALUES ('35', 'article', 'publish', '', '信息发布', '新闻', '信息中心', 'node', 'article/publish/index,article/publish/call,article/publish/cancel,article/default/add,article/default/vote,article/default/submit,article/data/edit,article/data/option,article/category/getcurapproval');
INSERT INTO `yckj_node` VALUES ('36', 'article', 'category', '', '分类管理', '新闻', '信息中心', 'node', 'article/category/add,article/category/edit,article/category/del,article/category/move,article/category/index');
INSERT INTO `yckj_node` VALUES ('37', 'article', 'manager', '', '内容管理', '新闻', '信息中心', 'data', 'article/category/add,article/category/edit,article/category/del,article/category/move,article/category/index');
INSERT INTO `yckj_node` VALUES ('38', 'article', 'manager', 'edit', '编辑', '新闻', '信息中心', 'data', 'article/default/edit,article/default/top,article/default/move,article/default/hightlight,article/default/save,article/default/getmove,article/default/vote,article/default/submit,article/data/edit,article/data/option');
INSERT INTO `yckj_node` VALUES ('39', 'article', 'manager', 'del', '删除', '新闻', '信息中心', 'data', 'article/default/delete');
INSERT INTO `yckj_node` VALUES ('40', 'assignment', 'assignment', '', '任务管理', '任务指派', '任务指派', 'node', 'assignment/default/add,assignment/default/edit,assignment/default/del,assignment/default/show,assignment/unfinished/index,assignment/unfinished/ajaxentrance,assignment/finished/index,assignment/comment/getcommentlist,assignment/comment/addcomment,assignment/comment/delcomment');
INSERT INTO `yckj_node` VALUES ('41', 'assignment', 'review', '', '查看下属任务', '任务指派', '任务指派', 'node', 'assignment/unfinished/sublist');
INSERT INTO `yckj_node` VALUES ('42', 'calendar', 'schedule', '', '日程', '日程安排', '日程', 'node', 'calendar/schedule/index,calendar/schedule/subschedule,calendar/schedule/shareschedule,calendar/schedule/add,calendar/schedule/edit,calendar/schedule/del');
INSERT INTO `yckj_node` VALUES ('43', 'calendar', 'task', '', '待办', '日程安排', '日程', 'node', 'calendar/task/index,calendar/task/subtask,calendar/task/add,calendar/task/edit,calendar/task/del');
INSERT INTO `yckj_node` VALUES ('44', 'calendar', 'loop', '', '周期性事务', '日程安排', '日程', 'node', 'calendar/loop/index,calendar/loop/add,calendar/loop/edit,calendar/loop/del');
INSERT INTO `yckj_node` VALUES ('45', 'contact', 'contact', '', '通讯录', '通讯录', '人力资源', 'node', 'contact/default/index,contact/default/ajaxapi,contact/default/export,contact/default/printcontact,contact/constant/index,contact/api/deptlist,contact/api/userlist,contact/api/groupuserlist,contact/api/search,contact/api/corp,contact/api/dept,contact/api/user,contact/api/hiddenuidarr');
INSERT INTO `yckj_node` VALUES ('46', 'diary', 'diary', '', '日志管理', '工作日志', '日志', 'node', 'diary/default/index,diary/default/add,diary/default/edit,diary/default/del,diary/default/show,diary/share/index,diary/share/show,diary/attention/index,diary/attention/edit,diary/attention/show,diary/comment/getcommentlist,diary/comment/addcomment,diary/comment/delcomment');
INSERT INTO `yckj_node` VALUES ('47', 'diary', 'review', '', '评阅下属', '工作日志', '日志', 'node', 'diary/review/index,diary/review/personal,diary/review/add,diary/review/edit,diary/review/del,diary/review/show');
INSERT INTO `yckj_node` VALUES ('48', 'diary', 'statistics', '', '查看统计', '工作日志', '日志', 'node', 'diary/stats/personal,diary/stats/review');
INSERT INTO `yckj_node` VALUES ('49', 'email', 'inbox', '', '内部邮箱', '邮件管理', '邮件', 'node', 'email/list/index,email/list/search,email/folder/index,email/folder/add,email/folder/edit,email/folder/del,email/content/index,email/content/add,email/content/edit,email/content/show,email/content/export');
INSERT INTO `yckj_node` VALUES ('50', 'email', 'webinbox', '', '外部邮箱', '邮件管理', '邮件', 'node', 'email/web/index,email/web/add,email/web/edit,email/web/del,email/web/receive,email/web/show');
INSERT INTO `yckj_node` VALUES ('51', 'file', 'persoanl', '', '个人网盘', '文件柜', '文件柜', 'node', 'file/default/index,file/default/getdynamic,file/personal/index,file/personal/getcate,file/personal/add,file/personal/del,file/personal/show,file/personal/ajaxent,file/myshare/index,file/myshare/getcate,file/myshare/share,file/myshare/show,file/fromshare/index,file/fromshare/getcate,file/fromshare/show');
INSERT INTO `yckj_node` VALUES ('52', 'file', 'company', '', '公司网盘', '文件柜', '文件柜', 'node', 'file/company/index,file/company/getcate,file/company/add,file/company/del,file/company/show,file/company/ajaxent');
INSERT INTO `yckj_node` VALUES ('53', 'officialdoc', 'view', '', '通知浏览', '通知公告', '通知公告', 'node', 'officialdoc/officialdoc/index,officialdoc/officialdoc/show,officialdoc/officialdoc/getdoclist,officialdoc/category/index,officialdoc/comment/getcommentlist,officialdoc/comment/addcomment,officialdoc/comment/delcomment');
INSERT INTO `yckj_node` VALUES ('54', 'officialdoc', 'publish', '', '通知发布', '通知公告', '通知公告', 'node', 'officialdoc/officialdoc/add');
INSERT INTO `yckj_node` VALUES ('55', 'officialdoc', 'category', '', '通知分类管理', '通知公告', '通知公告', 'node', 'officialdoc/category/index,officialdoc/category/add,officialdoc/category/edit,officialdoc/category/del');
INSERT INTO `yckj_node` VALUES ('56', 'officialdoc', 'manager', '', '通知管理', '通知公告', '通知公告', 'data', 'officialdoc/category/index,officialdoc/category/add,officialdoc/category/edit,officialdoc/category/del');
INSERT INTO `yckj_node` VALUES ('57', 'officialdoc', 'manager', 'edit', '编辑', '通知公告', '通知公告', 'data', 'officialdoc/officialdoc/edit');
INSERT INTO `yckj_node` VALUES ('58', 'officialdoc', 'manager', 'del', '删除', '通知公告', '通知公告', 'data', 'officialdoc/officialdoc/del');
INSERT INTO `yckj_node` VALUES ('59', 'recruit', 'resume', '', '人才管理', '招聘管理', '人力资源', 'node', 'recruit/resume/index,recruit/resume/add,recruit/resume/show,recruit/resume/edit,recruit/resume/sendemail,recruit/resume/del');
INSERT INTO `yckj_node` VALUES ('60', 'recruit', 'contact', '', '联系记录', '招聘管理', '人力资源', 'node', 'recruit/contact/index,recruit/contact/add,recruit/contact/edit,recruit/contact/del,recruit/contact/export');
INSERT INTO `yckj_node` VALUES ('61', 'recruit', 'interview', '', '面试记录', '招聘管理', '人力资源', 'node', 'recruit/interview/index,recruit/interview/add,recruit/interview/edit,recruit/interview/del,recruit/interview/export');
INSERT INTO `yckj_node` VALUES ('62', 'recruit', 'bgchecks', '', '背景调查', '招聘管理', '人力资源', 'node', 'recruit/bgchecks/index,recruit/bgchecks/add,recruit/bgchecks/edit,recruit/bgchecks/del,recruit/bgchecks/export');
INSERT INTO `yckj_node` VALUES ('63', 'recruit', 'statistics', '', '招聘统计', '招聘管理', '人力资源', 'node', 'recruit/stats/index');
INSERT INTO `yckj_node` VALUES ('64', 'report', 'report', '', '个人汇报', '工作汇报', '工作汇报', 'node', 'report/default/index,report/default/add,report/default/edit,report/default/del,report/default/show,report/type/add,report/type/edit,report/type/del,report/comment/getcommentlist,report/comment/addcomment,report/comment/delcomment,report/api/addcomment,report/api/allread,report/api/delcomment,report/api/delreport,report/api/formreport,report/api/getcommentlist,report/api/getlist,report/api/getreader,report/api/savereport,report/api/showreport,report/api/usertemplate,report/api/shoplist,report/api/getcount,report/api/getcommentview,report/api/getreviewcomment,report/api/getauthority,report/api/getcharge,report/api/addtemplate');
INSERT INTO `yckj_node` VALUES ('65', 'report', 'managertemplate', '', '管理模板', '工作汇报', '工作汇报', 'node', 'report/api/savetemplate,report/api/formtemplate,report/api/settemplate,report/api/deltemplte,report/api/sorttemplate,report/api/usertemplate,report/api/shoplist,report/api/managertemplate,report/api/getpicture');
INSERT INTO `yckj_node` VALUES ('66', 'report', 'settemplate', '', '设置模板', '工作汇报', '工作汇报', 'node', 'report/api/settemplate,report/api/managertemplate,report/api/usertemplate,report/api/shoplist');
INSERT INTO `yckj_node` VALUES ('67', 'report', 'review', '', '评阅下属汇报', '工作汇报', '工作汇报', 'node', 'report/review/index,report/review/personal,report/review/add,report/review/edit,report/review/del,report/review/show,report/api/allread,report/api/formreport,report/api/getreader,report/api/savereport,report/api/showreport,report/api/usertemplate,report/api/getstamp,report/api/setstamp,report/api/shoplist,report/api/getcount,report/api/getcommentview,report/api/getreviewcomment,report/api/getauthority');
INSERT INTO `yckj_node` VALUES ('68', 'vote', 'view', '', '调查浏览', '调查投票', '调查投票', 'node', 'vote/default/index,vote/default/show,vote/default/fetchindexlist,vote/default/showvote,vote/default/showvoteusers,vote/default/vote');
INSERT INTO `yckj_node` VALUES ('69', 'vote', 'publish', '', '调查发布', '调查投票', '调查投票', 'node', 'vote/form/addorupdate,vote/form/updateendtime,vote/form/del,vote/form/show,vote/form/edit');
INSERT INTO `yckj_node` VALUES ('70', 'vote', 'manager', '', '调查管理', '调查投票', '调查投票', 'node', 'vote/default/export');

-- ----------------------------
-- Table structure for `yckj_node_related`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_node_related`;
CREATE TABLE `yckj_node_related` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `roleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `module` varchar(30) NOT NULL COMMENT '模块名称',
  `key` varchar(20) NOT NULL COMMENT '授权节点key',
  `node` varchar(20) NOT NULL COMMENT '节点（如果有）',
  `val` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '数据权限',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=299 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_node_related
-- ----------------------------
INSERT INTO `yckj_node_related` VALUES ('240', '1', 'vote', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('239', '1', 'vote', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('238', '1', 'vote', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('237', '1', 'report', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('236', '1', 'report', 'report', '', '0');
INSERT INTO `yckj_node_related` VALUES ('235', '1', 'recruit', 'statistics', '', '0');
INSERT INTO `yckj_node_related` VALUES ('234', '1', 'recruit', 'bgchecks', '', '0');
INSERT INTO `yckj_node_related` VALUES ('233', '1', 'recruit', 'interview', '', '0');
INSERT INTO `yckj_node_related` VALUES ('232', '1', 'recruit', 'contact', '', '0');
INSERT INTO `yckj_node_related` VALUES ('231', '1', 'recruit', 'resume', '', '0');
INSERT INTO `yckj_node_related` VALUES ('230', '1', 'officialdoc', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('272', '2', 'vote', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('271', '2', 'vote', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('270', '2', 'vote', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('269', '2', 'report', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('268', '2', 'report', 'report', '', '0');
INSERT INTO `yckj_node_related` VALUES ('267', '2', 'recruit', 'bgchecks', '', '0');
INSERT INTO `yckj_node_related` VALUES ('266', '2', 'recruit', 'interview', '', '0');
INSERT INTO `yckj_node_related` VALUES ('265', '2', 'recruit', 'contact', '', '0');
INSERT INTO `yckj_node_related` VALUES ('264', '2', 'recruit', 'resume', '', '0');
INSERT INTO `yckj_node_related` VALUES ('298', '3', 'vote', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('297', '3', 'report', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('296', '3', 'report', 'report', '', '0');
INSERT INTO `yckj_node_related` VALUES ('295', '3', 'officialdoc', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('294', '3', 'officialdoc', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('229', '1', 'officialdoc', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('228', '1', 'officialdoc', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('227', '1', 'officialdoc', 'category', '', '0');
INSERT INTO `yckj_node_related` VALUES ('226', '1', 'officialdoc', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('225', '1', 'officialdoc', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('224', '1', 'file', 'company', '', '0');
INSERT INTO `yckj_node_related` VALUES ('223', '1', 'file', 'persoanl', '', '0');
INSERT INTO `yckj_node_related` VALUES ('222', '1', 'email', 'webinbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('221', '1', 'email', 'inbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('220', '1', 'diary', 'statistics', '', '0');
INSERT INTO `yckj_node_related` VALUES ('219', '1', 'diary', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('218', '1', 'diary', 'diary', '', '0');
INSERT INTO `yckj_node_related` VALUES ('217', '1', 'contact', 'contact', '', '0');
INSERT INTO `yckj_node_related` VALUES ('216', '1', 'calendar', 'loop', '', '0');
INSERT INTO `yckj_node_related` VALUES ('263', '2', 'officialdoc', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('262', '2', 'officialdoc', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('261', '2', 'officialdoc', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('260', '2', 'officialdoc', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('259', '2', 'officialdoc', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('258', '2', 'file', 'company', '', '0');
INSERT INTO `yckj_node_related` VALUES ('257', '2', 'file', 'persoanl', '', '0');
INSERT INTO `yckj_node_related` VALUES ('256', '2', 'email', 'webinbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('255', '2', 'email', 'inbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('254', '2', 'diary', 'statistics', '', '0');
INSERT INTO `yckj_node_related` VALUES ('253', '2', 'diary', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('252', '2', 'diary', 'diary', '', '0');
INSERT INTO `yckj_node_related` VALUES ('251', '2', 'contact', 'contact', '', '0');
INSERT INTO `yckj_node_related` VALUES ('250', '2', 'calendar', 'loop', '', '0');
INSERT INTO `yckj_node_related` VALUES ('293', '3', 'officialdoc', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('292', '3', 'officialdoc', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('291', '3', 'officialdoc', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('290', '3', 'file', 'company', '', '0');
INSERT INTO `yckj_node_related` VALUES ('289', '3', 'file', 'persoanl', '', '0');
INSERT INTO `yckj_node_related` VALUES ('288', '3', 'email', 'webinbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('287', '3', 'email', 'inbox', '', '0');
INSERT INTO `yckj_node_related` VALUES ('286', '3', 'diary', 'statistics', '', '0');
INSERT INTO `yckj_node_related` VALUES ('285', '3', 'diary', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('284', '3', 'diary', 'diary', '', '0');
INSERT INTO `yckj_node_related` VALUES ('283', '3', 'contact', 'contact', '', '0');
INSERT INTO `yckj_node_related` VALUES ('282', '3', 'calendar', 'loop', '', '0');
INSERT INTO `yckj_node_related` VALUES ('281', '3', 'calendar', 'task', '', '0');
INSERT INTO `yckj_node_related` VALUES ('280', '3', 'calendar', 'schedule', '', '0');
INSERT INTO `yckj_node_related` VALUES ('279', '3', 'assignment', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('215', '1', 'calendar', 'task', '', '0');
INSERT INTO `yckj_node_related` VALUES ('214', '1', 'calendar', 'schedule', '', '0');
INSERT INTO `yckj_node_related` VALUES ('213', '1', 'assignment', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('212', '1', 'assignment', 'assignment', '', '0');
INSERT INTO `yckj_node_related` VALUES ('211', '1', 'article', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('210', '1', 'article', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('209', '1', 'article', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('208', '1', 'article', 'category', '', '0');
INSERT INTO `yckj_node_related` VALUES ('207', '1', 'article', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('206', '1', 'article', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('249', '2', 'calendar', 'task', '', '0');
INSERT INTO `yckj_node_related` VALUES ('248', '2', 'calendar', 'schedule', '', '0');
INSERT INTO `yckj_node_related` VALUES ('247', '2', 'assignment', 'review', '', '0');
INSERT INTO `yckj_node_related` VALUES ('246', '2', 'assignment', 'assignment', '', '0');
INSERT INTO `yckj_node_related` VALUES ('245', '2', 'article', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('244', '2', 'article', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('243', '2', 'article', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('242', '2', 'article', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('241', '2', 'article', 'view', '', '0');
INSERT INTO `yckj_node_related` VALUES ('278', '3', 'assignment', 'assignment', '', '0');
INSERT INTO `yckj_node_related` VALUES ('277', '3', 'article', 'manager', 'del', '8');
INSERT INTO `yckj_node_related` VALUES ('276', '3', 'article', 'manager', 'edit', '8');
INSERT INTO `yckj_node_related` VALUES ('275', '3', 'article', 'manager', '', '0');
INSERT INTO `yckj_node_related` VALUES ('274', '3', 'article', 'publish', '', '0');
INSERT INTO `yckj_node_related` VALUES ('273', '3', 'article', 'view', '', '0');

-- ----------------------------
-- Table structure for `yckj_notify_alarm`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_notify_alarm`;
CREATE TABLE `yckj_notify_alarm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `node` varchar(50) NOT NULL COMMENT '事件节点',
  `module` char(30) NOT NULL COMMENT '模块名称',
  `title` varchar(250) NOT NULL COMMENT '标题',
  `body` text NOT NULL COMMENT '内容',
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '链接地址',
  `receiveuids` text NOT NULL COMMENT '接收提醒的用户ID,逗号隔开',
  `stime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '自定义发送时间',
  `alarmtype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '提醒类型：0为自定义时间，1为关联事件时间',
  `issend` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态：0未发送，1已发送',
  `diffetime` int(10) NOT NULL DEFAULT '0' COMMENT '差异量:分钟数,负数代表提前，正数代表增加',
  `eventid` varchar(60) NOT NULL DEFAULT '0' COMMENT '事件ID',
  `tablename` varchar(50) NOT NULL COMMENT '关联事件表名',
  `fieldname` varchar(50) NOT NULL COMMENT '关联事件时间字段名',
  `uptime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `idname` varchar(50) NOT NULL COMMENT '关联事件时间id名',
  `timenode` varchar(50) NOT NULL COMMENT '事件时间节点',
  PRIMARY KEY (`id`),
  KEY `notify_state` (`issend`) USING BTREE,
  KEY `notify_uid` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=775 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_notify_alarm
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_notify_email`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_notify_email`;
CREATE TABLE `yckj_notify_email` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户UiD',
  `node` varchar(50) NOT NULL COMMENT '节点名称',
  `module` char(30) NOT NULL COMMENT '模块名称',
  `email` varchar(250) NOT NULL COMMENT '邮件接受地址',
  `issend` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否已经发送',
  `title` varchar(250) NOT NULL COMMENT '邮件标题',
  `body` text NOT NULL COMMENT '邮件内容',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `sendtime` int(11) NOT NULL DEFAULT '0' COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_notify_email
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_notify_message`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_notify_message`;
CREATE TABLE `yckj_notify_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `node` varchar(50) NOT NULL COMMENT '节点名称',
  `module` char(30) NOT NULL COMMENT '模块名称',
  `title` varchar(250) NOT NULL COMMENT '标题',
  `body` text NOT NULL COMMENT '内容',
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已读',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '链接地址',
  `isalarm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为闹钟提醒',
  `senduid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '主动提醒发送用户ID',
  PRIMARY KEY (`id`),
  KEY `uid_read` (`uid`,`isread`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_notify_message
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_notify_node`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_notify_node`;
CREATE TABLE `yckj_notify_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `node` varchar(50) NOT NULL COMMENT '节点名称',
  `nodeinfo` varchar(50) NOT NULL COMMENT '节点描述',
  `module` char(30) NOT NULL COMMENT '模块名称',
  `titlekey` varchar(50) NOT NULL COMMENT '标题key',
  `contentkey` varchar(50) NOT NULL COMMENT '内容key',
  `sendemail` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否发送邮件',
  `sendmessage` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否发送短消息',
  `sendsms` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发送短信',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '信息类型：1 表示用户发送的 2表示是系统发送的',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of yckj_notify_node
-- ----------------------------
INSERT INTO `yckj_notify_node` VALUES ('1', 'message_digg', '微博的赞', 'message', 'message/default/Digg message title', 'message/default/Digg message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('2', 'message_empty_digg', '微博的无文字赞', 'message', 'message/default/Digg empty message title', 'message/default/Digg empty message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('3', 'user_follow', '新粉丝提醒', 'message', 'message/default/Follow message title', 'message/default/Follow message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('4', 'comment', '评论我的', 'message', 'message/default/Notify comment title', 'message/default/Notify comment content', '0', '0', '0', '1');
INSERT INTO `yckj_notify_node` VALUES ('5', 'normal_alarm_notily', '普通提醒', 'message', 'message/default/Alarm title', 'message/default/Alarm content', '1', '1', '1', '1');
INSERT INTO `yckj_notify_node` VALUES ('6', 'user_group_upgrade', '用户组升级', 'user', 'user/default/User group upgrade title', 'user/default/User group upgrade content', '0', '1', '0', '2');
INSERT INTO `yckj_notify_node` VALUES ('7', 'article_message', '信息中心消息提醒', 'article', 'article/default/New message title', 'article/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('8', 'article_verify_message', '信息中心新闻审核提醒', 'article', 'article/default/New verify message title', 'article/default/New verify message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('9', 'article_back_message', '信息中心审核退回提醒', 'article', 'article/default/New back title', 'article/default/New back content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('10', 'assignment_timing_message', '任务提醒', 'assignment', 'assignment/default/Timing assign title', 'assignment/default/Timing assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('11', 'assignment_new_message', '任务指派新任务提醒', 'assignment', 'assignment/default/New assign title', 'assignment/default/New assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('12', 'assignment_push_message', '任务催办提醒', 'assignment', 'assignment/default/Push assign title', 'assignment/default/Push assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('13', 'assignment_finish_message', '任务完成消息', 'assignment', 'assignment/default/Finish assign title', 'assignment/default/Finish assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('14', 'assignment_applydelay_message', '任务延期申请消息', 'assignment', 'assignment/default/Applydelay assign title', 'assignment/default/Applydelay assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('15', 'assignment_applydelayresult_message', '任务延期申请结果', 'assignment', 'assignment/default/Applydelayresult title', 'assignment/default/Applydelayresult content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('16', 'assignment_applycancel_message', '任务取消申请消息', 'assignment', 'assignment/default/Applycancel assign title', 'assignment/default/Applycancel assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('17', 'assignment_applycancelresult_message', '任务取消申请结果', 'assignment', 'assignment/default/Applycancelresult title', 'assignment/default/Applycancelresult content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('18', 'assignment_appraisal_message', '任务评价消息', 'assignment', 'assignment/default/Appraisal assign title', 'assignment/default/Appraisal assign content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('19', 'assignment_task', '任务指派', 'assignment', 'message/default/Alarm title', 'message/default/Alarm content', '1', '1', '1', '1');
INSERT INTO `yckj_notify_node` VALUES ('20', 'add_calendar_message', '日程添加提醒', 'calendar', 'calendar/default/Add schedule message title', 'calendar/default/Add schedule message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('21', 'calendar_message', '日程消息提醒', 'calendar', 'calendar/default/New schedule message title', 'calendar/default/New schedule message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('22', 'task_message', '任务消息提醒', 'calendar', 'calendar/default/New task message title', 'calendar/default/New task message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('23', 'diary_message', '工作日志消息提醒', 'diary', 'diary/default/New message title', 'diary/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('24', 'email_message', '邮件消息提醒', 'email', 'email/default/New message title', 'email/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('25', 'email_receive_message', '邮件回执提醒', 'email', 'email/default/Already receive', '', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('26', 'officialdoc_message', '通知消息提醒', 'officialdoc', 'officialdoc/default/New message title', 'officialdoc/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('27', 'officialdoc_verify_message', '信息中心通知审核提醒', 'officialdoc', 'officialdoc/default/New verify message title', 'officialdoc/default/New verify message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('28', 'officialdoc_sign_remind', '通知签收提醒', 'officialdoc', 'officialdoc/default/Sign message title', 'officialdoc/default/Sign message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('29', 'official_back_message', '通知中心审核退回提醒', 'officialdoc', 'officialdoc/default/New back title', 'officialdoc/default/New back content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('30', 'report_message', '工作汇报消息提醒', 'report', 'report/default/New message title', 'report/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('31', 'vote_publish_message', '投票发布提醒', 'vote', 'vote/default/New message title', 'vote/default/New message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('32', 'vote_update_message', '投票更新提醒', 'vote', 'vote/default/Update message title', 'vote/default/Update message content', '1', '1', '1', '2');
INSERT INTO `yckj_notify_node` VALUES ('33', 'vote_survey', '调查投票', 'vote', 'message/default/Alarm title', 'message/default/Alarm content', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `yckj_notify_sms`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_notify_sms`;
CREATE TABLE `yckj_notify_sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `touid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `node` varchar(50) NOT NULL COMMENT '节点名称',
  `module` char(30) NOT NULL COMMENT '模块名称',
  `mobile` char(11) NOT NULL COMMENT '手机号码',
  `content` varchar(255) NOT NULL COMMENT '消息内容',
  `return` varchar(255) NOT NULL,
  `posturl` varchar(255) NOT NULL,
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of yckj_notify_sms
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_onlinetime`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_onlinetime`;
CREATE TABLE `yckj_onlinetime` (
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `thismonth` smallint(6) unsigned NOT NULL DEFAULT '0',
  `total` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_onlinetime
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_page`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_page`;
CREATE TABLE `yckj_page` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `template` varchar(50) NOT NULL DEFAULT '' COMMENT '模板',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_page
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_position`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_position`;
CREATE TABLE `yckj_position` (
  `positionid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '岗位id',
  `catid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '岗位分类',
  `posname` char(20) NOT NULL COMMENT '职位名称',
  `sort` mediumint(8) NOT NULL DEFAULT '0' COMMENT '排序序号',
  `goal` text NOT NULL COMMENT '职位权限',
  `minrequirement` text NOT NULL COMMENT '最低要求',
  `number` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '在职人数',
  PRIMARY KEY (`positionid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_position
-- ----------------------------
INSERT INTO `yckj_position` VALUES ('1', '1', '总经理', '1', '', '', '0');
INSERT INTO `yckj_position` VALUES ('2', '1', '部门经理', '2', '', '', '1');
INSERT INTO `yckj_position` VALUES ('3', '1', '职员', '3', '', '', '2');

-- ----------------------------
-- Table structure for `yckj_position_category`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_position_category`;
CREATE TABLE `yckj_position_category` (
  `catid` mediumint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '岗位分类id',
  `pid` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '岗位分类父id',
  `name` char(20) NOT NULL COMMENT '岗位分类名称',
  `sort` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序id',
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_position_category
-- ----------------------------
INSERT INTO `yckj_position_category` VALUES ('1', '0', '默认分类', '1');

-- ----------------------------
-- Table structure for `yckj_position_related`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_position_related`;
CREATE TABLE `yckj_position_related` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `positionid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '岗位id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_position_related
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_position_responsibility`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_position_responsibility`;
CREATE TABLE `yckj_position_responsibility` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '职责范围与衡量标准id',
  `positionid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '所属岗位的id',
  `responsibility` text NOT NULL COMMENT '职责范围',
  `criteria` text NOT NULL COMMENT '衡量标准',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_position_responsibility
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_process`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_process`;
CREATE TABLE `yckj_process` (
  `processid` char(32) NOT NULL COMMENT '进程id',
  `expiry` int(10) DEFAULT '0' COMMENT '过期时间',
  `extra` int(10) DEFAULT '0' COMMENT '扩展字段',
  PRIMARY KEY (`processid`),
  KEY `expiry` (`expiry`) USING HASH
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_process
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_rc_type`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_rc_type`;
CREATE TABLE `yckj_rc_type` (
  `rcid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) NOT NULL COMMENT '名称',
  `classname` varchar(255) NOT NULL COMMENT '标题和文号css样式',
  `content` text NOT NULL COMMENT '原生的内容',
  `escape_content` text NOT NULL COMMENT '转义后的内容',
  PRIMARY KEY (`rcid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_rc_type
-- ----------------------------
INSERT INTO `yckj_rc_type` VALUES ('1', '默认套红', '', '套红A模板xxx( xxx )xx号关于 xxx 的通知xxxx公司   xxxx年xx月xx日', '<div><div style=\"border-bottom:4px solid #e26f50;\"><h2 style=\"text-align:center;font:700 36px/1.8 &#39;fangsong&#39;,&#39;simsun&#39;;color:#e26f50;margin-top:40px;margin-bottom:15px;\">套红A模板</h2><div style=\"padding:15px 0;text-align:center;\"><em style=\"font-style:normal;font-size:20px;font-family:&#39;fangsong&#39;,&#39;simsun&#39;;letter-spacing:3px\">xxx( xxx )xx号</em></div></div><div style=\"border-top:1px solid #e26f50;margin-top:4px;\"><div><h1 style=\"text-align:center;font:700 24px/2 &#39;fangsong&#39;,&#39;simsun&#39;;color:#666\">关于 xxx 的通知</h1><div id=\"original-content\" style=\"min-height:400px;font:16px/2 &#39;fangsong&#39;,&#39;simsun&#39;;color:#666;\"></div><div style=\"border-top:1px dotted #ddd;margin:20px 0;\"></div><div style=\"padding:20px 0;text-align:right;color:#666;letter-spacing:3px;font:18px/2 &#39;fangsong&#39;,&#39;simsun&#39;\">xxxx公司 &nbsp; <br />xxxx年xx月xx日</div></div></div></div>');

-- ----------------------------
-- Table structure for `yckj_reader`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_reader`;
CREATE TABLE `yckj_reader` (
  `readerid` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `module` char(20) NOT NULL COMMENT '模块名',
  `moduleid` mediumint(8) unsigned NOT NULL COMMENT '关联模块 id',
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户 id',
  PRIMARY KEY (`readerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='阅读记录表';

-- ----------------------------
-- Records of yckj_reader
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_regular`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_regular`;
CREATE TABLE `yckj_regular` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '验证规则',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '类型索引',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '验证规则说明',
  `regex` text NOT NULL COMMENT '正则表达式',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_regular
-- ----------------------------
INSERT INTO `yckj_regular` VALUES ('1', 'notempty', '不能为空', '');
INSERT INTO `yckj_regular` VALUES ('2', 'chinese', '只能为中文', '');
INSERT INTO `yckj_regular` VALUES ('3', 'letter', '只能为英文', '');
INSERT INTO `yckj_regular` VALUES ('4', 'num', '只能为数字', '');
INSERT INTO `yckj_regular` VALUES ('5', 'idcard', '身份证', '');
INSERT INTO `yckj_regular` VALUES ('6', 'mobile', '手机号码', '');
INSERT INTO `yckj_regular` VALUES ('7', 'money', '金额', '');
INSERT INTO `yckj_regular` VALUES ('8', 'tel', '电话号码', '');
INSERT INTO `yckj_regular` VALUES ('9', 'zipcode', '邮政编码', '');
INSERT INTO `yckj_regular` VALUES ('10', 'email', 'Email', '');

-- ----------------------------
-- Table structure for `yckj_report`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_report`;
CREATE TABLE `yckj_report` (
  `repid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '总结id',
  `subject` varchar(255) NOT NULL COMMENT '标题',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '汇报人',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '汇报时间',
  `tid` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '汇报模板id，用户自己的模板',
  `remark` text NOT NULL COMMENT '汇报备注',
  `attachmentid` text NOT NULL COMMENT '附件id',
  `toid` text NOT NULL COMMENT '汇报对象',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '汇报状态',
  `stamp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '图章',
  `isreview` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已评阅',
  `lastcommenttime` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL COMMENT '评阅内容',
  `commentline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评阅时间戳',
  `replyer` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '评阅人',
  `reminddate` int(10) NOT NULL DEFAULT '0',
  `commentcount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  `place` varchar(255) DEFAULT NULL COMMENT '填写汇报的地点',
  `isdel` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，1表示已删除，0表示未删除',
  PRIMARY KEY (`repid`),
  UNIQUE KEY `REP_ID` (`repid`) USING BTREE,
  KEY `USER_ID` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_report
-- ----------------------------
INSERT INTO `yckj_report` VALUES ('1', '超级管理员-周报', '3', '1517282443', '37', '', '', '2', '1', '0', '0', '0', '', '0', '0', '0', '0', null, '0');
INSERT INTO `yckj_report` VALUES ('2', '超级管理员-周报', '2', '1517282443', '37', '', '', '1', '1', '0', '0', '0', '', '0', '0', '0', '0', null, '0');

-- ----------------------------
-- Table structure for `yckj_report_record`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_report_record`;
CREATE TABLE `yckj_report_record` (
  `recordid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `repid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '汇报id',
  `content` text COMMENT '字段内容',
  `fieldid` int(11) NOT NULL COMMENT '字段的id',
  `fieldname` varchar(255) NOT NULL COMMENT '字段名称',
  `iswrite` int(11) NOT NULL DEFAULT '0' COMMENT '是否必填，0表示不需要，1表示需要',
  `fieldtype` int(11) NOT NULL COMMENT '字段类型，1表示长文本，2表示短文本，3表示数字，4表示日期与时间，5表示时间，6表示日期，7表示下拉',
  `fieldvalue` text COMMENT '字段值',
  PRIMARY KEY (`recordid`),
  KEY `REP_ID` (`repid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_report_record
-- ----------------------------
INSERT INTO `yckj_report_record` VALUES ('1', '1', '', '16', '原计划', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('2', '1', '1 . 招聘面试\r<br >2 . 广告投放业务\r<br >3 . 部门工作会议\r<br >4 . 外出考察学习', '17', '计划外', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('3', '1', '好的OA协同管理平台不仅仅是拥有几个特色功能，需要站在企业战略高度透视即将迎来的企业大数据时代赋予CIO们“高效管理”、“提升运营”、“推进转型”、“开拓创新”这样的新使命，还需要迎合一线使用者的使用感受。因此简洁易用，功能强大、安全稳定、可拓展、零风险是年度中国优秀CIO们考虑的主要因素：\r<br >1、简洁易用。系统不能界面布置让你眼花缭乱，让使用者要大量时间花费在熟悉系统、查询操作上面，时间长了发现大部分模块不好用，让系统最终成为发发通知、公告的摆设，而核心管理流程很难在OA协同管理平台中跑得通，最后成了一个食之无味、弃之可惜的‘鸡肋’!\r<br >2、功能强大。中小企业关注的自由协作、信息发布和文档管理;中型企业关注的职能分工、流程规范;大型企业关注的效率提升、业务督办;集团企业更关注的集中管控、风险控制。好的OA协同管理平台不仅要满足现阶段，还要考虑企业未来3-5年的发展。\r<br >3、安全稳定。OA协同管理平台使用之后，大量企业知识，管理数据都会存在系统里。平台的安全性、稳定性对于企业来说，是非常重要的。\r<br >4、可拓展。企业在持续不断的发展过程中，战略、组织架构、业务流程、角色权限都在不断的变化，尤其是基础管理需求在不断的变化，其中一些变化可以通过流程优化和表单调整完成，但还有相当多的变化无法通过产品化模块实现，因此能够开放源码的产品为首选。\r<br >5、零风险。众多网站的调查报告指出，国内企业使用OA协同管理平台的失败率高达60%，如何让企业在OA协同管理平台选型时避免错误，从而提升项目成功率，成为关键。\r<br >伴随着互联网技术的演进，以及IT与业务融合进程的深入，新时代的CIO们不断被赋予新的使命和职责。核心团队来自腾讯、谷歌埃森哲等一流企业的IBOS产品团队，在协同软件中融入强互联网元素，用了五年时间将IBOS协同管理平台变得如QQ空间、FACEBOOK、淘宝一样简单易用，你会发现只要IT人员会WORD表格，EXCEL函数，三分钟设计工作流程。同时博思协创提供的30天内无条件全额退款，终身质保服务将实施风险降到最低。\r<br >今天中国的企业正处在一个充满波动性和复杂性的特殊阶段，CIO们不仅需要在基础的技术方面出色地完成任务，还需要从数据中挖掘有价值的洞察力，并最终将其转化为创新的催化剂。借助IBOS协同管理平台，中国优秀CIO们轻松肩负“高效管理”、“提升运营”、“推进转型”、“开拓创新”这样时代赋予的新使命。', '18', '工作总结', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('4', '1', '1 . 零风险\r<br >2 . 安全稳定\r<br >3 . 功能强大\r<br >4 . 简洁易用', '19', '工作计划', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('5', '2', '市场营销活动策划', '13', '原计划', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('6', '2', '', '14', '计划外', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('7', '2', '一、促销组合是指企业根据促销的需要，对广告、销售促进、推销与公共关系等各种促销方式进行的适当选择和综合编配。二、促销组合的构成就狭义而言，促销组合只包括具有沟通性质的促销工具，主要包括各种形式的广告、包装、展销会、购买现场陈列、销售辅助物以及公共关系等。三、影响促销组合的因素1、产品类型2、推式与拉式策略3、促销目标4、产品生命周期阶段5、经济前景四、推销是指企业通过派出销售人员与一个或一个以上可能成为购买者的人交谈，作口头陈述，以促进和扩大销售。五、推销的优点主要有：1、推销注重人际关系，有利于顾客同销售人员之间建立友谊；2、推销具有较大的灵活性；3、与广告相比，推销针对性强，无效劳动较少；4、推销在大多数情况下能实现潜在交换，达成实际销售；5、推销有利于企业了解市场，提高决策水平；6、推销经常用于竞争激烈情况，也适用于推销那些价格昂贵和性能复杂的商品。推销的缺点：成本费用高六、不同类型的销售促进工具1、针对消费者的促销工具：样品、折价券、以旧换新、减价、赠奖、竞赛、商品示范等；2、针对产业用品的促销工具：折扣、赠品、特殊服务等；3、针对中间商的促销工具：购买折让、免费货品、商品推广津贴、合作广告、推销金、经销商销售竞赛等；4、针对推销人员的促销工具：红利、竞赛等。七、公共关系是指某一组织为改善与社会公众的关系，促进公众对组织的认识、理解及支持、达到树立良好的组织形象、实现组织与公众的共同利益与目标的管理活动与职能。公共关系的职能：信息监测、舆论宣传、沟通协调、危机处理八、广告媒体的特性报纸、杂志、广播、电视、直接邮寄、户外广告、国际互联网 ', '15', '工作总结', '0', '1', null);
INSERT INTO `yckj_report_record` VALUES ('8', '2', '寻找需求客户', '16', '工作计划', '0', '1', null);

-- ----------------------------
-- Table structure for `yckj_report_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_report_statistics`;
CREATE TABLE `yckj_report_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `repid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '总结ID',
  `tid` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '模板id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户UID',
  `stamp` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '图章id',
  `integration` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `scoretime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评分时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_ID` (`repid`) USING BTREE,
  KEY `USER_ID` (`uid`) USING BTREE,
  KEY `SCORE_TIME` (`scoretime`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_report_statistics
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_report_type`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_report_type`;
CREATE TABLE `yckj_report_type` (
  `typeid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '汇报类型id',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型排序',
  `typename` varchar(255) NOT NULL DEFAULT '' COMMENT '类型名字',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `intervaltype` tinyint(1) unsigned NOT NULL COMMENT '区间(0:周 1:月 2:季 3:半年 4:年 5:其他)',
  `intervals` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '自定义的间隔日期',
  `issystype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是系统自带类型',
  PRIMARY KEY (`typeid`),
  UNIQUE KEY `TYPE_ID` (`typeid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_report_type
-- ----------------------------
INSERT INTO `yckj_report_type` VALUES ('1', '0', '周总结与下周计划', '0', '0', '0', '1');
INSERT INTO `yckj_report_type` VALUES ('2', '0', '月总结与下月计划', '0', '1', '0', '1');
INSERT INTO `yckj_report_type` VALUES ('3', '0', '季总结与下季计划', '0', '2', '0', '1');
INSERT INTO `yckj_report_type` VALUES ('4', '0', '年总结与下年计划', '0', '4', '0', '1');

-- ----------------------------
-- Table structure for `yckj_resume`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume`;
CREATE TABLE `yckj_resume` (
  `resumeid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '简历id',
  `input` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '添加简历者uid',
  `positionid` smallint(6) NOT NULL DEFAULT '0' COMMENT '适合职位id',
  `entrytime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '录入时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `remark` char(255) NOT NULL DEFAULT '' COMMENT '备注',
  `remarktime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '备注时间',
  `flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '简历状态(1:面试中 2:录取3:入职4:待安排5：淘汰)',
  `statustime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '改变状态的日期时间戳，取0点',
  PRIMARY KEY (`resumeid`),
  UNIQUE KEY `ID` (`resumeid`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `flag` (`flag`) USING BTREE,
  KEY `entrytime` (`entrytime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume
-- ----------------------------
INSERT INTO `yckj_resume` VALUES ('1', '1', '3', '1392357451', '1392357544', '', '0', '0', '4', '1392357451');

-- ----------------------------
-- Table structure for `yckj_resume_bgchecks`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume_bgchecks`;
CREATE TABLE `yckj_resume_bgchecks` (
  `checkid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `resumeid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '简历id',
  `company` varchar(255) NOT NULL DEFAULT '' COMMENT '公司名称',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '公司地址',
  `phone` varchar(255) NOT NULL DEFAULT '' COMMENT '电话',
  `fax` varchar(255) NOT NULL DEFAULT '' COMMENT '传真',
  `contact` varchar(255) NOT NULL DEFAULT '' COMMENT '联系人',
  `position` varchar(255) NOT NULL DEFAULT '' COMMENT '职务',
  `entrytime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '入职时间',
  `quittime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '离职时间',
  `detail` text NOT NULL COMMENT '详细内容',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户UID',
  PRIMARY KEY (`checkid`),
  UNIQUE KEY `checkid` (`checkid`) USING BTREE,
  KEY `resumeid` (`resumeid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume_bgchecks
-- ----------------------------
INSERT INTO `yckj_resume_bgchecks` VALUES ('1', '1', '待业', '', '8392673', '', '', 'PHP软件工程师', '1383840000', '1392134400', '个人能力比较强', '1');

-- ----------------------------
-- Table structure for `yckj_resume_contact`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume_contact`;
CREATE TABLE `yckj_resume_contact` (
  `contactid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '联系id',
  `resumeid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '简历id',
  `input` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '录入者',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '录入时间',
  `contact` varchar(255) NOT NULL DEFAULT '' COMMENT '联系方式',
  `purpose` varchar(255) NOT NULL DEFAULT '' COMMENT '联系目的',
  `detail` text NOT NULL COMMENT '沟通内容',
  PRIMARY KEY (`contactid`),
  UNIQUE KEY `contactid` (`contactid`) USING BTREE,
  KEY `resumeid` (`resumeid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume_contact
-- ----------------------------
INSERT INTO `yckj_resume_contact` VALUES ('1', '1', '1', '1392393600', '电话', '通知初选', '');

-- ----------------------------
-- Table structure for `yckj_resume_detail`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume_detail`;
CREATE TABLE `yckj_resume_detail` (
  `detailid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '详细信息id',
  `resumeid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '对应的简历id',
  `positionid` smallint(6) NOT NULL DEFAULT '0' COMMENT '目标职位id',
  `realname` varchar(20) NOT NULL DEFAULT '' COMMENT '名字',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别\n(0:不详 1:男 2:女)',
  `birthday` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出生日期',
  `maritalstatus` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '婚姻状况\n(0:未婚 1:已婚 2:不详)',
  `residecity` varchar(255) NOT NULL DEFAULT '' COMMENT '居住城市',
  `idcard` varchar(255) NOT NULL DEFAULT '' COMMENT '证件号码',
  `birthplace` varchar(255) NOT NULL DEFAULT '' COMMENT '籍贯',
  `height` varchar(255) NOT NULL DEFAULT '' COMMENT '身高',
  `weight` varchar(255) NOT NULL DEFAULT '' COMMENT '体重',
  `workyears` varchar(10) NOT NULL DEFAULT '' COMMENT '工作年限',
  `education` varchar(255) NOT NULL DEFAULT '' COMMENT '学历',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '电子邮件',
  `qq` varchar(20) NOT NULL DEFAULT '' COMMENT 'QQ号码',
  `msn` varchar(64) NOT NULL DEFAULT '' COMMENT 'msn号码',
  `mobile` varchar(255) NOT NULL DEFAULT '' COMMENT '手机号码',
  `telephone` varchar(255) NOT NULL DEFAULT '' COMMENT '固定电话',
  `zipcode` varchar(255) NOT NULL DEFAULT '' COMMENT '邮政编码',
  `selfevaluation` text NOT NULL COMMENT '自我评价',
  `workexperience` text NOT NULL COMMENT '工作经历',
  `computerskill` text NOT NULL COMMENT '计算机技能',
  `eduexperience` text NOT NULL COMMENT '教育经历',
  `langskill` text NOT NULL COMMENT '语言水平',
  `professionskill` text NOT NULL COMMENT '职业技能',
  `trainexperience` text NOT NULL COMMENT '培训经历',
  `attachmentid` text NOT NULL COMMENT '附件ID',
  `recchannel` varchar(255) NOT NULL DEFAULT '' COMMENT '简历来源',
  `projectexperience` text NOT NULL COMMENT '项目经验',
  `relevantcertificates` text NOT NULL COMMENT '相关证书',
  `expectsalary` varchar(255) NOT NULL DEFAULT '' COMMENT '期望月薪',
  `workplace` varchar(255) NOT NULL DEFAULT '' COMMENT '工作地点',
  `beginworkday` varchar(255) NOT NULL DEFAULT '' COMMENT '到岗时间',
  `socialpractice` text NOT NULL COMMENT '社会实践',
  `avatarid` text NOT NULL COMMENT '头像',
  PRIMARY KEY (`detailid`),
  UNIQUE KEY `detailid` (`detailid`) USING BTREE,
  KEY `resumeid` (`resumeid`) USING BTREE,
  KEY `realname` (`realname`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume_detail
-- ----------------------------
INSERT INTO `yckj_resume_detail` VALUES ('1', '1', '3', '赵二', '2', '631123200', '0', '', '', '广东', '', '', '0', 'COLLEGE', 'wad@qq.com', '', '', '13987640978', '8143268', '343100', '做事认真负责，吃苦耐劳，有较强的意志力；\r\n积极乐观，具有团队意识。\r\n', '', '', '', '', '* 熟悉DIV+CSS、HTML/XHTML/XML等前台技术\r\n* 熟练应用Javascript、Ajax技术实现对客户端的脚步验证，\r\n* 熟悉jQuery类库。\r\n* 熟悉网站开发制作流程、Photoshop、Dreamweaver等常用开发工具的应用；\r\n* 对MVC开发模式和OOP有一定的了解\r\n* 工作踏实、主动积极，有较强的团队合作精神以及团队协作能力；\r\n', '', '', '智联招聘', '', '', '4000', '', '5天内', '', '');

-- ----------------------------
-- Table structure for `yckj_resume_interview`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume_interview`;
CREATE TABLE `yckj_resume_interview` (
  `interviewid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '面试id',
  `resumeid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '简历id',
  `interviewtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '面试时间',
  `interviewer` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '面试人',
  `method` varchar(255) NOT NULL DEFAULT '' COMMENT '面试方法',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '面试类型',
  `process` text NOT NULL COMMENT '面试过程',
  PRIMARY KEY (`interviewid`),
  UNIQUE KEY `interviewid` (`interviewid`) USING BTREE,
  KEY `resumeid` (`resumeid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume_interview
-- ----------------------------
INSERT INTO `yckj_resume_interview` VALUES ('1', '1', '1392652800', '2', '上门', '初试', '有些紧张，个人能力不错');

-- ----------------------------
-- Table structure for `yckj_resume_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_resume_statistics`;
CREATE TABLE `yckj_resume_statistics` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `new` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增数量',
  `pending` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '待安排数量',
  `interview` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '面试数量',
  `employ` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '录用数量',
  `eliminate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '淘汰数量',
  `datetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '日期时间戳',
  PRIMARY KEY (`id`),
  KEY `datetime` (`datetime`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_resume_statistics
-- ----------------------------
INSERT INTO `yckj_resume_statistics` VALUES ('1', '0', '0', '0', '0', '0', '1517155200');

-- ----------------------------
-- Table structure for `yckj_role`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_role`;
CREATE TABLE `yckj_role` (
  `roleid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `rolename` char(20) NOT NULL COMMENT '角色名称',
  `roletype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '角色类型，默认0，普通角色0，普通管理员1',
  PRIMARY KEY (`roleid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_role
-- ----------------------------
INSERT INTO `yckj_role` VALUES ('1', '管理员', '0');
INSERT INTO `yckj_role` VALUES ('2', '编辑人员', '0');
INSERT INTO `yckj_role` VALUES ('3', '普通成员', '0');

-- ----------------------------
-- Table structure for `yckj_role_related`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_role_related`;
CREATE TABLE `yckj_role_related` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `roleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_role_related
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_session`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_session`;
CREATE TABLE `yckj_session` (
  `sid` char(6) NOT NULL DEFAULT '',
  `ip1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ip2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ip3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ip4` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(15) NOT NULL DEFAULT '',
  `groupid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `invisible` tinyint(1) NOT NULL DEFAULT '0',
  `action` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastactivity` int(10) unsigned NOT NULL DEFAULT '0',
  `lastolupdate` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sid`),
  UNIQUE KEY `sid` (`sid`),
  KEY `uid` (`uid`) USING HASH
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_session
-- ----------------------------
INSERT INTO `yckj_session` VALUES ('7Rr2b2', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282444', '0');
INSERT INTO `yckj_session` VALUES ('f07UG7', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282444', '0');
INSERT INTO `yckj_session` VALUES ('wqqeUZ', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282879', '0');
INSERT INTO `yckj_session` VALUES ('ZlD16L', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282880', '0');
INSERT INTO `yckj_session` VALUES ('2wZE82', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282880', '0');
INSERT INTO `yckj_session` VALUES ('YcEUmf', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282888', '0');
INSERT INTO `yckj_session` VALUES ('cZS44Z', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282889', '0');
INSERT INTO `yckj_session` VALUES ('5kZfcS', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282890', '0');
INSERT INTO `yckj_session` VALUES ('hPxGyW', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282890', '0');
INSERT INTO `yckj_session` VALUES ('r9Jj00', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282894', '0');
INSERT INTO `yckj_session` VALUES ('HLCGqX', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282898', '0');
INSERT INTO `yckj_session` VALUES ('Ky3rYZ', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282928', '0');
INSERT INTO `yckj_session` VALUES ('2Xr01R', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282933', '0');
INSERT INTO `yckj_session` VALUES ('jJ00Va', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282934', '0');
INSERT INTO `yckj_session` VALUES ('O50kLl', '172', '16', '1', '50', '1', '', '0', '1', '0', '1517282935', '0');

-- ----------------------------
-- Table structure for `yckj_setting`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_setting`;
CREATE TABLE `yckj_setting` (
  `skey` varchar(255) NOT NULL DEFAULT '' COMMENT '键',
  `svalue` text NOT NULL COMMENT '值',
  PRIMARY KEY (`skey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_setting
-- ----------------------------
INSERT INTO `yckj_setting` VALUES ('appclosed', '0');
INSERT INTO `yckj_setting` VALUES ('unit', 'a:10:{s:8:\"fullname\";s:24:\"易仓科技有限公司\";s:9:\"shortname\";s:12:\"移仓科技\";s:8:\"corpcode\";s:4:\"yckj\";s:9:\"systemurl\";s:21:\"http://dev.office.com\";s:7:\"logourl\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:3:\"fax\";s:0:\"\";s:7:\"zipcode\";s:0:\"\";s:7:\"address\";s:0:\"\";s:10:\"adminemail\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('creditremind', '1');
INSERT INTO `yckj_setting` VALUES ('creditsformula', 'extcredits2+extcredits1*2+extcredits3*3');
INSERT INTO `yckj_setting` VALUES ('creditsformulaexp', '<span data-type=\"entry\" data-value=\"经验\" class=\"entry disabled\">经验</span><span data-type=\"operator\" data-value=\"+\" class=\"operator\">+</span><span data-type=\"entry\" data-value=\"金钱\" class=\"entry disabled\">金钱</span><span data-type=\"operator\" data-value=\"*\" class=\"operator\">*</span><span data-type=\"number\" data-value=\"2\" class=\"number\">2</span><span data-type=\"operator\" data-value=\"+\" class=\"operator\">+</span><span data-type=\"entry\" data-value=\"贡献\" class=\"entry disabled\">贡献</span><span data-type=\"operator\" data-value=\"*\" class=\"operator\">*</span><span data-type=\"number\" data-value=\"3\" class=\"number\">3</span>');
INSERT INTO `yckj_setting` VALUES ('sphinxhost', '');
INSERT INTO `yckj_setting` VALUES ('sphinxport', '');
INSERT INTO `yckj_setting` VALUES ('sphinxon', 'a:3:{s:5:\"email\";i:0;s:5:\"diary\";i:0;s:7:\"article\";i:0;}');
INSERT INTO `yckj_setting` VALUES ('sphinxsubindex', 'a:3:{s:5:\"email\";s:0:\"\";s:5:\"diary\";s:0:\"\";s:7:\"article\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('sphinxmsgindex', 'a:3:{s:5:\"email\";s:0:\"\";s:5:\"diary\";s:0:\"\";s:7:\"article\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('sphinxmaxquerytime', 'a:3:{s:5:\"email\";s:0:\"\";s:5:\"diary\";s:0:\"\";s:7:\"article\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('sphinxlimit', 'a:3:{s:5:\"email\";s:0:\"\";s:5:\"diary\";s:0:\"\";s:7:\"article\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('sphinxrank', 'a:3:{s:5:\"email\";s:23:\"SPH_RANK_PROXIMITY_BM25\";s:5:\"diary\";s:23:\"SPH_RANK_PROXIMITY_BM25\";s:7:\"article\";s:23:\"SPH_RANK_PROXIMITY_BM25\";}');
INSERT INTO `yckj_setting` VALUES ('dateformat', 'Y-n-j');
INSERT INTO `yckj_setting` VALUES ('dateconvert', '1');
INSERT INTO `yckj_setting` VALUES ('timeoffset', '8');
INSERT INTO `yckj_setting` VALUES ('timeformat', 'H:i');
INSERT INTO `yckj_setting` VALUES ('attachdir', 'data/attachment');
INSERT INTO `yckj_setting` VALUES ('attachurl', 'data/attachment');
INSERT INTO `yckj_setting` VALUES ('watermarkstatus', '0');
INSERT INTO `yckj_setting` VALUES ('thumbquality', '100');
INSERT INTO `yckj_setting` VALUES ('waterconfig', '{\"watermarkminwidth\":\"120\",\"watermarkminheight\":\"40\",\"watermarktype\":\"text\",\"watermarktrans\":\"50\",\"watermarktext\":{\"text\":\"Welcome to use the IBOS!\",\"size\":\"12\",\"color\":\"0070c0\",\"fontpath\":\"FetteSteinschrift.ttf\"},\"watermarkquality\":\"90\",\"watermarkimg\":\"static/image/watermark_preview.jpg\",\"watermarkposition\":\"9\"}');
INSERT INTO `yckj_setting` VALUES ('watermodule', '[]');
INSERT INTO `yckj_setting` VALUES ('attachsize', '10');
INSERT INTO `yckj_setting` VALUES ('filetype', 'csv, chm, pdf, zip, rar, tar, gz, bzip2, gif, jpg, jpeg, png, txt, doc, xls, ppt, docx, xlsx, pptx, htm, html');
INSERT INTO `yckj_setting` VALUES ('im', 'a:2:{s:3:\"rtx\";a:8:{s:4:\"open\";i:0;s:6:\"server\";s:9:\"127.0.0.1\";s:7:\"appport\";i:8006;s:7:\"sdkport\";i:6000;s:4:\"push\";a:2:{s:4:\"note\";i:0;s:3:\"msg\";i:0;}s:3:\"sso\";i:0;s:14:\"reverselanding\";i:0;s:8:\"syncuser\";i:0;}s:2:\"qq\";a:10:{s:4:\"open\";i:0;s:2:\"id\";s:0:\"\";s:5:\"token\";s:0:\"\";s:5:\"appid\";s:0:\"\";s:9:\"appsecret\";s:0:\"\";s:3:\"sso\";i:0;s:4:\"push\";a:2:{s:4:\"note\";i:0;s:3:\"msg\";i:0;}s:8:\"syncuser\";i:0;s:7:\"syncorg\";i:0;s:10:\"showunread\";i:0;}}');
INSERT INTO `yckj_setting` VALUES ('custombackup', '');
INSERT INTO `yckj_setting` VALUES ('mail', 'a:5:{s:8:\"mailsend\";i:1;s:6:\"server\";a:0:{}s:13:\"maildelimiter\";i:2;s:12:\"mailusername\";i:1;s:14:\"sendmailsilent\";i:1;}');
INSERT INTO `yckj_setting` VALUES ('account', 'a:9:{s:10:\"expiration\";i:0;s:9:\"minlength\";i:5;s:5:\"mixed\";i:0;s:10:\"errorlimit\";i:1;s:11:\"errorrepeat\";i:5;s:9:\"errortime\";i:15;s:9:\"autologin\";i:0;s:10:\"allowshare\";i:1;s:7:\"timeout\";i:720;}');
INSERT INTO `yckj_setting` VALUES ('license', '');
INSERT INTO `yckj_setting` VALUES ('upgrade', '');
INSERT INTO `yckj_setting` VALUES ('verhash', '');
INSERT INTO `yckj_setting` VALUES ('smsenabled', '0');
INSERT INTO `yckj_setting` VALUES ('smsinterface', '1');
INSERT INTO `yckj_setting` VALUES ('smssetup', 'a:2:{s:9:\"accesskey\";s:0:\"\";s:9:\"secretkey\";s:0:\"\";}');
INSERT INTO `yckj_setting` VALUES ('smsmodule', 'a:0:{}');
INSERT INTO `yckj_setting` VALUES ('emailtable_info', 'a:2:{i:0;a:1:{s:4:\"memo\";s:0:\"\";}i:1;a:2:{s:4:\"memo\";s:0:\"\";s:11:\"displayname\";s:12:\"默认归档\";}}');
INSERT INTO `yckj_setting` VALUES ('emailtableids', 'a:2:{i:0;i:0;i:1;i:1;}');
INSERT INTO `yckj_setting` VALUES ('diarytable_info', 'a:2:{i:0;a:1:{s:4:\"memo\";s:0:\"\";}i:1;a:2:{s:4:\"memo\";s:0:\"\";s:11:\"displayname\";s:12:\"默认归档\";}}');
INSERT INTO `yckj_setting` VALUES ('diarytableids', 'a:2:{i:0;i:0;i:1;i:1;}');
INSERT INTO `yckj_setting` VALUES ('cronarchive', 'a:3:{s:11:\"cronarchive\";a:1:{s:5:\"diary\";a:3:{s:13:\"sourcetableid\";i:0;s:13:\"targetabletid\";s:1:\"1\";s:10:\"conditions\";a:2:{s:13:\"sourcetableid\";s:1:\"0\";s:9:\"timerange\";i:3;}}}s:5:\"email\";a:3:{s:13:\"sourcetableid\";i:0;s:13:\"targettableid\";s:1:\"1\";s:10:\"conditions\";a:2:{s:13:\"sourcetableid\";s:1:\"0\";s:9:\"timerange\";i:6;}}s:5:\"diary\";a:3:{s:13:\"sourcetableid\";i:0;s:13:\"targettableid\";s:1:\"1\";s:10:\"conditions\";a:2:{s:13:\"sourcetableid\";s:1:\"0\";s:9:\"timerange\";i:6;}}}');
INSERT INTO `yckj_setting` VALUES ('logtableid', '0');
INSERT INTO `yckj_setting` VALUES ('iboscloud', 'a:5:{s:5:\"appid\";s:0:\"\";s:6:\"secret\";s:0:\"\";s:6:\"isopen\";i:0;s:7:\"apilist\";a:0:{}s:3:\"url\";s:21:\"http://cloud.ibos.cn/\";}');
INSERT INTO `yckj_setting` VALUES ('websiteuid', '0');
INSERT INTO `yckj_setting` VALUES ('skin', 'white');
INSERT INTO `yckj_setting` VALUES ('aeskey', '6fdf0ea653Uy3330I33zyf02YwQgLrD23lLIuFLuIPL');
INSERT INTO `yckj_setting` VALUES ('corpid', '0');
INSERT INTO `yckj_setting` VALUES ('qrcode', '0');
INSERT INTO `yckj_setting` VALUES ('cobinding', '0');
INSERT INTO `yckj_setting` VALUES ('coinfo', '');
INSERT INTO `yckj_setting` VALUES ('cacheuserstatus', '1');
INSERT INTO `yckj_setting` VALUES ('cacheuserconfig', '{\"offset\":\"0\",\"limit\":\"1000\",\"uid\":\"1\"}');
INSERT INTO `yckj_setting` VALUES ('version', '4.4.2 open');
INSERT INTO `yckj_setting` VALUES ('wbmovement', 'a:0:{}');
INSERT INTO `yckj_setting` VALUES ('wbnums', '140');
INSERT INTO `yckj_setting` VALUES ('wbpostfrequency', '10');
INSERT INTO `yckj_setting` VALUES ('wbposttype', 'a:3:{s:5:\"image\";i:1;s:5:\"topic\";i:0;s:6:\"praise\";i:0;}');
INSERT INTO `yckj_setting` VALUES ('wbwatermark', '0');
INSERT INTO `yckj_setting` VALUES ('wbwcenabled', '0');
INSERT INTO `yckj_setting` VALUES ('articleapprover', '0');
INSERT INTO `yckj_setting` VALUES ('articlecommentenable', '1');
INSERT INTO `yckj_setting` VALUES ('articlevoteenable', '1');
INSERT INTO `yckj_setting` VALUES ('articlemessageenable', '1');
INSERT INTO `yckj_setting` VALUES ('articlethumbenable', '1');
INSERT INTO `yckj_setting` VALUES ('articlethumbwh', '160,120');
INSERT INTO `yckj_setting` VALUES ('calendaraddschedule', '1');
INSERT INTO `yckj_setting` VALUES ('calendareditschedule', '0');
INSERT INTO `yckj_setting` VALUES ('calendarworkingtime', '8,18');
INSERT INTO `yckj_setting` VALUES ('calendaredittask', '0');
INSERT INTO `yckj_setting` VALUES ('diaryconfig', 'a:11:{s:7:\"lockday\";s:1:\"0\";s:14:\"sharepersonnel\";s:1:\"1\";s:12:\"sharecomment\";s:1:\"1\";s:9:\"attention\";s:1:\"1\";s:10:\"autoreview\";s:1:\"1\";s:15:\"autoreviewstamp\";s:1:\"1\";s:13:\"remindcontent\";s:0:\"\";s:11:\"stampenable\";s:1:\"1\";s:11:\"pointsystem\";s:1:\"5\";s:12:\"stampdetails\";s:40:\"0:10,0:9,0:8,0:7,0:6,4:5,5:4,2:3,1:2,8:1\";s:10:\"reviewlock\";i:0;}');
INSERT INTO `yckj_setting` VALUES ('emailexternalmail', '0');
INSERT INTO `yckj_setting` VALUES ('emailrecall', '0');
INSERT INTO `yckj_setting` VALUES ('emailsystemremind', '0');
INSERT INTO `yckj_setting` VALUES ('emailroleallocation', '0');
INSERT INTO `yckj_setting` VALUES ('emaildefsize', '50');
INSERT INTO `yckj_setting` VALUES ('filedefsize', '50');
INSERT INTO `yckj_setting` VALUES ('filecompmanager', '');
INSERT INTO `yckj_setting` VALUES ('filecloudopen', '0');
INSERT INTO `yckj_setting` VALUES ('filecloudid', '0');
INSERT INTO `yckj_setting` VALUES ('docconfig', 'a:2:{s:11:\"docapprover\";i:0;s:16:\"doccommentenable\";i:1;}');
INSERT INTO `yckj_setting` VALUES ('recruitconfig', 'a:33:{s:15:\"recruitrealname\";s:10:\"1,notempty\";s:10:\"recruitsex\";s:16:\"1,notrequirement\";s:15:\"recruitbirthday\";s:10:\"1,notempty\";s:17:\"recruitbirthplace\";s:16:\"1,notrequirement\";s:16:\"recruitworkyears\";s:16:\"1,notrequirement\";s:16:\"recruiteducation\";s:16:\"1,notrequirement\";s:13:\"recruitstatus\";s:10:\"1,notempty\";s:13:\"recruitidcard\";s:8:\"0,idcard\";s:13:\"recruitheight\";s:5:\"0,num\";s:13:\"recruitweight\";s:5:\"0,num\";s:20:\"recruitmaritalstatus\";s:16:\"0,notrequirement\";s:17:\"recruitresidecity\";s:16:\"1,notrequirement\";s:14:\"recruitzipcode\";s:9:\"1,zipcode\";s:13:\"recruitmobile\";s:8:\"1,mobile\";s:12:\"rucruitemail\";s:7:\"1,email\";s:16:\"recruittelephone\";s:5:\"1,tel\";s:9:\"recruitqq\";s:5:\"0,num\";s:10:\"recruitmsn\";s:16:\"0,notrequirement\";s:19:\"recruitbeginworkday\";s:16:\"1,notrequirement\";s:21:\"recruittargetposition\";s:16:\"1,notrequirement\";s:19:\"recruitexpectsalary\";s:16:\"1,notrequirement\";s:16:\"recruitworkplace\";s:16:\"1,notrequirement\";s:17:\"recruitrecchannel\";s:16:\"1,notrequirement\";s:21:\"recruitworkexperience\";s:16:\"1,notrequirement\";s:24:\"recruitprojectexperience\";s:16:\"1,notrequirement\";s:20:\"recruiteduexperience\";s:16:\"1,notrequirement\";s:16:\"recruitlangskill\";s:16:\"0,notrequirement\";s:20:\"recruitcomputerskill\";s:16:\"0,notrequirement\";s:22:\"recruitprofessionskill\";s:16:\"0,notrequirement\";s:22:\"recruittrainexperience\";s:16:\"0,notrequirement\";s:21:\"recruitselfevaluation\";s:16:\"0,notrequirement\";s:27:\"recruitrelevantcertificates\";s:16:\"0,notrequirement\";s:21:\"recruitsocialpractice\";s:16:\"0,notrequirement\";}');
INSERT INTO `yckj_setting` VALUES ('reportconfig', 'a:6:{s:16:\"reporttypemanage\";s:0:\"\";s:11:\"stampenable\";i:1;s:11:\"pointsystem\";i:5;s:12:\"stampdetails\";s:40:\"0:10,0:9,0:8,0:7,0:6,4:5,5:4,2:3,1:2,8:1\";s:10:\"autoreview\";i:1;s:15:\"autoreviewstamp\";i:1;}');
INSERT INTO `yckj_setting` VALUES ('statmodules', 'a:0:{}');
INSERT INTO `yckj_setting` VALUES ('votethumbenable', '0');
INSERT INTO `yckj_setting` VALUES ('votethumbwh', '0,0');

-- ----------------------------
-- Table structure for `yckj_stamp`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_stamp`;
CREATE TABLE `yckj_stamp` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `code` varchar(30) NOT NULL DEFAULT '' COMMENT '显示名称',
  `stamp` varchar(100) NOT NULL DEFAULT '' COMMENT '图章地址',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '图标地址',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否系统自带图章',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_stamp
-- ----------------------------
INSERT INTO `yckj_stamp` VALUES ('1', '1', '已阅', 'data/stamp/001.png', 'data/stamp/001.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('2', '2', '有进步', 'data/stamp/002.png', 'data/stamp/002.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('3', '3', '继续努力', 'data/stamp/003.png', 'data/stamp/003.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('4', '4', '干得不错', 'data/stamp/004.png', 'data/stamp/004.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('5', '5', '很给力', 'data/stamp/005.png', 'data/stamp/005.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('6', '6', '非常赞', 'data/stamp/006.png', 'data/stamp/006.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('7', '7', '待提高', 'data/stamp/007.png', 'data/stamp/007.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('8', '8', '不理想', 'data/stamp/008.png', 'data/stamp/008.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('9', '9', '不给力', 'data/stamp/009.png', 'data/stamp/009.small.png', '1');
INSERT INTO `yckj_stamp` VALUES ('10', '10', '没完成', 'data/stamp/010.png', 'data/stamp/010.small.png', '1');

-- ----------------------------
-- Table structure for `yckj_syscache`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_syscache`;
CREATE TABLE `yckj_syscache` (
  `name` varchar(32) NOT NULL COMMENT '缓存类型名称',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '缓存类型，1为数组，其余为0',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间戳',
  `value` mediumblob NOT NULL COMMENT '值',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_syscache
-- ----------------------------
INSERT INTO `yckj_syscache` VALUES ('setting', '1', '1517282444', 0x613A38373A7B733A393A22617070636C6F736564223B733A313A2230223B733A343A22756E6974223B613A31303A7B733A383A2266756C6C6E616D65223B733A32343A22E69893E4BB93E7A791E68A80E69C89E99990E585ACE58FB8223B733A393A2273686F72746E616D65223B733A31323A22E7A7BBE4BB93E7A791E68A80223B733A383A22636F7270636F6465223B733A343A2279636B6A223B733A393A2273797374656D75726C223B733A32313A22687474703A2F2F6465762E6F66666963652E636F6D223B733A373A226C6F676F75726C223B733A303A22223B733A353A2270686F6E65223B733A303A22223B733A333A22666178223B733A303A22223B733A373A227A6970636F6465223B733A303A22223B733A373A2261646472657373223B733A303A22223B733A31303A2261646D696E656D61696C223B733A303A22223B7D733A31323A2263726564697472656D696E64223B733A313A2231223B733A31343A2263726564697473666F726D756C61223B733A36363A2224757365725B276578746372656469747332275D2B24757365725B276578746372656469747331275D2A322B24757365725B276578746372656469747333275D2A33223B733A31373A2263726564697473666F726D756C61657870223B733A3633343A223C7370616E20646174612D747970653D22656E7472792220646174612D76616C75653D22E7BB8FE9AA8C2220636C6173733D22656E7472792064697361626C6564223EE7BB8FE9AA8C3C2F7370616E3E3C7370616E20646174612D747970653D226F70657261746F722220646174612D76616C75653D222B2220636C6173733D226F70657261746F72223E2B3C2F7370616E3E3C7370616E20646174612D747970653D22656E7472792220646174612D76616C75653D22E98791E992B12220636C6173733D22656E7472792064697361626C6564223EE98791E992B13C2F7370616E3E3C7370616E20646174612D747970653D226F70657261746F722220646174612D76616C75653D222A2220636C6173733D226F70657261746F72223E2A3C2F7370616E3E3C7370616E20646174612D747970653D226E756D6265722220646174612D76616C75653D22322220636C6173733D226E756D626572223E323C2F7370616E3E3C7370616E20646174612D747970653D226F70657261746F722220646174612D76616C75653D222B2220636C6173733D226F70657261746F72223E2B3C2F7370616E3E3C7370616E20646174612D747970653D22656E7472792220646174612D76616C75653D22E8B4A1E78CAE2220636C6173733D22656E7472792064697361626C6564223EE8B4A1E78CAE3C2F7370616E3E3C7370616E20646174612D747970653D226F70657261746F722220646174612D76616C75653D222A2220636C6173733D226F70657261746F72223E2A3C2F7370616E3E3C7370616E20646174612D747970653D226E756D6265722220646174612D76616C75653D22332220636C6173733D226E756D626572223E333C2F7370616E3E223B733A31303A22737068696E78686F7374223B733A303A22223B733A31303A22737068696E78706F7274223B733A303A22223B733A383A22737068696E786F6E223B613A333A7B733A353A22656D61696C223B693A303B733A353A226469617279223B693A303B733A373A2261727469636C65223B693A303B7D733A31343A22737068696E78737562696E646578223B613A333A7B733A353A22656D61696C223B733A303A22223B733A353A226469617279223B733A303A22223B733A373A2261727469636C65223B733A303A22223B7D733A31343A22737068696E786D7367696E646578223B613A333A7B733A353A22656D61696C223B733A303A22223B733A353A226469617279223B733A303A22223B733A373A2261727469636C65223B733A303A22223B7D733A31383A22737068696E786D6178717565727974696D65223B613A333A7B733A353A22656D61696C223B733A303A22223B733A353A226469617279223B733A303A22223B733A373A2261727469636C65223B733A303A22223B7D733A31313A22737068696E786C696D6974223B613A333A7B733A353A22656D61696C223B733A303A22223B733A353A226469617279223B733A303A22223B733A373A2261727469636C65223B733A303A22223B7D733A31303A22737068696E7872616E6B223B613A333A7B733A353A22656D61696C223B733A32333A225350485F52414E4B5F50524F58494D4954595F424D3235223B733A353A226469617279223B733A32333A225350485F52414E4B5F50524F58494D4954595F424D3235223B733A373A2261727469636C65223B733A32333A225350485F52414E4B5F50524F58494D4954595F424D3235223B7D733A31303A2264617465666F726D6174223B733A353A22592D6E2D6A223B733A31313A2264617465636F6E76657274223B733A313A2231223B733A31303A2274696D656F6666736574223B733A313A2238223B733A31303A2274696D65666F726D6174223B733A333A22483A69223B733A393A22617474616368646972223B733A31353A22646174612F6174746163686D656E74223B733A393A2261747461636875726C223B733A31353A22646174612F6174746163686D656E74223B733A31353A2277617465726D61726B737461747573223B733A313A2230223B733A31323A227468756D627175616C697479223B733A333A22313030223B733A31313A227761746572636F6E666967223B733A3331343A227B2277617465726D61726B6D696E7769647468223A22313230222C2277617465726D61726B6D696E686569676874223A223430222C2277617465726D61726B74797065223A2274657874222C2277617465726D61726B7472616E73223A223530222C2277617465726D61726B74657874223A7B2274657874223A2257656C636F6D6520746F20757365207468652049424F5321222C2273697A65223A223132222C22636F6C6F72223A22303037306330222C22666F6E7470617468223A224665747465537465696E736368726966742E747466227D2C2277617465726D61726B7175616C697479223A223930222C2277617465726D61726B696D67223A227374617469632F696D6167652F77617465726D61726B5F707265766965772E6A7067222C2277617465726D61726B706F736974696F6E223A2239227D223B733A31313A2277617465726D6F64756C65223B733A323A225B5D223B733A31303A2261747461636873697A65223B733A323A223130223B733A383A2266696C6574797065223B733A3130393A226373762C2063686D2C207064662C207A69702C207261722C207461722C20677A2C20627A6970322C206769662C206A70672C206A7065672C20706E672C207478742C20646F632C20786C732C207070742C20646F63782C20786C73782C20707074782C2068746D2C2068746D6C223B733A323A22696D223B613A323A7B733A333A22727478223B613A383A7B733A343A226F70656E223B693A303B733A363A22736572766572223B733A393A223132372E302E302E31223B733A373A22617070706F7274223B693A383030363B733A373A2273646B706F7274223B693A363030303B733A343A2270757368223B613A323A7B733A343A226E6F7465223B693A303B733A333A226D7367223B693A303B7D733A333A2273736F223B693A303B733A31343A22726576657273656C616E64696E67223B693A303B733A383A2273796E6375736572223B693A303B7D733A323A227171223B613A31303A7B733A343A226F70656E223B693A303B733A323A226964223B733A303A22223B733A353A22746F6B656E223B733A303A22223B733A353A226170706964223B733A303A22223B733A393A22617070736563726574223B733A303A22223B733A333A2273736F223B693A303B733A343A2270757368223B613A323A7B733A343A226E6F7465223B693A303B733A333A226D7367223B693A303B7D733A383A2273796E6375736572223B693A303B733A373A2273796E636F7267223B693A303B733A31303A2273686F77756E72656164223B693A303B7D7D733A31323A22637573746F6D6261636B7570223B733A303A22223B733A343A226D61696C223B613A353A7B733A383A226D61696C73656E64223B693A313B733A363A22736572766572223B613A303A7B7D733A31333A226D61696C64656C696D69746572223B693A323B733A31323A226D61696C757365726E616D65223B693A313B733A31343A2273656E646D61696C73696C656E74223B693A313B7D733A373A226163636F756E74223B613A393A7B733A31303A2265787069726174696F6E223B693A303B733A393A226D696E6C656E677468223B693A353B733A353A226D69786564223B693A303B733A31303A226572726F726C696D6974223B693A313B733A31313A226572726F72726570656174223B693A353B733A393A226572726F7274696D65223B693A31353B733A393A226175746F6C6F67696E223B693A303B733A31303A22616C6C6F777368617265223B693A313B733A373A2274696D656F7574223B693A3732303B7D733A373A226C6963656E7365223B733A303A22223B733A373A2275706772616465223B733A303A22223B733A373A2276657268617368223B733A333A224E6B56223B733A31303A22736D73656E61626C6564223B733A313A2230223B733A31323A22736D73696E74657266616365223B733A313A2231223B733A383A22736D737365747570223B613A323A7B733A393A226163636573736B6579223B733A303A22223B733A393A227365637265746B6579223B733A303A22223B7D733A393A22736D736D6F64756C65223B613A303A7B7D733A31353A22656D61696C7461626C655F696E666F223B613A323A7B693A303B613A313A7B733A343A226D656D6F223B733A303A22223B7D693A313B613A323A7B733A343A226D656D6F223B733A303A22223B733A31313A22646973706C61796E616D65223B733A31323A22E9BB98E8AEA4E5BD92E6A1A3223B7D7D733A31333A22656D61696C7461626C65696473223B613A323A7B693A303B693A303B693A313B693A313B7D733A31353A2264696172797461626C655F696E666F223B613A323A7B693A303B613A313A7B733A343A226D656D6F223B733A303A22223B7D693A313B613A323A7B733A343A226D656D6F223B733A303A22223B733A31313A22646973706C61796E616D65223B733A31323A22E9BB98E8AEA4E5BD92E6A1A3223B7D7D733A31333A2264696172797461626C65696473223B613A323A7B693A303B693A303B693A313B693A313B7D733A31313A2263726F6E61726368697665223B613A333A7B733A31313A2263726F6E61726368697665223B613A313A7B733A353A226469617279223B613A333A7B733A31333A22736F757263657461626C656964223B693A303B733A31333A2274617267657461626C65746964223B733A313A2231223B733A31303A22636F6E646974696F6E73223B613A323A7B733A31333A22736F757263657461626C656964223B733A313A2230223B733A393A2274696D6572616E6765223B693A333B7D7D7D733A353A22656D61696C223B613A333A7B733A31333A22736F757263657461626C656964223B693A303B733A31333A227461726765747461626C656964223B733A313A2231223B733A31303A22636F6E646974696F6E73223B613A323A7B733A31333A22736F757263657461626C656964223B733A313A2230223B733A393A2274696D6572616E6765223B693A363B7D7D733A353A226469617279223B613A333A7B733A31333A22736F757263657461626C656964223B693A303B733A31333A227461726765747461626C656964223B733A313A2231223B733A31303A22636F6E646974696F6E73223B613A323A7B733A31333A22736F757263657461626C656964223B733A313A2230223B733A393A2274696D6572616E6765223B693A363B7D7D7D733A31303A226C6F677461626C656964223B733A313A2230223B733A393A2269626F73636C6F7564223B613A353A7B733A353A226170706964223B733A303A22223B733A363A22736563726574223B733A303A22223B733A363A2269736F70656E223B693A303B733A373A226170696C697374223B613A303A7B7D733A333A2275726C223B733A32313A22687474703A2F2F636C6F75642E69626F732E636E2F223B7D733A31303A2277656273697465756964223B733A313A2230223B733A343A22736B696E223B733A353A227768697465223B733A363A226165736B6579223B733A34333A22366664663065613635335579333333304933337A79663032597751674C724432336C4C4975464C7549504C223B733A363A22636F72706964223B733A313A2230223B733A363A227172636F6465223B733A313A2230223B733A393A22636F62696E64696E67223B733A313A2230223B733A363A22636F696E666F223B733A303A22223B733A31353A22636163686575736572737461747573223B733A313A2231223B733A31353A22636163686575736572636F6E666967223B733A33393A227B226F6666736574223A2230222C226C696D6974223A2231303030222C22756964223A2231227D223B733A373A2276657273696F6E223B733A31303A22342E342E32206F70656E223B733A31303A2277626D6F76656D656E74223B613A303A7B7D733A363A2277626E756D73223B733A333A22313430223B733A31353A227762706F73746672657175656E6379223B733A323A223130223B733A31303A227762706F737474797065223B613A333A7B733A353A22696D616765223B693A313B733A353A22746F706963223B693A303B733A363A22707261697365223B693A303B7D733A31313A22776277617465726D61726B223B733A313A2230223B733A31313A2277627763656E61626C6564223B733A313A2230223B733A31353A2261727469636C65617070726F766572223B733A313A2230223B733A32303A2261727469636C65636F6D6D656E74656E61626C65223B733A313A2231223B733A31373A2261727469636C65766F7465656E61626C65223B733A313A2231223B733A32303A2261727469636C656D657373616765656E61626C65223B733A313A2231223B733A31383A2261727469636C657468756D62656E61626C65223B733A313A2231223B733A31343A2261727469636C657468756D627768223B733A373A223136302C313230223B733A31393A2263616C656E6461726164647363686564756C65223B733A313A2231223B733A32303A2263616C656E646172656469747363686564756C65223B733A313A2230223B733A31393A2263616C656E646172776F726B696E6774696D65223B733A343A22382C3138223B733A31363A2263616C656E646172656469747461736B223B733A313A2230223B733A31313A226469617279636F6E666967223B613A31313A7B733A373A226C6F636B646179223B733A313A2230223B733A31343A227368617265706572736F6E6E656C223B733A313A2231223B733A31323A227368617265636F6D6D656E74223B733A313A2231223B733A393A22617474656E74696F6E223B733A313A2231223B733A31303A226175746F726576696577223B733A313A2231223B733A31353A226175746F7265766965777374616D70223B733A313A2231223B733A31333A2272656D696E64636F6E74656E74223B733A303A22223B733A31313A227374616D70656E61626C65223B733A313A2231223B733A31313A22706F696E7473797374656D223B733A313A2235223B733A31323A227374616D7064657461696C73223B733A34303A22303A31302C303A392C303A382C303A372C303A362C343A352C353A342C323A332C313A322C383A31223B733A31303A227265766965776C6F636B223B693A303B7D733A31373A22656D61696C65787465726E616C6D61696C223B733A313A2230223B733A31313A22656D61696C726563616C6C223B733A313A2230223B733A31373A22656D61696C73797374656D72656D696E64223B733A313A2230223B733A31393A22656D61696C726F6C65616C6C6F636174696F6E223B733A313A2230223B733A31323A22656D61696C64656673697A65223B733A323A223530223B733A31313A2266696C6564656673697A65223B733A323A223530223B733A31353A2266696C65636F6D706D616E61676572223B733A303A22223B733A31333A2266696C65636C6F75646F70656E223B733A313A2230223B733A31313A2266696C65636C6F75646964223B733A313A2230223B733A393A22646F63636F6E666967223B613A323A7B733A31313A22646F63617070726F766572223B693A303B733A31363A22646F63636F6D6D656E74656E61626C65223B693A313B7D733A31333A2272656372756974636F6E666967223B613A33333A7B733A31353A22726563727569747265616C6E616D65223B733A31303A22312C6E6F74656D707479223B733A31303A2272656372756974736578223B733A31363A22312C6E6F74726571756972656D656E74223B733A31353A22726563727569746269727468646179223B733A31303A22312C6E6F74656D707479223B733A31373A22726563727569746269727468706C616365223B733A31363A22312C6E6F74726571756972656D656E74223B733A31363A2272656372756974776F726B7965617273223B733A31363A22312C6E6F74726571756972656D656E74223B733A31363A2272656372756974656475636174696F6E223B733A31363A22312C6E6F74726571756972656D656E74223B733A31333A2272656372756974737461747573223B733A31303A22312C6E6F74656D707479223B733A31333A2272656372756974696463617264223B733A383A22302C696463617264223B733A31333A2272656372756974686569676874223B733A353A22302C6E756D223B733A31333A2272656372756974776569676874223B733A353A22302C6E756D223B733A32303A22726563727569746D61726974616C737461747573223B733A31363A22302C6E6F74726571756972656D656E74223B733A31373A227265637275697472657369646563697479223B733A31363A22312C6E6F74726571756972656D656E74223B733A31343A22726563727569747A6970636F6465223B733A393A22312C7A6970636F6465223B733A31333A22726563727569746D6F62696C65223B733A383A22312C6D6F62696C65223B733A31323A2272756372756974656D61696C223B733A373A22312C656D61696C223B733A31363A227265637275697474656C6570686F6E65223B733A353A22312C74656C223B733A393A22726563727569747171223B733A353A22302C6E756D223B733A31303A22726563727569746D736E223B733A31363A22302C6E6F74726571756972656D656E74223B733A31393A2272656372756974626567696E776F726B646179223B733A31363A22312C6E6F74726571756972656D656E74223B733A32313A2272656372756974746172676574706F736974696F6E223B733A31363A22312C6E6F74726571756972656D656E74223B733A31393A227265637275697465787065637473616C617279223B733A31363A22312C6E6F74726571756972656D656E74223B733A31363A2272656372756974776F726B706C616365223B733A31363A22312C6E6F74726571756972656D656E74223B733A31373A22726563727569747265636368616E6E656C223B733A31363A22312C6E6F74726571756972656D656E74223B733A32313A2272656372756974776F726B657870657269656E6365223B733A31363A22312C6E6F74726571756972656D656E74223B733A32343A227265637275697470726F6A656374657870657269656E6365223B733A31363A22312C6E6F74726571756972656D656E74223B733A32303A2272656372756974656475657870657269656E6365223B733A31363A22312C6E6F74726571756972656D656E74223B733A31363A22726563727569746C616E67736B696C6C223B733A31363A22302C6E6F74726571756972656D656E74223B733A32303A2272656372756974636F6D7075746572736B696C6C223B733A31363A22302C6E6F74726571756972656D656E74223B733A32323A227265637275697470726F66657373696F6E736B696C6C223B733A31363A22302C6E6F74726571756972656D656E74223B733A32323A2272656372756974747261696E657870657269656E6365223B733A31363A22302C6E6F74726571756972656D656E74223B733A32313A227265637275697473656C666576616C756174696F6E223B733A31363A22302C6E6F74726571756972656D656E74223B733A32373A227265637275697472656C6576616E74636572746966696361746573223B733A31363A22302C6E6F74726571756972656D656E74223B733A32313A2272656372756974736F6369616C7072616374696365223B733A31363A22302C6E6F74726571756972656D656E74223B7D733A31323A227265706F7274636F6E666967223B613A363A7B733A31363A227265706F7274747970656D616E616765223B733A303A22223B733A31313A227374616D70656E61626C65223B693A313B733A31313A22706F696E7473797374656D223B693A353B733A31323A227374616D7064657461696C73223B733A34303A22303A31302C303A392C303A382C303A372C303A362C343A352C353A342C323A332C313A322C383A31223B733A31303A226175746F726576696577223B693A313B733A31353A226175746F7265766965777374616D70223B693A313B7D733A31313A22737461746D6F64756C6573223B613A303A7B7D733A31353A22766F74657468756D62656E61626C65223B733A313A2230223B733A31313A22766F74657468756D627768223B733A333A22302C30223B733A31303A2265787463726564697473223B613A333A7B693A313B613A363A7B733A333A22636964223B733A313A2231223B733A363A2273797374656D223B733A313A2231223B733A343A226E616D65223B733A363A22E7BB8FE9AA8C223B733A373A22696E697469616C223B733A313A2230223B733A353A226C6F776572223B733A313A2230223B733A363A22656E61626C65223B733A313A2231223B7D693A323B613A363A7B733A333A22636964223B733A313A2232223B733A363A2273797374656D223B733A313A2231223B733A343A226E616D65223B733A363A22E98791E992B1223B733A373A22696E697469616C223B733A313A2230223B733A353A226C6F776572223B733A313A2230223B733A363A22656E61626C65223B733A313A2231223B7D693A333B613A363A7B733A333A22636964223B733A313A2233223B733A363A2273797374656D223B733A313A2231223B733A343A226E616D65223B733A363A22E8B4A1E78CAE223B733A373A22696E697469616C223B733A313A2230223B733A353A226C6F776572223B733A313A2230223B733A363A22656E61626C65223B733A313A2231223B7D7D733A31313A226372656469746E616D6573223B4E3B7D);
INSERT INTO `yckj_syscache` VALUES ('nav', '1', '1517282444', 0x613A363A7B693A313B613A31323A7B733A323A226964223B733A313A2231223B733A333A22706964223B693A303B733A343A226E616D65223B733A363A22E9A696E9A1B5223B733A333A2275726C223B733A31383A226A6176617363726970743A766F6964283029223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2231223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A323A7B693A303B613A31313A7B733A323A226964223B733A323A223131223B733A333A22706964223B733A313A2231223B733A343A226E616D65223B733A31323A22E58A9EE585ACE997A8E688B7223B733A333A2275726C223B733A31383A226D61696E2F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2231223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A313B613A31313A7B733A323A226964223B733A323A223130223B733A333A22706964223B733A313A2231223B733A343A226E616D65223B733A31323A22E4B8AAE4BABAE997A8E688B7223B733A333A2275726C223B733A31363A22776569626F2F686F6D652F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2232223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D7D7D693A323B613A31323A7B733A323A226964223B733A313A2232223B733A333A22706964223B693A303B733A343A226E616D65223B733A363A22E982AEE4BBB6223B733A333A2275726C223B733A31363A22656D61696C2F6C6973742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2232223B733A363A226D6F64756C65223B733A353A22656D61696C223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A303A7B7D7D693A31343B613A31323A7B733A323A226964223B733A323A223134223B733A333A22706964223B693A303B733A343A226E616D65223B733A363A22E697A5E7A88B223B733A333A2275726C223B733A32333A2263616C656E6461722F7363686564756C652F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2233223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A303A7B7D7D693A333B613A31323A7B733A323A226964223B733A313A2233223B733A333A22706964223B693A303B733A343A226E616D65223B733A31323A22E4B8AAE4BABAE58A9EE585AC223B733A333A2275726C223B733A31393A226A6176617363726970743A766F69642830293B223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2235223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A353A7B693A303B613A31313A7B733A323A226964223B733A323A223133223B733A333A22706964223B733A313A2233223B733A343A226E616D65223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A333A2275726C223B733A32373A2261737369676E6D656E742F756E66696E69736865642F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2231223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A313B613A31313A7B733A323A226964223B733A323A223136223B733A333A22706964223B733A313A2233223B733A343A226E616D65223B733A31323A22E5B7A5E4BD9CE697A5E5BF97223B733A333A2275726C223B733A31393A2264696172792F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2232223B733A363A226D6F64756C65223B733A353A226469617279223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A323B613A31313A7B733A323A226964223B733A323A223137223B733A333A22706964223B733A313A2233223B733A343A226E616D65223B733A393A22E69687E4BBB6E69F9C223B733A333A2275726C223B733A31383A2266696C652F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2233223B733A363A226D6F64756C65223B733A343A2266696C65223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A333B613A31313A7B733A323A226964223B733A323A223230223B733A333A22706964223B733A313A2233223B733A343A226E616D65223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A333A2275726C223B733A32303A227265706F72742F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2233223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A343B613A31313A7B733A323A226964223B733A323A223135223B733A333A22706964223B733A313A2233223B733A343A226E616D65223B733A393A22E9809AE8AEAFE5BD95223B733A333A2275726C223B733A32313A22636F6E746163742F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2235223B733A363A226D6F64756C65223B733A373A22636F6E74616374223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D7D7D693A353B613A31323A7B733A323A226964223B733A313A2235223B733A333A22706964223B693A303B733A343A226E616D65223B733A31323A22E7BBBCE59088E58A9EE585AC223B733A333A2275726C223B733A31393A226A6176617363726970743A766F69642830293B223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2236223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A333A7B693A303B613A31313A7B733A323A226964223B733A323A223132223B733A333A22706964223B733A313A2235223B733A343A226E616D65223B733A31323A22E4BFA1E681AFE585ACE5918A223B733A333A2275726C223B733A32313A2261727469636C652F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2231223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A313B613A31313A7B733A323A226964223B733A323A223138223B733A333A22706964223B733A313A2235223B733A343A226E616D65223B733A31323A22E9809AE79FA5E585ACE5918A223B733A333A2275726C223B733A32393A226F6666696369616C646F632F6F6666696369616C646F632F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2232223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D693A323B613A31313A7B733A323A226964223B733A323A223231223B733A333A22706964223B733A313A2235223B733A343A226E616D65223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A333A2275726C223B733A31383A22766F74652F64656661756C742F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2237223B733A363A226D6F64756C65223B733A343A22766F7465223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D7D7D693A393B613A31323A7B733A323A226964223B733A313A2239223B733A333A22706964223B693A303B733A343A226E616D65223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A333A2275726C223B733A31383A226A6176617363726970743A766F6964283029223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2231223B733A343A22736F7274223B733A313A2239223B733A363A226D6F64756C65223B733A303A22223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B733A353A226368696C64223B613A313A7B693A303B613A31313A7B733A323A226964223B733A323A223139223B733A333A22706964223B733A313A2239223B733A343A226E616D65223B733A31323A22E68B9BE88198E7AEA1E79086223B733A333A2275726C223B733A32303A22726563727569742F726573756D652F696E646578223B733A393A227461726765746E6577223B733A313A2230223B733A363A2273797374656D223B733A313A2231223B733A383A2264697361626C6564223B733A313A2230223B733A343A22736F7274223B733A313A2231223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A343A2274797065223B733A313A2230223B733A363A22706167656964223B733A313A2230223B7D7D7D7D);
INSERT INTO `yckj_syscache` VALUES ('creditrule', '1', '1517282444', 0x613A31393A7B733A31303A22616464636F6D6D656E74223B613A31333A7B733A333A22726964223B733A313A2231223B733A383A2272756C656E616D65223B733A363A22E8AF84E8AEBA223B733A363A22616374696F6E223B733A31303A22616464636F6D6D656E74223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223430223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2233223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A31383A22254538254146253834254538254145254241223B7D733A31303A22676574636F6D6D656E74223B613A31333A7B733A333A22726964223B733A313A2232223B733A383A2272756C656E616D65223B733A393A22E8A2ABE8AF84E8AEBA223B733A363A22616374696F6E223B733A31303A22676574636F6D6D656E74223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223230223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2232223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A32373A22254538254132254142254538254146253834254538254145254241223B7D733A31303A2264656C636F6D6D656E74223B613A31333A7B733A333A22726964223B733A313A2233223B733A383A2272756C656E616D65223B733A31323A22E588A0E999A4E8AF84E8AEBA223B733A363A22616374696F6E223B733A31303A2264656C636F6D6D656E74223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223230223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A323A222D33223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254535253838254130254539253939254134254538254146253834254538254145254241223B7D733A383A226461796C6F67696E223B613A31333A7B733A333A22726964223B733A313A2234223B733A383A2272756C656E616D65223B733A31323A22E6AF8FE5A4A9E799BBE5BD95223B733A363A22616374696F6E223B733A383A226461796C6F67696E223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2231223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254536254146253846254535254134254139254537253939254242254535254244253935223B7D733A31313A22766572696679656D61696C223B613A31333A7B733A333A22726964223B733A313A2235223B733A383A2272756C656E616D65223B733A31323A22E9AA8CE8AF81E982AEE7AEB1223B733A363A22616374696F6E223B733A31313A22766572696679656D61696C223B733A393A226379636C6574797065223B733A313A2231223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2231223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A323A223130223B733A31313A226578746372656469747332223B733A323A223130223B733A31313A226578746372656469747333223B733A313A2232223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254539254141253843254538254146253831254539253832254145254537254145254231223B7D733A31323A227665726966796D6F62696C65223B613A31333A7B733A333A22726964223B733A313A2236223B733A383A2272756C656E616D65223B733A31323A22E9AA8CE8AF81E6898BE69CBA223B733A363A22616374696F6E223B733A31323A227665726966796D6F62696C65223B733A393A226379636C6574797065223B733A313A2231223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2231223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A323A223130223B733A31313A226578746372656469747332223B733A323A223130223B733A31313A226578746372656469747333223B733A313A2232223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254539254141253843254538254146253831254536253839253842254536253943254241223B7D733A383A22616464776569626F223B613A31333A7B733A333A22726964223B733A313A2237223B733A383A2272756C656E616D65223B733A31323A22E58F91E5B883E5BEAEE58D9A223B733A363A22616374696F6E223B733A383A22616464776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223130223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2232223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254535253846253931254535254238253833254535254245254145254535253844253941223B7D733A31313A2264656C657465776569626F223B613A31333A7B733A333A22726964223B733A313A2238223B733A383A2272756C656E616D65223B733A31323A22E588A0E999A4E5BEAEE58D9A223B733A363A22616374696F6E223B733A31313A2264656C657465776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223130223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A323A222D31223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254535253838254130254539253939254134254535254245254145254535253844253941223B7D733A31323A22666F7277617264776569626F223B613A31333A7B733A333A22726964223B733A313A2239223B733A383A2272756C656E616D65223B733A31323A22E8BDACE58F91E5BEAEE58D9A223B733A363A22616374696F6E223B733A31323A22666F7277617264776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223130223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2231223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254538254244254143254535253846253931254535254245254145254535253844253941223B7D733A31343A22666F72776172646564776569626F223B613A31333A7B733A333A22726964223B733A323A223130223B733A383A2272756C656E616D65223B733A31353A22E5BEAEE58D9AE8A2ABE8BDACE58F91223B733A363A22616374696F6E223B733A31343A22666F72776172646564776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A323A223130223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2233223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A34353A22254535254245254145254535253844253941254538254132254142254538254244254143254535253846253931223B7D733A393A2264696767776569626F223B613A31333A7B733A333A22726964223B733A323A223131223B733A383A2272756C656E616D65223B733A393A22E9A1B6E5BEAEE58D9A223B733A363A22616374696F6E223B733A393A2264696767776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2235223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A32373A22254539254131254236254535254245254145254535253844253941223B7D733A31313A22646967676564776569626F223B613A31333A7B733A333A22726964223B733A323A223132223B733A383A2272756C656E616D65223B733A31323A22E5BEAEE58D9AE8A2ABE9A1B6223B733A363A22616374696F6E223B733A31313A22646967676564776569626F223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A333A22323030223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2235223B733A31313A226578746372656469747333223B733A313A2230223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254535254245254145254535253844253941254538254132254142254539254131254236223B7D733A31303A2261646461727469636C65223B613A31333A7B733A333A22726964223B733A323A223133223B733A383A2272756C656E616D65223B733A31383A22E58F91E8A1A8E4BFA1E681AFE585ACE5918A223B733A363A22616374696F6E223B733A31303A2261646461727469636C65223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2232223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A35343A22254535253846253931254538254131254138254534254246254131254536253831254146254535253835254143254535253931253841223B7D733A31363A2266696E69736861737369676E6D656E74223B613A31333A7B733A333A22726964223B733A323A223134223B733A383A2272756C656E616D65223B733A31383A22E5AE8CE68890E4BBBBE58AA1E68C87E6B4BE223B733A363A22616374696F6E223B733A31363A2266696E69736861737369676E6D656E74223B733A393A226379636C6574797065223B733A313A2231223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2230223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A35343A22254535254145253843254536253838253930254534254242254242254535253841254131254536253843253837254536254234254245223B7D733A383A226164646469617279223B613A31333A7B733A333A22726964223B733A323A223135223B733A383A2272756C656E616D65223B733A31383A22E58F91E8A1A8E5B7A5E4BD9CE697A5E5BF97223B733A363A22616374696F6E223B733A383A226164646469617279223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2232223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A35343A22254535253846253931254538254131254138254535254237254135254534254244253943254536253937254135254535254246253937223B7D733A31343A226164646F6666696369616C646F63223B613A31333A7B733A333A22726964223B733A323A223136223B733A383A2272756C656E616D65223B733A31323A22E58F91E8A1A8E9809AE79FA5223B733A363A22616374696F6E223B733A31343A226164646F6666696369616C646F63223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2232223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254535253846253931254538254131254138254539253830253941254537253946254135223B7D733A393A22616464726573756D65223B613A31333A7B733A333A22726964223B733A323A223137223B733A383A2272756C656E616D65223B733A31323A22E6B7BBE58AA0E7AE80E58E86223B733A363A22616374696F6E223B733A393A22616464726573756D65223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2231223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2231223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A33363A22254536254237254242254535253841254130254537254145253830254535253845253836223B7D733A393A226164647265706F7274223B613A31333A7B733A333A22726964223B733A323A223138223B733A383A2272756C656E616D65223B733A31383A22E58F91E8A1A8E5B7A5E4BD9CE6B187E68AA5223B733A363A22616374696F6E223B733A393A226164647265706F7274223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2232223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A35343A22254535253846253931254538254131254138254535254237254135254534254244253943254536254231253837254536253841254135223B7D733A383A22706F73746D61696C223B613A31333A7B733A333A22726964223B733A323A223139223B733A383A2272756C656E616D65223B733A393A22E58699E982AEE4BBB6223B733A363A22616374696F6E223B733A383A22706F73746D61696C223B733A393A226379636C6574797065223B733A313A2233223B733A393A226379636C6574696D65223B733A313A2230223B733A393A227265776172646E756D223B733A313A2234223B733A383A226E6F726570656174223B733A313A2230223B733A31313A226578746372656469747331223B733A313A2230223B733A31313A226578746372656469747332223B733A313A2232223B733A31313A226578746372656469747333223B733A313A2231223B733A31313A226578746372656469747334223B733A313A2230223B733A31313A226578746372656469747335223B733A313A2230223B733A31313A2272756C656E616D65756E69223B733A32373A22254535253836253939254539253832254145254534254242254236223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('ipbanned', '1', '1517282444', 0x613A303A7B7D);
INSERT INTO `yckj_syscache` VALUES ('department', '1', '0', '');
INSERT INTO `yckj_syscache` VALUES ('notifyNode', '1', '1517282444', 0x613A33333A7B733A31313A22766F74655F737572766579223B613A31303A7B733A323A226964223B733A323A223333223B733A343A226E6F6465223B733A31313A22766F74655F737572766579223B733A383A226E6F6465696E666F223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A363A226D6F64756C65223B733A343A22766F7465223B733A383A227469746C656B6579223B733A32373A226D6573736167652F64656661756C742F416C61726D207469746C65223B733A31303A22636F6E74656E746B6579223B733A32393A226D6573736167652F64656661756C742F416C61726D20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2231223B7D733A31393A22766F74655F7570646174655F6D657373616765223B613A31303A7B733A323A226964223B733A323A223332223B733A343A226E6F6465223B733A31393A22766F74655F7570646174655F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E68A95E7A5A8E69BB4E696B0E68F90E98692223B733A363A226D6F64756C65223B733A343A22766F7465223B733A383A227469746C656B6579223B733A33333A22766F74652F64656661756C742F557064617465206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33353A22766F74652F64656661756C742F557064617465206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32303A22766F74655F7075626C6973685F6D657373616765223B613A31303A7B733A323A226964223B733A323A223331223B733A343A226E6F6465223B733A32303A22766F74655F7075626C6973685F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E68A95E7A5A8E58F91E5B883E68F90E98692223B733A363A226D6F64756C65223B733A343A22766F7465223B733A383A227469746C656B6579223B733A33303A22766F74652F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33323A22766F74652F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31383A22757365725F67726F75705F75706772616465223B613A31303A7B733A323A226964223B733A313A2236223B733A343A226E6F6465223B733A31383A22757365725F67726F75705F75706772616465223B733A383A226E6F6465696E666F223B733A31353A22E794A8E688B7E7BB84E58D87E7BAA7223B733A363A226D6F64756C65223B733A343A2275736572223B733A383A227469746C656B6579223B733A33373A22757365722F64656661756C742F557365722067726F75702075706772616465207469746C65223B733A31303A22636F6E74656E746B6579223B733A33393A22757365722F64656661756C742F557365722067726F7570207570677261646520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2230223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2230223B733A343A2274797065223B733A313A2232223B7D733A31343A227265706F72745F6D657373616765223B613A31303A7B733A323A226964223B733A323A223330223B733A343A226E6F6465223B733A31343A227265706F72745F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E5B7A5E4BD9CE6B187E68AA5E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A383A227469746C656B6579223B733A33323A227265706F72742F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33343A227265706F72742F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32313A226F6666696369616C5F6261636B5F6D657373616765223B613A31303A7B733A323A226964223B733A323A223239223B733A343A226E6F6465223B733A32313A226F6666696369616C5F6261636B5F6D657373616765223B733A383A226E6F6465696E666F223B733A33303A22E9809AE79FA5E4B8ADE5BF83E5AEA1E6A0B8E98080E59B9EE68F90E98692223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A383A227469746C656B6579223B733A33343A226F6666696369616C646F632F64656661756C742F4E6577206261636B207469746C65223B733A31303A22636F6E74656E746B6579223B733A33363A226F6666696369616C646F632F64656661756C742F4E6577206261636B20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32333A226F6666696369616C646F635F7369676E5F72656D696E64223B613A31303A7B733A323A226964223B733A323A223238223B733A343A226E6F6465223B733A32333A226F6666696369616C646F635F7369676E5F72656D696E64223B733A383A226E6F6465696E666F223B733A31383A22E9809AE79FA5E7ADBEE694B6E68F90E98692223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A383A227469746C656B6579223B733A33383A226F6666696369616C646F632F64656661756C742F5369676E206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34303A226F6666696369616C646F632F64656661756C742F5369676E206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32363A226F6666696369616C646F635F7665726966795F6D657373616765223B613A31303A7B733A323A226964223B733A323A223237223B733A343A226E6F6465223B733A32363A226F6666696369616C646F635F7665726966795F6D657373616765223B733A383A226E6F6465696E666F223B733A33303A22E4BFA1E681AFE4B8ADE5BF83E9809AE79FA5E5AEA1E6A0B8E68F90E98692223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A383A227469746C656B6579223B733A34343A226F6666696369616C646F632F64656661756C742F4E657720766572696679206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34363A226F6666696369616C646F632F64656661756C742F4E657720766572696679206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31393A226F6666696369616C646F635F6D657373616765223B613A31303A7B733A323A226964223B733A323A223236223B733A343A226E6F6465223B733A31393A226F6666696369616C646F635F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E9809AE79FA5E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A383A227469746C656B6579223B733A33373A226F6666696369616C646F632F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33393A226F6666696369616C646F632F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31323A226D6573736167655F64696767223B613A31303A7B733A323A226964223B733A313A2231223B733A343A226E6F6465223B733A31323A226D6573736167655F64696767223B733A383A226E6F6465696E666F223B733A31323A22E5BEAEE58D9AE79A84E8B59E223B733A363A226D6F64756C65223B733A373A226D657373616765223B733A383A227469746C656B6579223B733A33343A226D6573736167652F64656661756C742F44696767206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33363A226D6573736167652F64656661756C742F44696767206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31383A226D6573736167655F656D7074795F64696767223B613A31303A7B733A323A226964223B733A313A2232223B733A343A226E6F6465223B733A31383A226D6573736167655F656D7074795F64696767223B733A383A226E6F6465696E666F223B733A32313A22E5BEAEE58D9AE79A84E697A0E69687E5AD97E8B59E223B733A363A226D6F64756C65223B733A373A226D657373616765223B733A383A227469746C656B6579223B733A34303A226D6573736167652F64656661756C742F4469676720656D707479206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34323A226D6573736167652F64656661756C742F4469676720656D707479206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31313A22757365725F666F6C6C6F77223B613A31303A7B733A323A226964223B733A313A2233223B733A343A226E6F6465223B733A31313A22757365725F666F6C6C6F77223B733A383A226E6F6465696E666F223B733A31353A22E696B0E7B289E4B89DE68F90E98692223B733A363A226D6F64756C65223B733A373A226D657373616765223B733A383A227469746C656B6579223B733A33363A226D6573736167652F64656661756C742F466F6C6C6F77206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33383A226D6573736167652F64656661756C742F466F6C6C6F77206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A373A22636F6D6D656E74223B613A31303A7B733A323A226964223B733A313A2234223B733A343A226E6F6465223B733A373A22636F6D6D656E74223B733A383A226E6F6465696E666F223B733A31323A22E8AF84E8AEBAE68891E79A84223B733A363A226D6F64756C65223B733A373A226D657373616765223B733A383A227469746C656B6579223B733A33363A226D6573736167652F64656661756C742F4E6F7469667920636F6D6D656E74207469746C65223B733A31303A22636F6E74656E746B6579223B733A33383A226D6573736167652F64656661756C742F4E6F7469667920636F6D6D656E7420636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2230223B733A31313A2273656E646D657373616765223B733A313A2230223B733A373A2273656E64736D73223B733A313A2230223B733A343A2274797065223B733A313A2231223B7D733A31393A226E6F726D616C5F616C61726D5F6E6F74696C79223B613A31303A7B733A323A226964223B733A313A2235223B733A343A226E6F6465223B733A31393A226E6F726D616C5F616C61726D5F6E6F74696C79223B733A383A226E6F6465696E666F223B733A31323A22E699AEE9809AE68F90E98692223B733A363A226D6F64756C65223B733A373A226D657373616765223B733A383A227469746C656B6579223B733A32373A226D6573736167652F64656661756C742F416C61726D207469746C65223B733A31303A22636F6E74656E746B6579223B733A32393A226D6573736167652F64656661756C742F416C61726D20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2231223B7D733A32313A22656D61696C5F726563656976655F6D657373616765223B613A31303A7B733A323A226964223B733A323A223235223B733A343A226E6F6465223B733A32313A22656D61696C5F726563656976655F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E982AEE4BBB6E59B9EE689A7E68F90E98692223B733A363A226D6F64756C65223B733A353A22656D61696C223B733A383A227469746C656B6579223B733A32393A22656D61696C2F64656661756C742F416C72656164792072656365697665223B733A31303A22636F6E74656E746B6579223B733A303A22223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31333A22656D61696C5F6D657373616765223B613A31303A7B733A323A226964223B733A323A223234223B733A343A226E6F6465223B733A31333A22656D61696C5F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E982AEE4BBB6E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A353A22656D61696C223B733A383A227469746C656B6579223B733A33313A22656D61696C2F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33333A22656D61696C2F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31333A2264696172795F6D657373616765223B613A31303A7B733A323A226964223B733A323A223233223B733A343A226E6F6465223B733A31333A2264696172795F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E5B7A5E4BD9CE697A5E5BF97E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A353A226469617279223B733A383A227469746C656B6579223B733A33313A2264696172792F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33333A2264696172792F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32303A226164645F63616C656E6461725F6D657373616765223B613A31303A7B733A323A226964223B733A323A223230223B733A343A226E6F6465223B733A32303A226164645F63616C656E6461725F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E697A5E7A88BE6B7BBE58AA0E68F90E98692223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A383A227469746C656B6579223B733A34333A2263616C656E6461722F64656661756C742F416464207363686564756C65206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34353A2263616C656E6461722F64656661756C742F416464207363686564756C65206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31323A227461736B5F6D657373616765223B613A31303A7B733A323A226964223B733A323A223232223B733A343A226E6F6465223B733A31323A227461736B5F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E4BBBBE58AA1E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A383A227469746C656B6579223B733A33393A2263616C656E6461722F64656661756C742F4E6577207461736B206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34313A2263616C656E6461722F64656661756C742F4E6577207461736B206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31363A2263616C656E6461725F6D657373616765223B613A31303A7B733A323A226964223B733A323A223231223B733A343A226E6F6465223B733A31363A2263616C656E6461725F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E697A5E7A88BE6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A383A227469746C656B6579223B733A34333A2263616C656E6461722F64656661756C742F4E6577207363686564756C65206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34353A2263616C656E6461722F64656661756C742F4E6577207363686564756C65206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32323A2261737369676E6D656E745F6E65775F6D657373616765223B613A31303A7B733A323A226964223B733A323A223131223B733A343A226E6F6465223B733A32323A2261737369676E6D656E745F6E65775F6D657373616765223B733A383A226E6F6465696E666F223B733A32373A22E4BBBBE58AA1E68C87E6B4BEE696B0E4BBBBE58AA1E68F90E98692223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A33353A2261737369676E6D656E742F64656661756C742F4E65772061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A33373A2261737369676E6D656E742F64656661756C742F4E65772061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31353A2261737369676E6D656E745F7461736B223B613A31303A7B733A323A226964223B733A323A223139223B733A343A226E6F6465223B733A31353A2261737369676E6D656E745F7461736B223B733A383A226E6F6465696E666F223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A32373A226D6573736167652F64656661756C742F416C61726D207469746C65223B733A31303A22636F6E74656E746B6579223B733A32393A226D6573736167652F64656661756C742F416C61726D20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2231223B7D733A32333A2261737369676E6D656E745F707573685F6D657373616765223B613A31303A7B733A323A226964223B733A323A223132223B733A343A226E6F6465223B733A32333A2261737369676E6D656E745F707573685F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E4BBBBE58AA1E582ACE58A9EE68F90E98692223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A33363A2261737369676E6D656E742F64656661756C742F507573682061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A33383A2261737369676E6D656E742F64656661756C742F507573682061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32353A2261737369676E6D656E745F66696E6973685F6D657373616765223B613A31303A7B733A323A226964223B733A323A223133223B733A343A226E6F6465223B733A32353A2261737369676E6D656E745F66696E6973685F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E4BBBBE58AA1E5AE8CE68890E6B688E681AF223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A33383A2261737369676E6D656E742F64656661756C742F46696E6973682061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A34303A2261737369676E6D656E742F64656661756C742F46696E6973682061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32393A2261737369676E6D656E745F6170706C7964656C61795F6D657373616765223B613A31303A7B733A323A226964223B733A323A223134223B733A343A226E6F6465223B733A32393A2261737369676E6D656E745F6170706C7964656C61795F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E4BBBBE58AA1E5BBB6E69C9FE794B3E8AFB7E6B688E681AF223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A34323A2261737369676E6D656E742F64656661756C742F4170706C7964656C61792061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A34343A2261737369676E6D656E742F64656661756C742F4170706C7964656C61792061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A33353A2261737369676E6D656E745F6170706C7964656C6179726573756C745F6D657373616765223B613A31303A7B733A323A226964223B733A323A223135223B733A343A226E6F6465223B733A33353A2261737369676E6D656E745F6170706C7964656C6179726573756C745F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E4BBBBE58AA1E5BBB6E69C9FE794B3E8AFB7E7BB93E69E9C223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A34313A2261737369676E6D656E742F64656661756C742F4170706C7964656C6179726573756C74207469746C65223B733A31303A22636F6E74656E746B6579223B733A34333A2261737369676E6D656E742F64656661756C742F4170706C7964656C6179726573756C7420636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A33363A2261737369676E6D656E745F6170706C7963616E63656C726573756C745F6D657373616765223B613A31303A7B733A323A226964223B733A323A223137223B733A343A226E6F6465223B733A33363A2261737369676E6D656E745F6170706C7963616E63656C726573756C745F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E4BBBBE58AA1E58F96E6B688E794B3E8AFB7E7BB93E69E9C223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A34323A2261737369676E6D656E742F64656661756C742F4170706C7963616E63656C726573756C74207469746C65223B733A31303A22636F6E74656E746B6579223B733A34343A2261737369676E6D656E742F64656661756C742F4170706C7963616E63656C726573756C7420636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A33303A2261737369676E6D656E745F6170706C7963616E63656C5F6D657373616765223B613A31303A7B733A323A226964223B733A323A223136223B733A343A226E6F6465223B733A33303A2261737369676E6D656E745F6170706C7963616E63656C5F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E4BBBBE58AA1E58F96E6B688E794B3E8AFB7E6B688E681AF223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A34333A2261737369676E6D656E742F64656661756C742F4170706C7963616E63656C2061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A34353A2261737369676E6D656E742F64656661756C742F4170706C7963616E63656C2061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32353A2261737369676E6D656E745F74696D696E675F6D657373616765223B613A31303A7B733A323A226964223B733A323A223130223B733A343A226E6F6465223B733A32353A2261737369676E6D656E745F74696D696E675F6D657373616765223B733A383A226E6F6465696E666F223B733A31323A22E4BBBBE58AA1E68F90E98692223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A33383A2261737369676E6D656E742F64656661756C742F54696D696E672061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A34303A2261737369676E6D656E742F64656661756C742F54696D696E672061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32383A2261737369676E6D656E745F61707072616973616C5F6D657373616765223B613A31303A7B733A323A226964223B733A323A223138223B733A343A226E6F6465223B733A32383A2261737369676E6D656E745F61707072616973616C5F6D657373616765223B733A383A226E6F6465696E666F223B733A31383A22E4BBBBE58AA1E8AF84E4BBB7E6B688E681AF223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A383A227469746C656B6579223B733A34313A2261737369676E6D656E742F64656661756C742F41707072616973616C2061737369676E207469746C65223B733A31303A22636F6E74656E746B6579223B733A34333A2261737369676E6D656E742F64656661756C742F41707072616973616C2061737369676E20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32323A2261727469636C655F7665726966795F6D657373616765223B613A31303A7B733A323A226964223B733A313A2238223B733A343A226E6F6465223B733A32323A2261727469636C655F7665726966795F6D657373616765223B733A383A226E6F6465696E666F223B733A33303A22E4BFA1E681AFE4B8ADE5BF83E696B0E997BBE5AEA1E6A0B8E68F90E98692223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A383A227469746C656B6579223B733A34303A2261727469636C652F64656661756C742F4E657720766572696679206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A34323A2261727469636C652F64656661756C742F4E657720766572696679206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A32303A2261727469636C655F6261636B5F6D657373616765223B613A31303A7B733A323A226964223B733A313A2239223B733A343A226E6F6465223B733A32303A2261727469636C655F6261636B5F6D657373616765223B733A383A226E6F6465696E666F223B733A33303A22E4BFA1E681AFE4B8ADE5BF83E5AEA1E6A0B8E98080E59B9EE68F90E98692223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A383A227469746C656B6579223B733A33303A2261727469636C652F64656661756C742F4E6577206261636B207469746C65223B733A31303A22636F6E74656E746B6579223B733A33323A2261727469636C652F64656661756C742F4E6577206261636B20636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D733A31353A2261727469636C655F6D657373616765223B613A31303A7B733A323A226964223B733A313A2237223B733A343A226E6F6465223B733A31353A2261727469636C655F6D657373616765223B733A383A226E6F6465696E666F223B733A32343A22E4BFA1E681AFE4B8ADE5BF83E6B688E681AFE68F90E98692223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A383A227469746C656B6579223B733A33333A2261727469636C652F64656661756C742F4E6577206D657373616765207469746C65223B733A31303A22636F6E74656E746B6579223B733A33353A2261727469636C652F64656661756C742F4E6577206D65737361676520636F6E74656E74223B733A393A2273656E64656D61696C223B733A313A2231223B733A31313A2273656E646D657373616765223B733A313A2231223B733A373A2273656E64736D73223B733A313A2231223B733A343A2274797065223B733A313A2232223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('role', '1', '1517282444', 0x613A333A7B693A313B613A333A7B733A363A22726F6C656964223B733A313A2231223B733A383A22726F6C656E616D65223B733A393A22E7AEA1E79086E59198223B733A383A22726F6C6574797065223B733A313A2230223B7D693A323B613A333A7B733A363A22726F6C656964223B733A313A2232223B733A383A22726F6C656E616D65223B733A31323A22E7BC96E8BE91E4BABAE59198223B733A383A22726F6C6574797065223B733A313A2230223B7D693A333B613A333A7B733A363A22726F6C656964223B733A313A2233223B733A383A22726F6C656E616D65223B733A31323A22E699AEE9809AE68890E59198223B733A383A22726F6C6574797065223B733A313A2230223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('position', '1', '1517282444', 0x613A333A7B693A313B613A373A7B733A31303A22706F736974696F6E6964223B733A313A2231223B733A353A226361746964223B733A313A2231223B733A373A22706F736E616D65223B733A393A22E680BBE7BB8FE79086223B733A343A22736F7274223B733A313A2231223B733A343A22676F616C223B733A303A22223B733A31343A226D696E726571756972656D656E74223B733A303A22223B733A363A226E756D626572223B733A313A2230223B7D693A323B613A373A7B733A31303A22706F736974696F6E6964223B733A313A2232223B733A353A226361746964223B733A313A2231223B733A373A22706F736E616D65223B733A31323A22E983A8E997A8E7BB8FE79086223B733A343A22736F7274223B733A313A2232223B733A343A22676F616C223B733A303A22223B733A31343A226D696E726571756972656D656E74223B733A303A22223B733A363A226E756D626572223B733A313A2231223B7D693A333B613A373A7B733A31303A22706F736974696F6E6964223B733A313A2233223B733A353A226361746964223B733A313A2231223B733A373A22706F736E616D65223B733A363A22E8818CE59198223B733A343A22736F7274223B733A313A2233223B733A343A22676F616C223B733A303A22223B733A31343A226D696E726571756972656D656E74223B733A303A22223B733A363A226E756D626572223B733A313A2232223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('positioncategory', '1', '1517282444', 0x613A313A7B693A313B613A343A7B733A353A226361746964223B733A313A2231223B733A333A22706964223B733A313A2230223B733A343A226E616D65223B733A31323A22E9BB98E8AEA4E58886E7B1BB223B733A343A22736F7274223B733A313A2231223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('authitem', '1', '1517282444', 0x613A31313A7B733A31363A2235703244365A6D5135596958364B476F223B613A323A7B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A353A2267726F7570223B613A373A7B733A383A223537755235613661223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E7BB91E5AE9A223B733A343A226E6F6465223B613A333A7B693A303B613A393A7B733A323A226964223B733A313A2231223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31303A22636F62696E64696E6773223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31353A22E985B7E58A9EE585ACE7BB91E5AE9A223B733A353A2267726F7570223B733A363A22E7BB91E5AE9A223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A34383A2264617368626F6172642F636F62696E64696E672F696E6465782C64617368626F6172642F636F73796E632F696E646578223B7D693A313B613A393A7B733A323A226964223B733A313A2232223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31303A22777862696E64696E6773223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A32313A22E5BEAEE4BFA1E4BC81E4B89AE58FB7E7BB91E5AE9A223B733A353A2267726F7570223B733A363A22E7BB91E5AE9A223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32353A2264617368626F6172642F777862696E64696E672F696E646578223B7D693A323B613A393A7B733A323A226964223B733A313A2233223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A333A22696D73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E58DB3E697B6E9809AE8AEAFE7BB91E5AE9A223B733A353A2267726F7570223B733A363A22E7BB91E5AE9A223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A31383A2264617368626F6172642F696D2F696E646578223B7D7D7D733A383A223559576F35624741223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E585A8E5B180223B733A343A226E6F6465223B613A31333A7B693A303B613A393A7B733A323A226964223B733A313A2234223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A22676C6F62616C73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E58D95E4BD8DE7AEA1E79086223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32303A2264617368626F6172642F756E69742F696E646578223B7D693A313B613A393A7B733A323A226964223B733A313A2235223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A2263726564697473223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E7A7AFE58886E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32323A2264617368626F6172642F6372656469742F7365747570223B7D693A323B613A393A7B733A323A226964223B733A313A2236223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31303A227573657267726F757073223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A393A22E794A8E688B7E7BB84223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32353A2264617368626F6172642F7573657267726F75702F696E646578223B7D693A333B613A393A7B733A323A226964223B733A313A2237223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A226F7074696D697A6573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E680A7E883BDE4BC98E58C96223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F6F7074696D697A652F6361636865223B7D693A343B613A393A7B733A323A226964223B733A313A2238223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A353A226461746573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E697B6E997B4E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32303A2264617368626F6172642F646174652F696E646578223B7D693A353B613A393A7B733A323A226964223B733A313A2239223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A2275706C6F616473223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4B88AE4BCA0E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32323A2264617368626F6172642F75706C6F61642F696E646578223B7D693A363B613A393A7B733A323A226964223B733A323A223130223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A343A22736D7373223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E6898BE69CBAE79FADE4BFA1E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32313A2264617368626F6172642F736D732F6D616E61676572223B7D693A373B613A393A7B733A323A226964223B733A323A223131223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A383A22737973636F646573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E7B3BBE7BB9FE4BBA3E7A081E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32333A2264617368626F6172642F737973636F64652F696E646578223B7D693A383B613A393A7B733A323A226964223B733A323A223132223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A363A22656D61696C73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E982AEE4BBB6E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32313A2264617368626F6172642F656D61696C2F7365747570223B7D693A393B613A393A7B733A323A226964223B733A323A223133223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A22736563757269747973223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E5AE89E585A8E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F73656375726974792F7365747570223B7D693A31303B613A393A7B733A323A226964223B733A323A223134223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A227379737374616D7073223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E7B3BBE7BB9FE59BBEE7ABA0223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F7379737374616D702F696E646578223B7D693A31313B613A393A7B733A323A226964223B733A323A223135223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A22617070726F76616C73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E5AEA1E689B9E6B581E7A88B223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F617070726F76616C2F696E646578223B7D693A31323B613A393A7B733A323A226964223B733A323A223136223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A226E6F7469667973223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E68F90E98692E7AD96E795A5E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E585A8E5B180223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32323A2264617368626F6172642F6E6F746966792F7365747570223B7D7D7D733A383A223535536F356F6933223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E794A8E688B7223B733A343A226E6F6465223B613A343A7B693A303B613A393A7B733A323A226964223B733A323A223137223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A353A227573657273223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E983A8E997A8E794A8E688B7E7AEA1E79086223B733A353A2267726F7570223B733A363A22E794A8E688B7223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32303A2264617368626F6172642F757365722F696E646578223B7D693A313B613A393A7B733A323A226964223B733A323A223138223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A353A22726F6C6573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E8A792E889B2E69D83E99990E7AEA1E79086223B733A353A2267726F7570223B733A363A22E794A8E688B7223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32303A2264617368626F6172642F726F6C652F696E646578223B7D693A323B613A393A7B733A323A226964223B733A323A223139223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A22706F736974696F6E73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E5B297E4BD8DE7AEA1E79086223B733A353A2267726F7570223B733A363A22E794A8E688B7223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F706F736974696F6E2F696E646578223B7D693A333B613A393A7B733A323A226964223B733A323A223230223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31303A22726F6C6561646D696E73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31353A22E7AEA1E79086E59198E7AEA1E79086223B733A353A2267726F7570223B733A363A22E794A8E688B7223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32353A2264617368626F6172642F726F6C6561646D696E2F696E646578223B7D7D7D733A383A223535574D365A3269223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E7958CE99DA2223B733A343A226E6F6465223B613A343A7B693A303B613A393A7B733A323A226964223B733A323A223231223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A343A226E617673223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E9A1B6E983A8E5AFBCE888AAE8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E7958CE99DA2223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A31393A2264617368626F6172642F6E61762F696E646578223B7D693A313B613A393A7B733A323A226964223B733A323A223232223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A22717569636B6E617673223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E5BFABE68DB7E5AFBCE888AAE8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E7958CE99DA2223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F717569636B6E61762F696E646578223B7D693A323B613A393A7B733A323A226964223B733A323A223233223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A363A226C6F67696E73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A32313A22E799BBE5BD95E9A1B5E8838CE699AFE8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E7958CE99DA2223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32313A2264617368626F6172642F6C6F67696E2F696E646578223B7D693A333B613A393A7B733A323A226964223B733A323A223234223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31313A226261636B67726F756E6473223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E7B3BBE7BB9FE8838CE699AFE8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E7958CE99DA2223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32353A2264617368626F6172642F6261636B67726F75642F696E646578223B7D7D7D733A383A2235716968355A3258223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E6A8A1E59D97223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223235223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A226D6F64756C6573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E6A8A1E59D97E7AEA1E79086223B733A353A2267726F7570223B733A363A22E6A8A1E59D97223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32343A2264617368626F6172642F6D6F64756C652F6D616E61676572223B7D693A313B613A393A7B733A323A226964223B733A323A223236223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31323A227065726D697373696F6E7373223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E69D83E99990E8AEBEE7BDAE223B733A353A2267726F7570223B733A363A22E6A8A1E59D97223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32373A2264617368626F6172642F7065726D697373696F6E732F7365747570223B7D7D7D733A383A223536366835354347223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E7AEA1E79086223B733A343A226E6F6465223B613A353A7B693A303B613A393A7B733A323A226964223B733A323A223237223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A373A2275706461746573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E69BB4E696B0E7BC93E5AD98223B733A353A2267726F7570223B733A363A22E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32323A2264617368626F6172642F7570646174652F696E646578223B7D693A313B613A393A7B733A323A226964223B733A323A223238223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A31333A22616E6E6F756E63656D656E7473223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E7B3BBE7BB9FE585ACE5918A223B733A353A2267726F7570223B733A363A22E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32383A2264617368626F6172642F616E6E6F756E63656D656E742F7365747570223B7D693A323B613A393A7B733A323A226964223B733A323A223239223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A393A22646174616261736573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A393A22E695B0E68DAEE5BA93223B733A353A2267726F7570223B733A363A22E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32353A2264617368626F6172642F64617461626173652F6261636B7570223B7D693A333B613A393A7B733A323A226964223B733A323A223330223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A353A2263726F6E73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8AEA1E58892E4BBBBE58AA1223B733A353A2267726F7570223B733A363A22E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32303A2264617368626F6172642F63726F6E2F696E646578223B7D693A343B613A393A7B733A323A226964223B733A323A223331223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A383A227570677261646573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E59CA8E7BABFE58D87E7BAA7223B733A353A2267726F7570223B733A363A22E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32333A2264617368626F6172642F757067726164652F696E646578223B7D7D7D733A383A223570794E35597168223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E69C8DE58AA1223B733A343A226E6F6465223B613A313A7B693A303B613A393A7B733A323A226964223B733A323A223332223B733A363A226D6F64756C65223B733A393A2264617368626F617264223B733A333A226B6579223B733A383A227365727669636573223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A393A22E4BA91E69C8DE58AA1223B733A353A2267726F7570223B733A363A22E69C8DE58AA1223B733A383A2263617465676F7279223B733A31323A22E69D83E99990E58897E8A1A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32333A2264617368626F6172642F736572766963652F696E646578223B7D7D7D7D7D733A31363A22354C2B68356F4776354C697435622B44223B613A323A7B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A353A2267726F7570223B613A313A7B733A383A2235706177365A6537223B613A323A7B733A393A2267726F75704E616D65223B733A363A22E696B0E997BB223B733A343A226E6F6465223B613A353A7B693A303B613A393A7B733A323A226964223B733A323A223333223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A343A2276696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4BFA1E681AFE69FA5E79C8B223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3337393A2261727469636C652F636F6D6D656E742F676574636F6D6D656E746C6973742C61727469636C652F636F6D6D656E742F616464636F6D6D656E742C61727469636C652F636F6D6D656E742F64656C636F6D6D656E742C61727469636C652F636F6D6D656E742F676574636F6D6D656E74766965772C61727469636C652F63617465676F72792F696E6465782C61727469636C652F64656661756C742F696E6465782C61727469636C652F64656661756C742F73686F772C61727469636C652F64656661756C742F707265766965772C61727469636C652F64656661756C742F766F74652C61727469636C652F64656661756C742F6765747265616465722C61727469636C652F64656661756C742F676574636F756E742C61727469636C652F64656661756C742F726561642C61727469636C652F646174612F696E6465782C61727469636C652F646174612F73686F772C61727469636C652F646174612F707265766965772C61727469636C652F7665726966792F666C6F776C6F67223B7D693A313B613A393A7B733A323A226964223B733A323A223334223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A363A22766572696679223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4BFA1E681AFE5AEA1E6A0B8223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3130353A2261727469636C652F7665726966792F696E6465782C61727469636C652F7665726966792F7665726966792C61727469636C652F7665726966792F6261636B2C61727469636C652F7665726966792F696E6465782C61727469636C652F7665726966792F63616E63656C223B7D693A323B613A393A7B733A323A226964223B733A323A223335223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A373A227075626C697368223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4BFA1E681AFE58F91E5B883223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3139393A2261727469636C652F7075626C6973682F696E6465782C61727469636C652F7075626C6973682F63616C6C2C61727469636C652F7075626C6973682F63616E63656C2C61727469636C652F64656661756C742F6164642C61727469636C652F64656661756C742F766F74652C61727469636C652F64656661756C742F7375626D69742C61727469636C652F646174612F656469742C61727469636C652F646174612F6F7074696F6E2C61727469636C652F63617465676F72792F676574637572617070726F76616C223B7D693A333B613A393A7B733A323A226964223B733A323A223336223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A383A2263617465676F7279223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E58886E7B1BBE7AEA1E79086223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3130383A2261727469636C652F63617465676F72792F6164642C61727469636C652F63617465676F72792F656469742C61727469636C652F63617465676F72792F64656C2C61727469636C652F63617465676F72792F6D6F76652C61727469636C652F63617465676F72792F696E646578223B7D693A343B613A393A7B733A323A226964223B733A323A223337223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223338223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B733A343A2265646974223B733A343A226E616D65223B733A363A22E7BC96E8BE91223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A3231353A2261727469636C652F64656661756C742F656469742C61727469636C652F64656661756C742F746F702C61727469636C652F64656661756C742F6D6F76652C61727469636C652F64656661756C742F68696768746C696768742C61727469636C652F64656661756C742F736176652C61727469636C652F64656661756C742F6765746D6F76652C61727469636C652F64656661756C742F766F74652C61727469636C652F64656661756C742F7375626D69742C61727469636C652F646174612F656469742C61727469636C652F646174612F6F7074696F6E223B7D693A313B613A393A7B733A323A226964223B733A323A223339223B733A363A226D6F64756C65223B733A373A2261727469636C65223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B733A333A2264656C223B733A343A226E616D65223B733A363A22E588A0E999A4223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A32323A2261727469636C652F64656661756C742F64656C657465223B7D7D733A343A226E616D65223B733A31323A22E58685E5AEB9E7AEA1E79086223B733A353A2267726F7570223B733A363A22E696B0E997BB223B733A383A2263617465676F7279223B733A31323A22E4BFA1E681AFE4B8ADE5BF83223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A3130383A2261727469636C652F63617465676F72792F6164642C61727469636C652F63617465676F72792F656469742C61727469636C652F63617465676F72792F64656C2C61727469636C652F63617465676F72792F6D6F76652C61727469636C652F63617465676F72792F696E646578223B7D7D7D7D7D733A31363A22354C753735597168356F79483572532B223B613A323A7B733A383A2263617465676F7279223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A353A2267726F7570223B613A313A7B733A31363A22354C753735597168356F79483572532B223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223430223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A333A226B6579223B733A31303A2261737369676E6D656E74223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4BBBBE58AA1E7AEA1E79086223B733A353A2267726F7570223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A383A2263617465676F7279223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3237363A2261737369676E6D656E742F64656661756C742F6164642C61737369676E6D656E742F64656661756C742F656469742C61737369676E6D656E742F64656661756C742F64656C2C61737369676E6D656E742F64656661756C742F73686F772C61737369676E6D656E742F756E66696E69736865642F696E6465782C61737369676E6D656E742F756E66696E69736865642F616A6178656E7472616E63652C61737369676E6D656E742F66696E69736865642F696E6465782C61737369676E6D656E742F636F6D6D656E742F676574636F6D6D656E746C6973742C61737369676E6D656E742F636F6D6D656E742F616464636F6D6D656E742C61737369676E6D656E742F636F6D6D656E742F64656C636F6D6D656E74223B7D693A313B613A393A7B733A323A226964223B733A323A223431223B733A363A226D6F64756C65223B733A31303A2261737369676E6D656E74223B733A333A226B6579223B733A363A22726576696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E69FA5E79C8BE4B88BE5B19EE4BBBBE58AA1223B733A353A2267726F7570223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A383A2263617465676F7279223B733A31323A22E4BBBBE58AA1E68C87E6B4BE223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32393A2261737369676E6D656E742F756E66696E69736865642F7375626C697374223B7D7D7D7D7D733A383A223570656C3536694C223B613A323A7B733A383A2263617465676F7279223B733A363A22E697A5E7A88B223B733A353A2267726F7570223B613A313A7B733A31363A223570656C3536694C3561364A356F3653223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E697A5E7A88BE5AE89E68E92223B733A343A226E6F6465223B613A333A7B693A303B613A393A7B733A323A226964223B733A323A223432223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A333A226B6579223B733A383A227363686564756C65223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A363A22E697A5E7A88B223B733A353A2267726F7570223B733A31323A22E697A5E7A88BE5AE89E68E92223B733A383A2263617465676F7279223B733A363A22E697A5E7A88B223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3135323A2263616C656E6461722F7363686564756C652F696E6465782C63616C656E6461722F7363686564756C652F7375627363686564756C652C63616C656E6461722F7363686564756C652F73686172657363686564756C652C63616C656E6461722F7363686564756C652F6164642C63616C656E6461722F7363686564756C652F656469742C63616C656E6461722F7363686564756C652F64656C223B7D693A313B613A393A7B733A323A226964223B733A323A223433223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A333A226B6579223B733A343A227461736B223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A363A22E5BE85E58A9E223B733A353A2267726F7570223B733A31323A22E697A5E7A88BE5AE89E68E92223B733A383A2263617465676F7279223B733A363A22E697A5E7A88B223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A39363A2263616C656E6461722F7461736B2F696E6465782C63616C656E6461722F7461736B2F7375627461736B2C63616C656E6461722F7461736B2F6164642C63616C656E6461722F7461736B2F656469742C63616C656E6461722F7461736B2F64656C223B7D693A323B613A393A7B733A323A226964223B733A323A223434223B733A363A226D6F64756C65223B733A383A2263616C656E646172223B733A333A226B6579223B733A343A226C6F6F70223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31353A22E591A8E69C9FE680A7E4BA8BE58AA1223B733A353A2267726F7570223B733A31323A22E697A5E7A88BE5AE89E68E92223B733A383A2263617465676F7279223B733A363A22E697A5E7A88B223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A37343A2263616C656E6461722F6C6F6F702F696E6465782C63616C656E6461722F6C6F6F702F6164642C63616C656E6461722F6C6F6F702F656469742C63616C656E6461722F6C6F6F702F64656C223B7D7D7D7D7D733A31363A22354C713635597162364C574535727151223B613A323A7B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A353A2267726F7570223B613A323A7B733A31323A2236594361364B367635623256223B613A323A7B733A393A2267726F75704E616D65223B733A393A22E9809AE8AEAFE5BD95223B733A343A226E6F6465223B613A313A7B693A303B613A393A7B733A323A226964223B733A323A223435223B733A363A226D6F64756C65223B733A373A22636F6E74616374223B733A333A226B6579223B733A373A22636F6E74616374223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A393A22E9809AE8AEAFE5BD95223B733A353A2267726F7570223B733A393A22E9809AE8AEAFE5BD95223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3238333A22636F6E746163742F64656661756C742F696E6465782C636F6E746163742F64656661756C742F616A61786170692C636F6E746163742F64656661756C742F6578706F72742C636F6E746163742F64656661756C742F7072696E74636F6E746163742C636F6E746163742F636F6E7374616E742F696E6465782C636F6E746163742F6170692F646570746C6973742C636F6E746163742F6170692F757365726C6973742C636F6E746163742F6170692F67726F7570757365726C6973742C636F6E746163742F6170692F7365617263682C636F6E746163742F6170692F636F72702C636F6E746163742F6170692F646570742C636F6E746163742F6170692F757365722C636F6E746163742F6170692F68696464656E756964617272223B7D7D7D733A31363A22356F7562364947593536366835354347223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E68B9BE88198E7AEA1E79086223B733A343A226E6F6465223B613A353A7B693A303B613A393A7B733A323A226964223B733A323A223539223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A333A226B6579223B733A363A22726573756D65223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4BABAE6898DE7AEA1E79086223B733A353A2267726F7570223B733A31323A22E68B9BE88198E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3132333A22726563727569742F726573756D652F696E6465782C726563727569742F726573756D652F6164642C726563727569742F726573756D652F73686F772C726563727569742F726573756D652F656469742C726563727569742F726573756D652F73656E64656D61696C2C726563727569742F726573756D652F64656C223B7D693A313B613A393A7B733A323A226964223B733A323A223630223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A333A226B6579223B733A373A22636F6E74616374223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E88194E7B3BBE8AEB0E5BD95223B733A353A2267726F7570223B733A31323A22E68B9BE88198E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3130353A22726563727569742F636F6E746163742F696E6465782C726563727569742F636F6E746163742F6164642C726563727569742F636F6E746163742F656469742C726563727569742F636F6E746163742F64656C2C726563727569742F636F6E746163742F6578706F7274223B7D693A323B613A393A7B733A323A226964223B733A323A223631223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A333A226B6579223B733A393A22696E74657276696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E99DA2E8AF95E8AEB0E5BD95223B733A353A2267726F7570223B733A31323A22E68B9BE88198E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3131353A22726563727569742F696E746572766965772F696E6465782C726563727569742F696E746572766965772F6164642C726563727569742F696E746572766965772F656469742C726563727569742F696E746572766965772F64656C2C726563727569742F696E746572766965772F6578706F7274223B7D693A333B613A393A7B733A323A226964223B733A323A223632223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A333A226B6579223B733A383A226267636865636B73223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8838CE699AFE8B083E69FA5223B733A353A2267726F7570223B733A31323A22E68B9BE88198E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3131303A22726563727569742F6267636865636B732F696E6465782C726563727569742F6267636865636B732F6164642C726563727569742F6267636865636B732F656469742C726563727569742F6267636865636B732F64656C2C726563727569742F6267636865636B732F6578706F7274223B7D693A343B613A393A7B733A323A226964223B733A323A223633223B733A363A226D6F64756C65223B733A373A2272656372756974223B733A333A226B6579223B733A31303A2273746174697374696373223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E68B9BE88198E7BB9FE8AEA1223B733A353A2267726F7570223B733A31323A22E68B9BE88198E7AEA1E79086223B733A383A2263617465676F7279223B733A31323A22E4BABAE58A9BE8B584E6BA90223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A31393A22726563727569742F73746174732F696E646578223B7D7D7D7D7D733A383A223570656C35622B58223B613A323A7B733A383A2263617465676F7279223B733A363A22E697A5E5BF97223B733A353A2267726F7570223B613A313A7B733A31363A223562656C354C32633570656C35622B58223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E5B7A5E4BD9CE697A5E5BF97223B733A343A226E6F6465223B613A333A7B693A303B613A393A7B733A323A226964223B733A323A223436223B733A363A226D6F64756C65223B733A353A226469617279223B733A333A226B6579223B733A353A226469617279223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E697A5E5BF97E7AEA1E79086223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE697A5E5BF97223B733A383A2263617465676F7279223B733A363A22E697A5E5BF97223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3237313A2264696172792F64656661756C742F696E6465782C64696172792F64656661756C742F6164642C64696172792F64656661756C742F656469742C64696172792F64656661756C742F64656C2C64696172792F64656661756C742F73686F772C64696172792F73686172652F696E6465782C64696172792F73686172652F73686F772C64696172792F617474656E74696F6E2F696E6465782C64696172792F617474656E74696F6E2F656469742C64696172792F617474656E74696F6E2F73686F772C64696172792F636F6D6D656E742F676574636F6D6D656E746C6973742C64696172792F636F6D6D656E742F616464636F6D6D656E742C64696172792F636F6D6D656E742F64656C636F6D6D656E74223B7D693A313B613A393A7B733A323A226964223B733A323A223437223B733A363A226D6F64756C65223B733A353A226469617279223B733A333A226B6579223B733A363A22726576696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8AF84E99885E4B88BE5B19E223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE697A5E5BF97223B733A383A2263617465676F7279223B733A363A22E697A5E5BF97223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3131303A2264696172792F7265766965772F696E6465782C64696172792F7265766965772F706572736F6E616C2C64696172792F7265766965772F6164642C64696172792F7265766965772F656469742C64696172792F7265766965772F64656C2C64696172792F7265766965772F73686F77223B7D693A323B613A393A7B733A323A226964223B733A323A223438223B733A363A226D6F64756C65223B733A353A226469617279223B733A333A226B6579223B733A31303A2273746174697374696373223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E69FA5E79C8BE7BB9FE8AEA1223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE697A5E5BF97223B733A383A2263617465676F7279223B733A363A22E697A5E5BF97223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A33393A2264696172792F73746174732F706572736F6E616C2C64696172792F73746174732F726576696577223B7D7D7D7D7D733A383A2236594B75354C7532223B613A323A7B733A383A2263617465676F7279223B733A363A22E982AEE4BBB6223B733A353A2267726F7570223B613A313A7B733A31363A2236594B75354C75323536366835354347223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E982AEE4BBB6E7AEA1E79086223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223439223B733A363A226D6F64756C65223B733A353A22656D61696C223B733A333A226B6579223B733A353A22696E626F78223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E58685E983A8E982AEE7AEB1223B733A353A2267726F7570223B733A31323A22E982AEE4BBB6E7AEA1E79086223B733A383A2263617465676F7279223B733A363A22E982AEE4BBB6223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3230323A22656D61696C2F6C6973742F696E6465782C656D61696C2F6C6973742F7365617263682C656D61696C2F666F6C6465722F696E6465782C656D61696C2F666F6C6465722F6164642C656D61696C2F666F6C6465722F656469742C656D61696C2F666F6C6465722F64656C2C656D61696C2F636F6E74656E742F696E6465782C656D61696C2F636F6E74656E742F6164642C656D61696C2F636F6E74656E742F656469742C656D61696C2F636F6E74656E742F73686F772C656D61696C2F636F6E74656E742F6578706F7274223B7D693A313B613A393A7B733A323A226964223B733A323A223530223B733A363A226D6F64756C65223B733A353A22656D61696C223B733A333A226B6579223B733A383A22776562696E626F78223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E5A496E983A8E982AEE7AEB1223B733A353A2267726F7570223B733A31323A22E982AEE4BBB6E7AEA1E79086223B733A383A2263617465676F7279223B733A363A22E982AEE4BBB6223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A39313A22656D61696C2F7765622F696E6465782C656D61696C2F7765622F6164642C656D61696C2F7765622F656469742C656D61696C2F7765622F64656C2C656D61696C2F7765622F726563656976652C656D61696C2F7765622F73686F77223B7D7D7D7D7D733A31323A2235706148354C753235702B63223B613A323A7B733A383A2263617465676F7279223B733A393A22E69687E4BBB6E69F9C223B733A353A2267726F7570223B613A313A7B733A31323A2235706148354C753235702B63223B613A323A7B733A393A2267726F75704E616D65223B733A393A22E69687E4BBB6E69F9C223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223531223B733A363A226D6F64756C65223B733A343A2266696C65223B733A333A226B6579223B733A383A22706572736F616E6C223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4B8AAE4BABAE7BD91E79B98223B733A353A2267726F7570223B733A393A22E69687E4BBB6E69F9C223B733A383A2263617465676F7279223B733A393A22E69687E4BBB6E69F9C223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3330323A2266696C652F64656661756C742F696E6465782C66696C652F64656661756C742F67657464796E616D69632C66696C652F706572736F6E616C2F696E6465782C66696C652F706572736F6E616C2F676574636174652C66696C652F706572736F6E616C2F6164642C66696C652F706572736F6E616C2F64656C2C66696C652F706572736F6E616C2F73686F772C66696C652F706572736F6E616C2F616A6178656E742C66696C652F6D7973686172652F696E6465782C66696C652F6D7973686172652F676574636174652C66696C652F6D7973686172652F73686172652C66696C652F6D7973686172652F73686F772C66696C652F66726F6D73686172652F696E6465782C66696C652F66726F6D73686172652F676574636174652C66696C652F66726F6D73686172652F73686F77223B7D693A313B613A393A7B733A323A226964223B733A323A223532223B733A363A226D6F64756C65223B733A343A2266696C65223B733A333A226B6579223B733A373A22636F6D70616E79223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E585ACE58FB8E7BD91E79B98223B733A353A2267726F7570223B733A393A22E69687E4BBB6E69F9C223B733A383A2263617465676F7279223B733A393A22E69687E4BBB6E69F9C223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3131323A2266696C652F636F6D70616E792F696E6465782C66696C652F636F6D70616E792F676574636174652C66696C652F636F6D70616E792F6164642C66696C652F636F6D70616E792F64656C2C66696C652F636F6D70616E792F73686F772C66696C652F636F6D70616E792F616A6178656E74223B7D7D7D7D7D733A31363A223659436135352B6C35595773355A474B223B613A323A7B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A353A2267726F7570223B613A313A7B733A31363A223659436135352B6C35595773355A474B223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A226E6F6465223B613A343A7B693A303B613A393A7B733A323A226964223B733A323A223533223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A343A2276696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E9809AE79FA5E6B58FE8A788223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3231373A226F6666696369616C646F632F6F6666696369616C646F632F696E6465782C6F6666696369616C646F632F6F6666696369616C646F632F73686F772C6F6666696369616C646F632F6F6666696369616C646F632F676574646F636C6973742C6F6666696369616C646F632F63617465676F72792F696E6465782C6F6666696369616C646F632F636F6D6D656E742F676574636F6D6D656E746C6973742C6F6666696369616C646F632F636F6D6D656E742F616464636F6D6D656E742C6F6666696369616C646F632F636F6D6D656E742F64656C636F6D6D656E74223B7D693A313B613A393A7B733A323A226964223B733A323A223534223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A373A227075626C697368223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E9809AE79FA5E58F91E5B883223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A32373A226F6666696369616C646F632F6F6666696369616C646F632F616464223B7D693A323B613A393A7B733A323A226964223B733A323A223535223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A383A2263617465676F7279223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E9809AE79FA5E58886E7B1BBE7AEA1E79086223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3130323A226F6666696369616C646F632F63617465676F72792F696E6465782C6F6666696369616C646F632F63617465676F72792F6164642C6F6666696369616C646F632F63617465676F72792F656469742C6F6666696369616C646F632F63617465676F72792F64656C223B7D693A333B613A393A7B733A323A226964223B733A323A223536223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B613A323A7B693A303B613A393A7B733A323A226964223B733A323A223537223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B733A343A2265646974223B733A343A226E616D65223B733A363A22E7BC96E8BE91223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A32383A226F6666696369616C646F632F6F6666696369616C646F632F65646974223B7D693A313B613A393A7B733A323A226964223B733A323A223538223B733A363A226D6F64756C65223B733A31313A226F6666696369616C646F63223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B733A333A2264656C223B733A343A226E616D65223B733A363A22E588A0E999A4223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A32373A226F6666696369616C646F632F6F6666696369616C646F632F64656C223B7D7D733A343A226E616D65223B733A31323A22E9809AE79FA5E7AEA1E79086223B733A353A2267726F7570223B733A31323A22E9809AE79FA5E585ACE5918A223B733A383A2263617465676F7279223B733A31323A22E9809AE79FA5E585ACE5918A223B733A343A2274797065223B733A343A2264617461223B733A363A22726F75746573223B733A3130323A226F6666696369616C646F632F63617465676F72792F696E6465782C6F6666696369616C646F632F63617465676F72792F6164642C6F6666696369616C646F632F63617465676F72792F656469742C6F6666696369616C646F632F63617465676F72792F64656C223B7D7D7D7D7D733A31363A223562656C354C326335724748356F716C223B613A323A7B733A383A2263617465676F7279223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A353A2267726F7570223B613A313A7B733A31363A223562656C354C326335724748356F716C223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A343A226E6F6465223B613A343A7B693A303B613A393A7B733A323A226964223B733A323A223634223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A333A226B6579223B733A363A227265706F7274223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E4B8AAE4BABAE6B187E68AA5223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A383A2263617465676F7279223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3633313A227265706F72742F64656661756C742F696E6465782C7265706F72742F64656661756C742F6164642C7265706F72742F64656661756C742F656469742C7265706F72742F64656661756C742F64656C2C7265706F72742F64656661756C742F73686F772C7265706F72742F747970652F6164642C7265706F72742F747970652F656469742C7265706F72742F747970652F64656C2C7265706F72742F636F6D6D656E742F676574636F6D6D656E746C6973742C7265706F72742F636F6D6D656E742F616464636F6D6D656E742C7265706F72742F636F6D6D656E742F64656C636F6D6D656E742C7265706F72742F6170692F616464636F6D6D656E742C7265706F72742F6170692F616C6C726561642C7265706F72742F6170692F64656C636F6D6D656E742C7265706F72742F6170692F64656C7265706F72742C7265706F72742F6170692F666F726D7265706F72742C7265706F72742F6170692F676574636F6D6D656E746C6973742C7265706F72742F6170692F6765746C6973742C7265706F72742F6170692F6765747265616465722C7265706F72742F6170692F736176657265706F72742C7265706F72742F6170692F73686F777265706F72742C7265706F72742F6170692F7573657274656D706C6174652C7265706F72742F6170692F73686F706C6973742C7265706F72742F6170692F676574636F756E742C7265706F72742F6170692F676574636F6D6D656E74766965772C7265706F72742F6170692F676574726576696577636F6D6D656E742C7265706F72742F6170692F676574617574686F726974792C7265706F72742F6170692F6765746368617267652C7265706F72742F6170692F61646474656D706C617465223B7D693A313B613A393A7B733A323A226964223B733A323A223635223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A333A226B6579223B733A31353A226D616E6167657274656D706C617465223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E7AEA1E79086E6A8A1E69DBF223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A383A2263617465676F7279223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3230393A227265706F72742F6170692F7361766574656D706C6174652C7265706F72742F6170692F666F726D74656D706C6174652C7265706F72742F6170692F73657474656D706C6174652C7265706F72742F6170692F64656C74656D706C74652C7265706F72742F6170692F736F727474656D706C6174652C7265706F72742F6170692F7573657274656D706C6174652C7265706F72742F6170692F73686F706C6973742C7265706F72742F6170692F6D616E6167657274656D706C6174652C7265706F72742F6170692F67657470696374757265223B7D693A323B613A393A7B733A323A226964223B733A323A223636223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A333A226B6579223B733A31313A2273657474656D706C617465223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8AEBEE7BDAEE6A8A1E69DBF223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A383A2263617465676F7279223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A39333A227265706F72742F6170692F73657474656D706C6174652C7265706F72742F6170692F6D616E6167657274656D706C6174652C7265706F72742F6170692F7573657274656D706C6174652C7265706F72742F6170692F73686F706C697374223B7D693A333B613A393A7B733A323A226964223B733A323A223637223B733A363A226D6F64756C65223B733A363A227265706F7274223B733A333A226B6579223B733A363A22726576696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31383A22E8AF84E99885E4B88BE5B19EE6B187E68AA5223B733A353A2267726F7570223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A383A2263617465676F7279223B733A31323A22E5B7A5E4BD9CE6B187E68AA5223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3430343A227265706F72742F7265766965772F696E6465782C7265706F72742F7265766965772F706572736F6E616C2C7265706F72742F7265766965772F6164642C7265706F72742F7265766965772F656469742C7265706F72742F7265766965772F64656C2C7265706F72742F7265766965772F73686F772C7265706F72742F6170692F616C6C726561642C7265706F72742F6170692F666F726D7265706F72742C7265706F72742F6170692F6765747265616465722C7265706F72742F6170692F736176657265706F72742C7265706F72742F6170692F73686F777265706F72742C7265706F72742F6170692F7573657274656D706C6174652C7265706F72742F6170692F6765747374616D702C7265706F72742F6170692F7365747374616D702C7265706F72742F6170692F73686F706C6973742C7265706F72742F6170692F676574636F756E742C7265706F72742F6170692F676574636F6D6D656E74766965772C7265706F72742F6170692F676574726576696577636F6D6D656E742C7265706F72742F6170692F676574617574686F72697479223B7D7D7D7D7D733A31363A22364C434435702B6C356F71563536576F223B613A323A7B733A383A2263617465676F7279223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A353A2267726F7570223B613A313A7B733A31363A22364C434435702B6C356F71563536576F223B613A323A7B733A393A2267726F75704E616D65223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A343A226E6F6465223B613A333A7B693A303B613A393A7B733A323A226964223B733A323A223638223B733A363A226D6F64756C65223B733A343A22766F7465223B733A333A226B6579223B733A343A2276696577223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8B083E69FA5E6B58FE8A788223B733A353A2267726F7570223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A383A2263617465676F7279223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A3133313A22766F74652F64656661756C742F696E6465782C766F74652F64656661756C742F73686F772C766F74652F64656661756C742F6665746368696E6465786C6973742C766F74652F64656661756C742F73686F77766F74652C766F74652F64656661756C742F73686F77766F746575736572732C766F74652F64656661756C742F766F7465223B7D693A313B613A393A7B733A323A226964223B733A323A223639223B733A363A226D6F64756C65223B733A343A22766F7465223B733A333A226B6579223B733A373A227075626C697368223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8B083E69FA5E58F91E5B883223B733A353A2267726F7570223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A383A2263617465676F7279223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A38393A22766F74652F666F726D2F6164646F727570646174652C766F74652F666F726D2F757064617465656E6474696D652C766F74652F666F726D2F64656C2C766F74652F666F726D2F73686F772C766F74652F666F726D2F65646974223B7D693A323B613A393A7B733A323A226964223B733A323A223730223B733A363A226D6F64756C65223B733A343A22766F7465223B733A333A226B6579223B733A373A226D616E61676572223B733A343A226E6F6465223B733A303A22223B733A343A226E616D65223B733A31323A22E8B083E69FA5E7AEA1E79086223B733A353A2267726F7570223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A383A2263617465676F7279223B733A31323A22E8B083E69FA5E68A95E7A5A8223B733A343A2274797065223B733A343A226E6F6465223B733A363A22726F75746573223B733A31393A22766F74652F64656661756C742F6578706F7274223B7D7D7D7D7D7D);
INSERT INTO `yckj_syscache` VALUES ('users', '1', '0', '');
INSERT INTO `yckj_syscache` VALUES ('usergroup', '1', '1517282444', 0x613A373A7B693A313B613A363A7B733A333A22676964223B733A313A2231223B733A353A226772616465223B733A313A2231223B733A353A227469746C65223B733A363A22E4B99EE4B890223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A383A222D39393939393939223B733A31323A22637265646974736C6F776572223B733A313A2230223B7D693A323B613A363A7B733A333A22676964223B733A313A2232223B733A353A226772616465223B733A313A2232223B733A353A227469746C65223B733A31323A22E5889DE585A5E6B19FE6B996223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A313A2230223B733A31323A22637265646974736C6F776572223B733A323A223530223B7D693A333B613A363A7B733A333A22676964223B733A313A2233223B733A353A226772616465223B733A313A2233223B733A353A227469746C65223B733A31323A22E5B08FE69C89E5908DE6B094223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A323A223530223B733A31323A22637265646974736C6F776572223B733A333A22323030223B7D693A343B613A363A7B733A333A22676964223B733A313A2234223B733A353A226772616465223B733A313A2234223B733A353A227469746C65223B733A31323A22E6B19FE6B996E5B091E4BEA0223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A333A22323030223B733A31323A22637265646974736C6F776572223B733A333A22353030223B7D693A353B613A363A7B733A333A22676964223B733A313A2235223B733A353A226772616465223B733A313A2235223B733A353A227469746C65223B733A31323A22E6B19FE6B996E5A4A7E4BEA0223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A333A22353030223B733A31323A22637265646974736C6F776572223B733A343A2231303030223B7D693A363B613A363A7B733A333A22676964223B733A313A2236223B733A353A226772616465223B733A313A2236223B733A353A227469746C65223B733A31323A22E4B880E6B4BEE68E8CE997A8223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A343A2231303030223B733A31323A22637265646974736C6F776572223B733A343A2233303030223B7D693A373B613A363A7B733A333A22676964223B733A313A2237223B733A353A226772616465223B733A313A2237223B733A353A227469746C65223B733A31323A22E4B880E4BBA3E5AE97E5B888223B733A363A2273797374656D223B733A313A2231223B733A31333A2263726564697473686967686572223B733A343A2233303030223B733A31323A22637265646974736C6F776572223B733A393A22393939393939393939223B7D7D);
INSERT INTO `yckj_syscache` VALUES ('cronnextrun', '0', '1517287742', 0x31353137323837383030);
INSERT INTO `yckj_syscache` VALUES ('articlecategory', '1', '0', '');
INSERT INTO `yckj_syscache` VALUES ('officialdoccategory', '1', '0', '');

-- ----------------------------
-- Table structure for `yckj_syscode`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_syscode`;
CREATE TABLE `yckj_syscode` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `pid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `number` varchar(50) NOT NULL COMMENT '代码',
  `sort` mediumint(8) unsigned NOT NULL COMMENT '排序',
  `name` varchar(50) NOT NULL COMMENT '系统代码描述',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否系统代码',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '系统代码图标',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index` (`pid`,`number`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_syscode
-- ----------------------------
INSERT INTO `yckj_syscode` VALUES ('1', '0', 'RESUME_JOB_EDU', '1', '学历', '1', '');
INSERT INTO `yckj_syscode` VALUES ('2', '1', 'DOCTOR', '2', '博士', '1', '');
INSERT INTO `yckj_syscode` VALUES ('3', '1', 'MASTER', '3', '硕士', '1', '');
INSERT INTO `yckj_syscode` VALUES ('4', '1', 'BACHELOR_DEGREE', '4', '本科', '1', '');
INSERT INTO `yckj_syscode` VALUES ('5', '1', 'COLLEGE', '5', '大专', '1', '');
INSERT INTO `yckj_syscode` VALUES ('6', '1', 'SENIOR_HIGH', '6', '高中', '1', '');
INSERT INTO `yckj_syscode` VALUES ('7', '1', 'CHUNGCHI', '7', '中技', '1', '');
INSERT INTO `yckj_syscode` VALUES ('8', '1', 'TECHNICAL_SECONDARY', '8', '中专', '1', '');
INSERT INTO `yckj_syscode` VALUES ('9', '1', 'JUNIOR_HIGH', '9', '初中', '1', '');
INSERT INTO `yckj_syscode` VALUES ('10', '0', 'ENGLISH_LEVEL', '10', '英语水平', '1', '');
INSERT INTO `yckj_syscode` VALUES ('11', '10', 'VERY_GOOD', '11', '很好', '1', '');
INSERT INTO `yckj_syscode` VALUES ('12', '10', 'GOOD', '12', '较好', '1', '');
INSERT INTO `yckj_syscode` VALUES ('13', '10', 'ORDINARY', '13', '一般', '1', '');
INSERT INTO `yckj_syscode` VALUES ('14', '10', 'VERY_POOR', '14', '很差', '1', '');
INSERT INTO `yckj_syscode` VALUES ('15', '10', 'POOR', '15', '较差', '1', '');
INSERT INTO `yckj_syscode` VALUES ('16', '0', 'CONTACT_TYPE', '16', '联系类型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('17', '16', 'GTALK', '17', 'Gtalk', '1', '');
INSERT INTO `yckj_syscode` VALUES ('18', '16', 'YY', '18', 'YY', '1', '');
INSERT INTO `yckj_syscode` VALUES ('19', '16', 'SKYPE', '19', 'Skype', '1', '');
INSERT INTO `yckj_syscode` VALUES ('20', '16', 'QQ', '20', 'QQ', '1', '');
INSERT INTO `yckj_syscode` VALUES ('21', '16', 'MSN', '21', 'MSN', '1', '');
INSERT INTO `yckj_syscode` VALUES ('22', '16', 'FETION', '22', '飞信', '1', '');
INSERT INTO `yckj_syscode` VALUES ('23', '16', 'BAIDU_HI', '23', '百度Hi', '1', '');
INSERT INTO `yckj_syscode` VALUES ('24', '16', 'WANGWANG', '24', '旺旺', '1', '');
INSERT INTO `yckj_syscode` VALUES ('25', '16', 'PAOPAO', '25', '泡泡', '1', '');
INSERT INTO `yckj_syscode` VALUES ('26', '16', 'UC', '26', 'UC', '1', '');
INSERT INTO `yckj_syscode` VALUES ('27', '0', 'SUPPLIER_TYPE', '27', '供应商类型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('28', '27', 'INTERNAL_REFERRAL', '28', '内部推荐', '1', '');
INSERT INTO `yckj_syscode` VALUES ('29', '27', 'INTERMEDIARY', '29', '人才中介机构', '1', '');
INSERT INTO `yckj_syscode` VALUES ('30', '27', 'RECRUITMENT_SITE', '30', '招聘网站', '1', '');
INSERT INTO `yckj_syscode` VALUES ('31', '27', 'HEAD_HUNTING_COMPANY', '31', '猎头公司', '1', '');
INSERT INTO `yckj_syscode` VALUES ('69', '0', 'MEETING_ROOM_TYPE', '69', '会议室类型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('70', '69', 'INTERVIEW_FORM', '70', '接见式', '1', '');
INSERT INTO `yckj_syscode` VALUES ('71', '69', 'T-SHAPED', '71', 'T型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('72', '69', 'STEAMED_FORM', '72', '围桌', '1', '');
INSERT INTO `yckj_syscode` VALUES ('73', '69', 'THEATRE', '73', '剧院', '1', '');
INSERT INTO `yckj_syscode` VALUES ('74', '69', 'U-SHAPED', '74', 'U型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('75', '69', 'BANQUET_HALL', '75', '宴会厅', '1', '');
INSERT INTO `yckj_syscode` VALUES ('76', '69', 'BOARD_OF_DIRECTORS_FORM', '76', '董事会式', '1', '');
INSERT INTO `yckj_syscode` VALUES ('77', '69', 'AMPHITHEATRE', '77', '阶梯型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('78', '0', 'MEETING_ROOM_DEVICE_TYPE', '78', '会议室设备类型', '1', '');
INSERT INTO `yckj_syscode` VALUES ('79', '78', 'VIDICON', '79', '摄像机', '1', 'vidicon.png');
INSERT INTO `yckj_syscode` VALUES ('80', '78', 'NETBOOK', '80', '笔记本电脑', '1', 'netbook.png');
INSERT INTO `yckj_syscode` VALUES ('81', '78', 'RECORDER_PEN', '81', '录音笔', '1', 'recorder_pen.png');
INSERT INTO `yckj_syscode` VALUES ('82', '78', 'MICPHONE', '82', '麦克风', '1', 'micphone.png');
INSERT INTO `yckj_syscode` VALUES ('83', '78', 'WHITE_BOARD', '83', '白板', '1', 'white_board.png');
INSERT INTO `yckj_syscode` VALUES ('84', '78', 'WIRED_NETWORK', '84', '有线网络', '1', 'wired_network.png');
INSERT INTO `yckj_syscode` VALUES ('85', '78', 'WIRELESS_NETWORK', '85', '无线网络', '1', 'wireless_network.png');
INSERT INTO `yckj_syscode` VALUES ('86', '78', 'PROJECTOR', '86', '投影仪', '1', 'projector.png');
INSERT INTO `yckj_syscode` VALUES ('87', '78', 'LOUDSPEAKER_BOX', '87', '音箱', '1', 'loudspeaker_box.png');
INSERT INTO `yckj_syscode` VALUES ('88', '78', 'CAMERA', '88', '相机', '1', 'camera.png');
INSERT INTO `yckj_syscode` VALUES ('89', '78', 'SLIDE', '89', '幻灯片', '1', 'slide.png');

-- ----------------------------
-- Table structure for `yckj_tasks`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_tasks`;
CREATE TABLE `yckj_tasks` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '任务ID，由前台传递',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '任务主题',
  `pid` varchar(50) NOT NULL DEFAULT '' COMMENT '父级任务的ID',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `date` varchar(10) NOT NULL DEFAULT '' COMMENT '截止日期',
  `complete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '完成状态(0为未完成，1为完成)',
  `allcomplete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '父、子级所有任务完成状态(0为未完成，1为完成)',
  `completetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '完成时间',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '任务所属的用户ID',
  `upuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '上司的ID(添加人是上司时)',
  `isupper` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是上属添加的任务(0为自己添加的任务，1为上属添加的任务)',
  `mark` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否标记',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`,`complete`,`uid`,`upuid`,`isupper`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_template`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_template`;
CREATE TABLE `yckj_template` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `tname` varchar(255) NOT NULL COMMENT '汇报模板名称',
  `autonumber` varchar(255) DEFAULT NULL COMMENT '自动文号',
  `pictureurl` text NOT NULL COMMENT '模板图标url',
  `cateid` int(10) unsigned NOT NULL COMMENT '模板分类id',
  `description` text,
  `addtime` int(10) unsigned NOT NULL COMMENT '创建模板或者添加模板的时间',
  `deptid` text COMMENT '可用部门',
  `positionid` text COMMENT '可用职位',
  `roleid` text COMMENT '可用角色',
  `uid` text COMMENT '可用用户',
  `uptype` text COMMENT '主管类型，1表示一级主管，2表示二级主管，3表示三级主管，4表示四级主管，5表示五级主管',
  `upuid` text COMMENT '主管uid',
  `adduser` int(10) unsigned DEFAULT NULL COMMENT '添加系统模板和新建模板的用户id',
  `isnew` int(10) unsigned NOT NULL COMMENT '是不是最新的模板，1表示是，0表示不是',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_template
-- ----------------------------
INSERT INTO `yckj_template` VALUES ('36', '周报', '{U}-{T}', 'default', '0', null, '1486610594', 'alldept', null, null, '', '', '', '1', '1');

-- ----------------------------
-- Table structure for `yckj_template_add`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_template_add`;
CREATE TABLE `yckj_template_add` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `shoptid` int(10) unsigned NOT NULL COMMENT '模板商城模板id',
  `tid` int(10) unsigned NOT NULL COMMENT '添加模板id',
  `uid` int(10) unsigned NOT NULL COMMENT '添加这id',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_template_add
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_template_category`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_template_category`;
CREATE TABLE `yckj_template_category` (
  `cateid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类自增id',
  `categoryname` varchar(255) NOT NULL COMMENT '分类名',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`cateid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_template_category
-- ----------------------------
INSERT INTO `yckj_template_category` VALUES ('1', '个人模板', '1480489635');
INSERT INTO `yckj_template_category` VALUES ('2', '官方模板', '1480489636');
INSERT INTO `yckj_template_category` VALUES ('3', '服务业', '1480489637');
INSERT INTO `yckj_template_category` VALUES ('4', '餐饮业', '1480489638');
INSERT INTO `yckj_template_category` VALUES ('5', '建筑业', '1480489639');

-- ----------------------------
-- Table structure for `yckj_template_field`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_template_field`;
CREATE TABLE `yckj_template_field` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '字段自增id',
  `tid` int(10) unsigned NOT NULL COMMENT '模板id',
  `fieldname` varchar(255) NOT NULL COMMENT '字段名称',
  `iswrite` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填，0表示不需要，1表示需要',
  `fieldtype` int(10) unsigned NOT NULL COMMENT '字段类型，1表示长文本，2表示短文本，3表示数字，4表示日期与时间，5表示时间，6表示日期，7表示下拉',
  `fieldvalue` text COMMENT '字段值',
  `fieldsort` int(10) unsigned NOT NULL COMMENT '字段排序序号',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_template_field
-- ----------------------------
INSERT INTO `yckj_template_field` VALUES ('1', '36', '原计划', '0', '2', null, '1');
INSERT INTO `yckj_template_field` VALUES ('2', '36', '计划外', '0', '2', null, '1');
INSERT INTO `yckj_template_field` VALUES ('3', '36', '工作总结', '0', '1', null, '1');
INSERT INTO `yckj_template_field` VALUES ('4', '36', '工作计划', '0', '1', null, '1');

-- ----------------------------
-- Table structure for `yckj_template_sort`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_template_sort`;
CREATE TABLE `yckj_template_sort` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` int(10) unsigned NOT NULL COMMENT '用户uid',
  `tid` int(10) unsigned NOT NULL COMMENT '模板id',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序序号',
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_template_sort
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_user`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user`;
CREATE TABLE `yckj_user` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` char(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `isadministrator` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id标识: 0为非管理员，1为管理员',
  `deptid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '部门id',
  `positionid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '职位id',
  `roleid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `upuid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '直属领导id ',
  `groupid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '用户组id',
  `jobnumber` char(20) NOT NULL DEFAULT '' COMMENT '工号',
  `realname` char(20) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `gender` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别\n(0女1男)',
  `weixin` varchar(100) NOT NULL DEFAULT '' COMMENT '微信号',
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `email` char(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户状态，0正常、1锁定、2禁用',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `credits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总积分',
  `newcomer` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否新成员标识',
  `salt` char(10) NOT NULL DEFAULT '' COMMENT '用户身份验证码',
  `validationemail` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证了邮件地址( (1为已验证0为未验证)',
  `validationmobile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证了手机号码 (1为已验证0为未验证)',
  `lastchangepass` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改密码的时间',
  `guid` char(36) NOT NULL DEFAULT '' COMMENT '用户的唯一ID',
  PRIMARY KEY (`uid`),
  KEY `groupid` (`groupid`) USING BTREE,
  KEY `mobile` (`mobile`),
  KEY `email` (`email`),
  KEY `jobnumber` (`jobnumber`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user
-- ----------------------------
INSERT INTO `yckj_user` VALUES ('1', 'admin', '1', '0', '0', '0', '0', '0', '', '超级管理员', 'cf5c03c010684fc8db86062cf1840eff', '1', '', '13242930866', '', '0', '1517282439', '0', '1', 'hohgLG', '0', '0', '0', 'C9586EA1-A4F1-5391-0BDE-3A014CB471C0');
INSERT INTO `yckj_user` VALUES ('2', '钟汉唐', '0', '9', '2', '1', '1', '2', '3', '钟汉唐', 'c6d6a14e0a2e39f9eaa8408ea3097a22', '1', '', '13658749658', 'test2@ibos.com.cn', '0', '1392342512', '0', '1', '8pg484', '0', '0', '0', 'DB4EF28D-29AA-90E6-99C2-4BAE34F570E8');
INSERT INTO `yckj_user` VALUES ('3', '彭君华', '0', '9', '3', '3', '2', '2', '1', '彭君华', '6c19dbc84e14de92f6a63f32c787233c', '1', '', '13586549582', 'test3@ibos.com.cn', '0', '1392342563', '0', '1', '3fegcg', '0', '0', '0', '76D8A4B4-8DE0-A41A-6A96-EBACCC2669CD');
INSERT INTO `yckj_user` VALUES ('4', '郑洁', '0', '9', '3', '3', '2', '2', '0', '郑洁', 'a2f6669d9ec7504c5f1fc99f8c854b61', '0', '', '13685423685', 'test4@ibos.com.cn', '0', '1392342608', '0', '1', 'wIWk2a', '0', '0', '0', '913F0D78-DA6C-EC2A-880B-BF4EFCB3611A');

-- ----------------------------
-- Table structure for `yckj_user_binding`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_binding`;
CREATE TABLE `yckj_user_binding` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `bindvalue` text NOT NULL COMMENT '绑定的值',
  `app` char(30) NOT NULL COMMENT '绑定的类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uidandapp` (`uid`,`app`),
  KEY `uid` (`uid`),
  KEY `app` (`app`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user_binding
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_user_count`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_count`;
CREATE TABLE `yckj_user_count` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `extcredits1` int(10) NOT NULL DEFAULT '0' COMMENT '扩展积分1',
  `extcredits2` int(10) NOT NULL DEFAULT '0' COMMENT '扩展积分2',
  `extcredits3` int(10) NOT NULL DEFAULT '0' COMMENT '扩展积分3',
  `extcredits4` int(10) NOT NULL DEFAULT '0' COMMENT '扩展积分4',
  `extcredits5` int(10) NOT NULL DEFAULT '0' COMMENT '扩展积分5',
  `attachsize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总附件大小',
  `oltime` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '在线时间',
  `feeds` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '动态数',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user_count
-- ----------------------------
INSERT INTO `yckj_user_count` VALUES ('1', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `yckj_user_count` VALUES ('2', '0', '18', '8', '0', '0', '0', '0', '0');
INSERT INTO `yckj_user_count` VALUES ('3', '0', '16', '7', '0', '0', '0', '0', '0');
INSERT INTO `yckj_user_count` VALUES ('4', '0', '16', '7', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `yckj_user_data`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_data`;
CREATE TABLE `yckj_user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户UID',
  `key` varchar(50) NOT NULL COMMENT 'Key',
  `value` text COMMENT '对应Key的 值',
  `mtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '当前时间戳',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user-key` (`uid`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user_data
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_user_follow`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_follow`;
CREATE TABLE `yckj_user_follow` (
  `followid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '关注者ID',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '被关注者ID',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '关注时间',
  PRIMARY KEY (`followid`),
  UNIQUE KEY `uid-fid` (`uid`,`fid`),
  UNIQUE KEY `fid-uid` (`fid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user_follow
-- ----------------------------

-- ----------------------------
-- Table structure for `yckj_user_group`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_group`;
CREATE TABLE `yckj_user_group` (
  `gid` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组ID',
  `grade` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '组头衔',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统自带：1为是；0为否',
  `creditshigher` int(10) NOT NULL DEFAULT '0' COMMENT '该组的积分上限',
  `creditslower` int(10) NOT NULL DEFAULT '0' COMMENT '该组的积分下限',
  PRIMARY KEY (`gid`),
  KEY `creditsrange` (`creditshigher`,`creditslower`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_user_group
-- ----------------------------
INSERT INTO `yckj_user_group` VALUES ('1', '1', '乞丐', '1', '-9999999', '0');
INSERT INTO `yckj_user_group` VALUES ('2', '2', '初入江湖', '1', '0', '50');
INSERT INTO `yckj_user_group` VALUES ('3', '3', '小有名气', '1', '50', '200');
INSERT INTO `yckj_user_group` VALUES ('4', '4', '江湖少侠', '1', '200', '500');
INSERT INTO `yckj_user_group` VALUES ('5', '5', '江湖大侠', '1', '500', '1000');
INSERT INTO `yckj_user_group` VALUES ('6', '6', '一派掌门', '1', '1000', '3000');
INSERT INTO `yckj_user_group` VALUES ('7', '7', '一代宗师', '1', '3000', '999999999');

-- ----------------------------
-- Table structure for `yckj_user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_profile`;
CREATE TABLE `yckj_user_profile` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `birthday` int(11) unsigned NOT NULL DEFAULT '0',
  `telephone` varchar(255) NOT NULL DEFAULT '' COMMENT '住宅电话',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '地址',
  `qq` varchar(255) NOT NULL DEFAULT '' COMMENT 'QQ',
  `bio` varchar(255) NOT NULL DEFAULT '' COMMENT '自我介绍',
  `remindsetting` text COMMENT '提醒设置',
  `avatar_big` varchar(255) NOT NULL DEFAULT '' COMMENT '大头像',
  `avatar_middle` varchar(255) NOT NULL DEFAULT '' COMMENT '中头像',
  `avatar_small` varchar(255) NOT NULL DEFAULT '' COMMENT '小头像',
  `bg_big` varchar(255) NOT NULL DEFAULT '' COMMENT '大背景',
  `bg_middle` varchar(255) NOT NULL DEFAULT '' COMMENT '中背景',
  `bg_small` varchar(255) NOT NULL DEFAULT '' COMMENT '小背景',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户资料表';

-- ----------------------------
-- Records of yckj_user_profile
-- ----------------------------
INSERT INTO `yckj_user_profile` VALUES ('1', '0', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `yckj_user_profile` VALUES ('2', '0', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `yckj_user_profile` VALUES ('3', '0', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `yckj_user_profile` VALUES ('4', '681321600', '13658246958', '广州', '228571845', 'KIM JAEJOONG', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `yckj_user_status`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_user_status`;
CREATE TABLE `yckj_user_status` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户UID',
  `regip` char(15) NOT NULL DEFAULT '' COMMENT '注册IP',
  `lastip` char(15) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `lastvisit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后访问',
  `lastactivity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后活动',
  `invisible` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐身登录',
  PRIMARY KEY (`uid`),
  KEY `lastactivity` (`lastactivity`,`invisible`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户状态表';

-- ----------------------------
-- Records of yckj_user_status
-- ----------------------------
INSERT INTO `yckj_user_status` VALUES ('1', '172.16.1.50', '172.16.1.50', '1517287783', '1517282444', '1');
INSERT INTO `yckj_user_status` VALUES ('2', '192.168.1.47', '192.168.1.11', '1392356216', '1392356216', '0');
INSERT INTO `yckj_user_status` VALUES ('3', '192.168.1.47', '192.168.1.163', '1392353552', '1392343004', '0');
INSERT INTO `yckj_user_status` VALUES ('4', '192.168.1.47', '192.168.1.47', '1392360128', '1392345815', '0');

-- ----------------------------
-- Table structure for `yckj_vote`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_vote`;
CREATE TABLE `yckj_vote` (
  `voteid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '投票描述',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `isvisible` tinyint(1) NOT NULL DEFAULT '0' COMMENT '投票结果查看权限，0：所有人可见、1：投票后可见',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '发布者UID',
  `deadlinetype` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '截至日期类型：0自定义，1周，2月，3半年，4年',
  `relatedmodule` varchar(64) NOT NULL DEFAULT '' COMMENT '模块名称',
  `relatedid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '该模块表下id列的值',
  `deptid` text NOT NULL COMMENT '阅读范围部门',
  `positionid` text NOT NULL COMMENT '阅读范围职位',
  `roleid` text NOT NULL COMMENT '阅读范围角色',
  `scopeuid` text NOT NULL COMMENT '阅读范围人员',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`voteid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_vote
-- ----------------------------
INSERT INTO `yckj_vote` VALUES ('1', '演示数据', '演示数据', '1477533362', '1477792440', '0', '1', '0', 'vote', '1', 'alldept', '', '', '', '1477533362', '1477533362');

-- ----------------------------
-- Table structure for `yckj_vote_item`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_vote_item`;
CREATE TABLE `yckj_vote_item` (
  `itemid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '投票项id',
  `voteid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '投票id',
  `topicid` int(11) unsigned NOT NULL COMMENT '投票题目 id',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '投票项内容',
  `number` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '投票数',
  `picpath` varchar(255) NOT NULL DEFAULT '' COMMENT '图片路径',
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_vote_item
-- ----------------------------
INSERT INTO `yckj_vote_item` VALUES ('1', '1', '1', '页面美观超养眼', '0', '');
INSERT INTO `yckj_vote_item` VALUES ('2', '1', '1', '功能强大价值高', '0', '');
INSERT INTO `yckj_vote_item` VALUES ('3', '1', '1', '创新办公新体验', '0', '');
INSERT INTO `yckj_vote_item` VALUES ('4', '1', '1', '以上全部', '1', '');
INSERT INTO `yckj_vote_item` VALUES ('5', '1', '2', '张飞群', '0', 'data/attachment/vote/201406/12/180134mpmp4pojdtm4odxj.jpg');
INSERT INTO `yckj_vote_item` VALUES ('6', '1', '2', '李多云', '0', 'data/attachment/vote/201406/12/180137ru6ru9qg8yrvn5ux.jpg');
INSERT INTO `yckj_vote_item` VALUES ('7', '1', '2', '王泽', '1', 'data/attachment/vote/201406/12/180140dup9pkyjz87ku5ux.jpg');

-- ----------------------------
-- Table structure for `yckj_vote_item_count`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_vote_item_count`;
CREATE TABLE `yckj_vote_item_count` (
  `voteid` mediumint(9) unsigned NOT NULL COMMENT '投票 id',
  `topicid` mediumint(9) unsigned NOT NULL COMMENT '投票话题 id',
  `itemid` mediumint(8) unsigned NOT NULL COMMENT '投票项ID',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'UID',
  UNIQUE KEY `itemid` (`itemid`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_vote_item_count
-- ----------------------------
INSERT INTO `yckj_vote_item_count` VALUES ('1', '1', '4', '1');
INSERT INTO `yckj_vote_item_count` VALUES ('1', '2', '7', '1');

-- ----------------------------
-- Table structure for `yckj_vote_topic`
-- ----------------------------
DROP TABLE IF EXISTS `yckj_vote_topic`;
CREATE TABLE `yckj_vote_topic` (
  `topicid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `voteid` mediumint(8) unsigned NOT NULL COMMENT '投票 id',
  `subject` varchar(255) NOT NULL DEFAULT '' COMMENT '投票题目标题',
  `type` tinyint(4) NOT NULL COMMENT '投票题目类型：1、内容；2、图片',
  `maxselectnum` tinyint(4) unsigned NOT NULL COMMENT '是否多选: 0：单选；1：多选',
  `itemnum` tinyint(4) NOT NULL COMMENT '选项个数',
  PRIMARY KEY (`topicid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yckj_vote_topic
-- ----------------------------
INSERT INTO `yckj_vote_topic` VALUES ('1', '1', 'IBOS2.0产品热度大调查，火速参与！', '1', '1', '4');
INSERT INTO `yckj_vote_topic` VALUES ('2', '1', '谁是你心中优秀的同事？', '2', '1', '3');
